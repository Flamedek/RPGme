import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `java-library`
    kotlin("jvm") version "1.8.0"
}

group = "com.rpgme.plugin"
version = "2.1.2"

repositories {
    maven {
        name = "spigot-repo"
        url = uri("https://hub.spigotmc.org/nexus/content/repositories/snapshots")
        content {
            includeGroup("org.bukkit")
            includeGroup("org.spigotmc")
        }
    }
    maven {
        name = "placeholder-repo"
        url = uri("https://repo.extendedclip.com/content/repositories/placeholderapi/")
        content {
            includeGroup("me.clip")
        }
    }
    maven {
        name = "bungee-repo"
        url = uri("https://oss.sonatype.org/content/repositories/snapshots")
    }
    mavenCentral()
    maven {
        name = "sk89q-repo"
        url = uri("https://maven.enginehub.org/repo/")
    }
}

dependencies {
    // kotlin
    implementation(kotlin("stdlib-jdk8", "1.8.0"))

    // spigot api
    compileOnly("org.spigotmc", "spigot-api", "1.19.3-R0.1-SNAPSHOT")

    // plugins
    compileOnly("com.sk89q.worldguard", "worldguard-bukkit", "7.0.0")
    compileOnly("com.sk89q.worldguard", "worldguard-core", "7.0.0")
    compileOnly("me.clip", "placeholderapi", "2.10.4")

    // test
    testImplementation("junit", "junit", "4.12")
}

/*
 * Set java version and encoding
 */
configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

val outputPath = project.findProperty("output")?.toString().orEmpty()

/*
 * Update build jar task
 */
tasks.jar {
    duplicatesStrategy = DuplicatesStrategy.WARN
    // Set jar output to output argument eg. 'gradle build -Poutput "~/server/plugins"'
    project.findProperty("output")?.let {
        destinationDirectory.set(File(it.toString()))
    }
    // include implementation dependencies
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
}