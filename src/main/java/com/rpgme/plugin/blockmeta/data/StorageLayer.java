package com.rpgme.plugin.blockmeta.data;

import org.bukkit.block.Block;
import org.bukkit.configuration.MemoryConfiguration;

public interface StorageLayer<V> {

    int size();

    String getName();

    void setData(Block block, V value);

    V getData(Block block);

    void readFromFile(MemoryConfiguration file);

    void writeToFile(MemoryConfiguration file);

}