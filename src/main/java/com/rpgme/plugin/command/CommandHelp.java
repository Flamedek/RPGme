package com.rpgme.plugin.command;

import com.google.common.collect.Lists;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Robin on 25/02/2017.
 */
public class CommandHelp {

    private final List<String> usages;
    private final List<String> descriptions;
    private final List<String> permissions;

    public CommandHelp(Builder builder) {
        this.usages = Collections.unmodifiableList(builder.usages);
        this.descriptions = Collections.unmodifiableList(builder.descriptions);
        this.permissions = Collections.unmodifiableList(builder.permissions);
    }

    public CommandHelp(String usage, String description) {
        this(new Builder().addUsage(usage, description, null));
    }

    public CommandHelp(String usage, String description, String permission) {
        this(new Builder().addUsage(usage, description, permission));
    }

    public CommandHelp() {
        usages = descriptions = permissions = Collections.emptyList();
    }

    public int size() {
        return usages.size();
    }

    public boolean isEmpty() {
        return usages.isEmpty();
    }

    public boolean isEmpty(CommandSender sender) {
        for (String permission : permissions) {
            if (permission.isEmpty() || sender.hasPermission(permission))
                return false;
        }
        return true;
    }

    public List<String> getUsages() {
        return usages;
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public List<String> getPermissions() {
        return permissions;
    }

//    public String listCommandUsages(CommandSender player, String descriptionColor, String commandColor) {
//        StringBuilder builder = new StringBuilder();
//        boolean first = true;
//
//        for(int i = 0; i < usages.size(); i++) {
//
//            String usage = usages.get(i);
//            String description = descriptions.get(i);
//            String permission = permissions.get(i);
//
//            if(usage.isEmpty() || (!permissions.isEmpty() && !player.hasPermission(permission))) {
//                continue;
//            }
//
//            if(first) {
//                first = false;
//            } else {
//                builder.append('\n');
//            }
//
//            builder.append(commandColor).append(usage);
//            if(!description.isEmpty()) {
//                builder.append(descriptionColor).append(description).append('\n');
//            }
//        }
//        return builder.toString();
//    }

    public static class Builder {

        private final ArrayList<String> usages = Lists.newArrayList();
        private final ArrayList<String> descriptions = Lists.newArrayList();
        private final ArrayList<String> permissions = Lists.newArrayList();

        public Builder addUsage(String usage) {
            return addUsage(usage, null, null);
        }

        public Builder addUsage(String usage, String description) {
            return addUsage(usage, description, null);
        }

        public Builder addUsage(String usage, String description, String permission) {
            usages.add(usage == null ? "" : usage);
            descriptions.add(description == null ? "" : description);
            permissions.add(permission == null ? "" : permission);
            return this;
        }

        public CommandHelp build() {
            usages.trimToSize();
            descriptions.trimToSize();
            permissions.trimToSize();
            return new CommandHelp(this);
        }
    }
}
