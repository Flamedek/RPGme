package com.rpgme.plugin.command

import com.rpgme.plugin.RPGme
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

object TabCompleteHelper {

    /**
     * Find auto-complete options for a command given a given [pattern].
     * Patterns are separated by a space for arguments. Easy argument may be:
     * - <player> completes a online players name
     * - <skill> completes a skills name
     * - any comma-separated list of strings
     */
    @JvmStatic
    fun completePattern(sender: CommandSender, args: List<String>, pattern: String): List<String> {
        if (args.isEmpty()) {
            return emptyList()
        }

        val lastWord = args.last()
        val patternParts = pattern.split(" ")

        if (args.size > patternParts.size) {
            return emptyList()
        }

        return when (val part = patternParts[args.size - 1]) {
            "<player>" -> matchPlayers(sender, lastWord)
            "<skill>" -> {
                val targetIndex = patternParts.indexOf("<player>")
                val target = args.getOrNull(targetIndex)?.let(Bukkit::getPlayer)
                matchSkills(lastWord, target, false)
            }
            "<skills>" -> {
                val targetIndex = patternParts.indexOf("<player>")
                val target = args.getOrNull(targetIndex)?.let(Bukkit::getPlayer)
                matchSkills(lastWord, target, true)
            }
            else -> {
                val options = part.split(",")
                match(options, lastWord)
            }
        }
    }

    /**
     * Find matching values from [options]
     */
    @JvmStatic
    @JvmOverloads
    fun match(options: List<String>, part: String, delimiter: String? = null): List<String> {
        if (delimiter.isNullOrEmpty() || delimiter !in part) {
            return options.filter { it.startsWith(part, ignoreCase = true) }
        } else {
            val splitPart = part.split(delimiter)
            val currentParts = splitPart.take(splitPart.size - 1).joinToString(delimiter)

            val lastPart = splitPart[splitPart.size - 1]
            val nextOptions = options.filter { it.startsWith(lastPart, ignoreCase = true) }

            return nextOptions.map { option ->
                currentParts + delimiter + option
            }
        }
    }

    /**
     * Find player names from online players visible to the sender.
     */
    @JvmStatic
    fun matchPlayers(sender: CommandSender, part: String): List<String> {
        val players = Bukkit.getServer().onlinePlayers
        val playerSender = sender as? Player
        return players.mapNotNull { player ->
            val canSee = playerSender === sender || playerSender?.canSee(player) != false
            player.name.takeIf { canSee && it.startsWith(part, ignoreCase = true) }
        }
    }

    /**
     * Find skill names from all skills or skills enabled for a given [player].
     */
    @JvmStatic
    fun matchSkills(part: String, player: Player?, multiple: Boolean): List<String> {
        val skills = if (player != null) {
            RPGme.getInstance().skillManager.getEnabledSkills(player)
        } else {
            RPGme.getInstance().skillManager.enabledSkills
        }
        val delimiter = if (multiple) "," else null
        return match(skills.map { it.displayName.toLowerCase() }, part, delimiter)
    }

}