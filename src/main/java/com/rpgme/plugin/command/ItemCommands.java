package com.rpgme.plugin.command;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.treasure.TreasureBag;
import com.rpgme.plugin.treasure.TreasureChest;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.RPGItems;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Robin on 26/02/2017.
 */
public class ItemCommands extends CoreCommand {

    private static final String PERMISSION = "rpgme.command.admin";

    private static final String USAGE_EXPTOMB = "/exptomb <player> <exp> (minlevel) (skills) (amount)";
    private static final String USAGE_TREASURE = "/treasure <player> (tier) (amount)";

    public ItemCommands(RPGme plugin) {
        super(plugin, "rpgitems", PERMISSION);
        setConsoleAllowed(true);

        setAliases("exptomb", "treasure", "treasurechest");
        setDescription("Command to spawn custom rpgme items: exp tombs that grant exp to players and treasure chests to roll for loot.");
        setCommandHelp(new CommandHelp.Builder()
                .addUsage(USAGE_EXPTOMB, "Create an exp tomb, optionally specifying a minimum required level, " +
                        "a target player, an amount and comma separated list of skills to choose from.")
                .addUsage(USAGE_TREASURE, "Spawn a treasure chest, optionally specifying a level (0-100), player and count.")
                .build());
    }

    @Override
    public List<String> getTabComplete(CommandSender sender, String label, List<String> args) {
        switch (label) {
            case "exptomb":
                return TabCompleteHelper.completePattern(sender, args, "<player> [exp] 1,[minlevel] <skills> 1,[amount]");
            case "treasure":
            case "treasurechest":
                return TabCompleteHelper.completePattern(sender, args, "<player> 1-10,[tier] 1,[amount]");
            default:
                return Collections.emptyList();
        }
    }

    @Override
    public void execute(CommandSender sender, String alias, List<String> flags) {
        switch (alias) {
            case "exptomb":
                handleExptomb(sender, flags);
                break;
            case "treasure":
            case "treasurechest":
                handleTreasure(sender, flags);
                break;
            default:
                plugin.getCommandManager().printCommandHelp(this, sender);
        }
    }

    private void handleExptomb(CommandSender sender, List<String> flags) {
        Player target;
        int exp;
        int minlvl = 0;
        int count = 1;
        List<Skill> restrictedSkills = new ArrayList<>();

        if (flags.size() < 2) {
            error(sender, "Not enough arguments. " + USAGE_EXPTOMB);
            return;
        }

        // <player>
        target = plugin.getServer().getPlayer(flags.get(0));
        if (target == null) {
            error(sender, "Could not find player '" + flags.get(0) + "'.");
            return;
        }
        // <exp>
        if (!StringUtils.isNumeric(flags.get(1))) {
            error(sender, "Invalid argument: '" + flags.get(1) + "'. Expected positive number");
            return;
        }
        exp = Integer.parseInt(flags.get(1));
        if (exp < 1) {
            error(sender, "Invalid argument: '" + flags.get(1) + "'. Expected positive number");
            return;
        }

        // <minlevel>
        if (flags.size() > 2) {
            if (!StringUtils.isNumeric(flags.get(2))) {
                error(sender, "Invalid argument: '" + flags.get(2) + "'. Expected positive number");
                return;
            }
            minlvl = Math.max(1, Integer.parseInt(flags.get(2)));
        }
        // <skills>
        if (flags.size() > 3) {
            String[] parts = flags.get(3).split(",");
            for (String s : parts) {
                Skill skill = plugin.getSkillManager().getByName(s);
                if (skill != null) {
                    restrictedSkills.add(skill);
                }
            }
        }

        // <amount>
        if (flags.size() > 4) {
            if (!StringUtils.isNumeric(flags.get(4))) {
                error(sender, "Invalid argument: '" + flags.get(4) + "'. Expected positive number");
                return;
            }
            count = Integer.parseInt(flags.get(4));
            if (count < 1) {
                error(sender, "Invalid argument: '" + flags.get(4) + "'. Expected positive number");
                return;
            }
        }

        String restriction = "";
        if (restrictedSkills.isEmpty()) {
            restrictedSkills.addAll(plugin.getSkillManager().getEnabledSkills(target));
            restriction = "for any skill";
        } else {
            restriction = "restricted to ";
            int restrictionCount = restrictedSkills.size();
            for (int i = 0; i < restrictionCount; i++) {
                restriction += restrictedSkills.get(i).getDisplayName();
                if (i < restrictionCount - 1) {
                    restriction += ", ";
                }
            }
        }

        ItemStack item = RPGItems.createExpTomb(exp, minlvl, restrictedSkills);
        item.setAmount(count);
        ItemUtils.give(target, item);

        GameSound.play(Sound.ENTITY_ITEM_PICKUP, target);
        sender.sendMessage(String.format(ChatColor.YELLOW + "Given %d exp tomb(s) worth %d exp to %s. Required level is %d, %s",
                count, exp, target.getName(), minlvl, restriction));
    }

    private void handleTreasure(CommandSender sender, List<String> flags) {
        Player target = null;
        int tier = 0;
        int count = 1;

        if (flags.size() > 0) {
            target = plugin.getServer().getPlayer(flags.get(0));
        }
        if (flags.size() > 1) {
            try {
                tier = Integer.parseInt(flags.get(1));
                if (tier < 0 || tier > 10) {
                    error(sender, "Invalid argument: '" + flags.get(1) + "'. Expected number in range 1-10");
                    return;
                }
            } catch (NumberFormatException e) {
                error(sender, "Invalid argument: '" + flags.get(1) + "'. Expected number in range 1-10");
                return;
            }
        }
        if (flags.size() > 2) {
            try {
                count = Integer.parseInt(flags.get(2));
                if (count < 1) {
                    error(sender, "Invalid argument: '" + flags.get(2) + "'. Expected positive number");
                    return;
                }
            } catch (NumberFormatException e) {
                error(sender, "Invalid argument: '" + flags.get(2) + "'. Expected positive number");
                return;
            }
        }

        if (target == null) {
            if (!consoleCheck(sender)) {
                return;
            }
            target = (Player) sender;
        }

        TreasureBag treasureManager = plugin.getModule(RPGme.MODULE_TREASURES);
        int level = TreasureChest.levelForTier(tier);

        for (int i = 0; i < count; i++) {
            ItemStack item = treasureManager.rollTreasureChest(level);
            ItemUtils.give(target, item);
        }
        sender.sendMessage(String.format(ChatColor.YELLOW + "Given %d treasure chest(s) of tier %s to %s", count, TreasureChest.toRomanTier(level), target.getName()));
    }
}
