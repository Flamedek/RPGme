package com.rpgme.plugin.api;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.config.MessagesBundle;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.List;

/**
 *
 */
public interface Skill extends ListenerModule {

    RPGme getPlugin();

    SkillManager getManager();

    int getId();

    @Override
    String getName();

    String getDisplayName();

    boolean hasDisplayName();

    String getDescription();

    Material getItemRepresentation();

    boolean isEnabled();

    boolean isEnabled(Player p);

    boolean isEnabled(RPGPlayer rp);

    RPGPlayer getPlayer(Player p);

    int getLevel(Player p);

    int getLevel(RPGPlayer p);

    float getExp(Player p);

    float getExp(RPGPlayer p);

    void addExp(Player p, float exp);

    void addExp(RPGPlayer p, float exp);

    ConfigurationSection getConfig();

    void addPersonalNotification(RPGPlayer player, Notification note);

    void addNotification(Notification note);

    void sendNotifications(RPGPlayer player, int level);

    <A extends Ability> A getAbility(int id);

    Iterable<Ability> getAbilities();

    void registerAbility(int id, Ability ability);

    Ability removeAbility(int id);

    double getExpMultiplier();

    List<Notification> getNotifications();

    MessagesBundle getMessages();

    void setExpModifier(double expModifier);

    int getExpLimit();

    void setExpLimit(int expLimit);

    void setDisplayName(String displayName);

    void printNotification(Player p, Notification notification);

    /**
     * Builds and sends the chat text shown in the /<skill> command.
     */
    void showCommandText(Player p);

}
