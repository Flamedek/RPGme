package com.rpgme.plugin.event;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Listener;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.PermissionChecker;
import com.rpgme.plugin.util.menu.ExpAddMenu;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.NBTConstants;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

import static com.rpgme.plugin.util.RPGItems.*;

/**
 * Created by Robin on 26/02/2017.
 */
public class GeneralEventListener extends Listener<RPGme> {

    public GeneralEventListener(RPGme plugin) {
        super(plugin);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSpawn(SpawnerSpawnEvent event) {
        event.getEntity().setMetadata("MonsterSpawner", CoreUtils.FIXED_META_VALUE);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onItemUse(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (!e.hasItem() || !PermissionChecker.isEnabled(player))
            return;

        if ((e.getHand() == EquipmentSlot.HAND && !CoreUtils.isLeftClick(e.getAction())) ||
                (e.getHand() == EquipmentSlot.OFF_HAND && !CoreUtils.isRightClick(e.getAction())))
            return;

        Material type = e.getMaterial();
        if (type == Material.BOOK) {

            CompoundTag tag = NBTFactory.getFrom(e.getItem());
            if (tag == null) {
                return;
            }

            if (tag.hasValueOfType(TAG_EXPTOMB_EXP, NBTConstants.TYPE_INT)) {
                onExpTombUse(player, e.getItem(), tag);
            }

        }


//        if(e.hasItem() && PermissionChecker.i.isLoaded(e.getPlayer())) {
//
//            if(type == Material.BOOK || type == Material.EMERALD) {
//                CompoundTag tag = NBTFactory.getFrom(e.getItem());
//                if(tag == null) {
//                    return;
//                }
//
//                int[] data = tag.getIntegerArray("expTomb");
//                if(data != null) {
//                    (new ExpAddMenu((RPGme)this.plugin, e.getPlayer(), data[0], data[1])).openGUI();
//                    return;
//                }
//
//                data = tag.getIntegerArray("skillTomb");
//                if(data != null) {
//                    final SkillType skillGem = SkillType.values()[data[0]];
//                    final int posibleTipes = data[1];
//                    ConfirmationScreen var10000 = new ConfirmationScreen((JavaPlugin)this.plugin, e.getPlayer(), "&fIncrease skill &e" + skillGem.readableName() + "?") {
//                        public void onDeny() {
//                            GameSound.play(Sound.ENTITY_VILLAGER_NO, this.player);
//                        }
//
//                        public void onConfirm() {
//                            ItemStack newitem = this.player.getInventory().getItemInMainHand();
//                            newitem.setAmount(newitem.getAmount() - 1);
//                            this.player.getInventory().setItemInMainHand(newitem.getAmount() > 0?newitem:null);
//                            GameSound.play(Sound.ENTITY_PLAYER_ATTACK_SWEEP, this.player, 2.0F, 1.2F);
//                            ((RPGme)this.plugin).players.get(this.player).addTrueExp(skillGem, (float)posibleTipes);
//                        }
//                    };
//                }
//
//                boolean var12 = tag.getBoolean("SkillGem");
//                if(var12) {
//                    Collection var13 = SkillType.enabledValues(e.getPlayer());
//                    int random = CoreUtil.random.nextInt(var13.size());
//                    SkillType chosen = null;
//                    Iterator level = var13.iterator();
//
//                    while(level.hasNext()) {
//                        SkillType player = (SkillType)level.next();
//                        chosen = player;
//                        if(random-- == 0) {
//                            break;
//                        }
//                    }
//
//                    RPGPlayer var14 = ((RPGme)this.plugin).players.get(e.getPlayer());
//                    int var15 = var14.getLevel(chosen);
//                    int bonusExp = ExpTables.xpForLevel(var15 + 1) - (int)var14.getExp(chosen) + 10;
//                    if(ItemUtil.take(e.getPlayer().getInventory(), e.getItem(), 1)) {
//                        var14.addTrueExp(chosen, (float)bonusExp);
//                    }
//
//                    return;
//                }
//            }
//
//        }
    }

    private void onExpTombUse(Player player, ItemStack item, CompoundTag tag) {
        int exp = tag.hasValueOfType(TAG_EXPTOMB_EXP, NBTConstants.TYPE_INT) ? tag.getInteger(TAG_EXPTOMB_EXP) : 1;
        int minlvl = tag.hasValueOfType(TAG_EXPTOMB_MINLEVEL, NBTConstants.TYPE_INT) ? tag.getInteger(TAG_EXPTOMB_MINLEVEL) : 1;
        String skills = tag.hasValueOfType(TAG_EXPTOMB_SKILLS, NBTConstants.TYPE_STRING) ? tag.getString(TAG_EXPTOMB_SKILLS) : "";

        List<Skill> availableSkills = null;
        if (!skills.isEmpty()) {
            String[] split = skills.split(",");
            availableSkills = new ArrayList<>(split.length);

            for (String s : split) {
                Skill skill = plugin.getSkill(s);
                if (skill != null) {
                    availableSkills.add(skill);
                }
            }
        }

        new ExpAddMenu(plugin, player, item, exp, minlvl, availableSkills).openGUI();
    }
}
