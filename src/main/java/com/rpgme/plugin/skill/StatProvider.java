package com.rpgme.plugin.skill;

import java.util.List;

public interface StatProvider {

    void addCurrentStatistics(int forlevel, List<String> list);

}
