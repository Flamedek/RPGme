package com.rpgme.plugin.skill;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.MessageUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.cooldown.Cooldown;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.List;

public abstract class Ability<S extends Skill> implements ListenerModule, StatProvider {

    // plugin
    protected final RPGme plugin;
    protected final S skill;
    // ability
    private final String name;
    private final int id;

    protected ConfigurationSection config;
    protected BundleSection messages;

    private boolean enabled;
    private String displayName;

    public Ability(S skill, String name, int id) {
        this.skill = skill;
        this.name = name;
        this.id = id;
        plugin = skill.getPlugin();
    }

    @Override
    public void onEnable() {

    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {

    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        config.addValue("enabled", true);
        messages.addValue("name", getName());
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        this.config = config;
        this.messages = messages;
        enabled = config.getBoolean("enabled");
        displayName = messages.getMessage("name");
    }

    public void sendOnCooldownMessage(Player p, long millis) {
        sendOnCooldownMessage(p, millis, true);
    }

    public void sendOnCooldownMessage(Player p, long millis, boolean sound) {
        String msg = skill.getManager().buildCooldownMessage(millis);
        MessageUtil.sendToActionBar(p, msg);
        if (sound) {
            GameSound.play(Sound.ENTITY_VILLAGER_NO, p);
        }
    }

    public void sendOnCooldownMessage(Player p, Cooldown cooldown, boolean sound) {
        sendOnCooldownMessage(p, cooldown.getMillisRemaining(p), sound);
    }

    public final S getSkill() {
        return skill;
    }

    @Override
    public final String getName() {
        return name;
    }

    public ConfigurationSection getConfig() {
        return config;
    }

    public BundleSection getMessages() {
        return messages;
    }

    public final RPGme getPlugin() {
        return plugin;
    }

    public void addExp(Player p, float exp) {
        skill.addExp(p, exp);
    }

    public float getExp(RPGPlayer p) {
        return skill.getExp(p);
    }

    public boolean isEnabled() {
        return skill.isEnabled();
    }

    public RPGPlayer getPlayer(Player p) {
        return skill.getPlayer(p);
    }

    public void addNotification(Notification note) {
        skill.addNotification(note);
    }

    public float getExp(Player p) {
        return skill.getExp(p);
    }

    public void addExp(RPGPlayer p, float exp) {
        skill.addExp(p, exp);
    }

    public void addPersonalNotification(RPGPlayer player, Notification note) {
        skill.addPersonalNotification(player, note);
    }

    public final int getSkillId() {
        return skill.getId();
    }

    public final int getId() {
        return id;
    }

    public int getLevel(Player p) {
        return skill.getLevel(p);
    }

    public int getLevel(RPGPlayer p) {
        return p.getLevel(skill.getId());
    }

    public void addNotification(int level, String icon, String title, String text) {
        skill.addNotification(new Notification(level, icon, title, text));
    }

    public void addNotification(int level, String icon, String title, String text, boolean hidden) {
        skill.addNotification(new Notification(level, icon, title, text, hidden));
    }

    public boolean isEnabled(Player p) {
        return enabled && skill.isEnabled(p);
    }

    public String getDisplayName() {
        return displayName;
    }

}
