package com.rpgme.plugin.integration

import org.bukkit.entity.Player

/**
 * Plugin hook that hooks into a plugin that defines player relations.
 * This is used to determine if two players are allies, neutral or enemies.
 */
interface TeamPluginHook : PluginHook {

    fun getRelation(one: Player, two: Player) = PlayerRelation.NEUTRAL

}
