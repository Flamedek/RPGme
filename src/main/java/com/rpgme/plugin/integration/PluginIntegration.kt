package com.rpgme.plugin.integration

import com.rpgme.plugin.RPGme
import com.rpgme.plugin.integration.plugin.PlaceholderApiHook
import com.rpgme.plugin.integration.plugin.WorldGuardHook
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.block.BlockFace
import org.bukkit.entity.Entity
import org.bukkit.entity.Player
import org.bukkit.event.Event
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.inventory.EquipmentSlot
import org.bukkit.inventory.ItemStack
import java.util.logging.Level

class PluginIntegration private constructor() {

    companion object {
        @JvmStatic
        val instance = PluginIntegration()
    }

    private val pluginHooks = ArrayList<RpgmeHook>(4)
    private val teamHooks = ArrayList<TeamPluginHook>(4)

    private val eventCaller = FakeEventCaller()

    fun onLoad() {
        val pm = Bukkit.getPluginManager()
        // Our build-in hooks: other plugins may register additional hooks
        if (pm.isPluginEnabled("WorldGuard")) {
            register(WorldGuardHook())
        }
    }

    fun onEnable(plugin: RPGme) {
        val pm = Bukkit.getPluginManager()
        if (pm.isPluginEnabled("PlaceHolderAPI")) {
            register(PlaceholderApiHook())
        }
        // Prepare our method for calling events programmatically
        Bukkit.getPluginManager().registerEvents(eventCaller, plugin)
    }

    /**
     * Register a hook so that RPGme considers it in various checks.
     * @return true if successfully hooked. False otherwise.
     */
    fun register(hook: PluginHook): Boolean {
        val pm = Bukkit.getPluginManager()
        val plugin = pm.getPlugin(hook.pluginName)
        val logger = RPGme.getInstance().logger

        if (plugin != null) {
            try {
                hook.enable(plugin)
            } catch (e: Exception) {
                logger.log(Level.SEVERE, "Error while enabling hook for " + hook.pluginName, e)
                return false
            }

            if (hook is RpgmeHook) {
                pluginHooks.add(hook)
            }
            if (hook is TeamPluginHook) {
                teamHooks.add(hook)
            }
            logger.info("Successfully hooked into plugin: " + hook.pluginName)
            return true
        }
        return false
    }

    /*
     * Public methods that consult hooked plugins
     */

    fun getExpMultiplier(player: Player, location: Location): Double {
        if (pluginHooks.isEmpty()) {
            return 1.0
        }
        return pluginHooks.fold(1.0) { multiplier, hook ->
            multiplier * hook.getExpMultiplier(player, location)
        }
    }

    fun isRPGmeEnabled(player: Player, location: Location): Boolean {
        return pluginHooks.all { it.isRPGmeEnabled(player, location) }
    }

    fun getRelation(one: Player, two: Player): PlayerRelation {
        if (one === two) {
            return PlayerRelation.SELF
        }

        if (teamHooks.any { it.getRelation(one, two) == PlayerRelation.TEAM }) {
            return PlayerRelation.TEAM
        }
        if (teamHooks.any { it.getRelation(one, two) == PlayerRelation.ENEMIES }) {
            return PlayerRelation.ENEMIES
        }
        return PlayerRelation.NEUTRAL
    }

    fun canBreakBlock(p: Player, block: Block): Boolean {
        val event = BlockBreakEvent(block, p)
        val result = eventCaller.callFakeEvent(event)
        return result != false
    }

    fun canPlaceBlock(p: Player, block: Block?): Boolean {
        if (block == null) return true
        val toState = block.state
        val placedAgainst = if (block.y > 1) block.getRelative(BlockFace.DOWN) else block.getRelative(BlockFace.UP)
        val item = ItemStack(Material.AIR)
        val event = BlockPlaceEvent(block, toState, placedAgainst, item, p, true, EquipmentSlot.HAND)
        val result = eventCaller.callFakeEvent(event)
        return result != false
    }

    fun canDamageEntity(p: Player, entity: Entity): Boolean {
        val event = EntityDamageByEntityEvent(p, entity, EntityDamageEvent.DamageCause.ENTITY_ATTACK, 1.0)
        val result = eventCaller.callFakeEvent(event)
        return result != false
    }

    private class FakeEventCaller : Listener {

        private val calledEvents = hashMapOf<Event, Any?>()

        /**
         * Calls an event through the plugin manager and makes sure it gets cancelled
         * at highest priority to hopefully avoid any side effects to other plugins.
         * Protection plugins are expected to cancel the event at a lower priority.
         */
        fun callFakeEvent(event: Event): Any? {
            calledEvents[event] = null
            Bukkit.getPluginManager().callEvent(event)
            return calledEvents.remove(event)
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        fun cancelBlockBreak(event: BlockBreakEvent) {
            if (event in calledEvents) {
                // for break events, store cancelled state
                calledEvents[event] = event.isCancelled
                event.isCancelled = true
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        fun cancelBlockBreak(event: BlockPlaceEvent) {
            if (event in calledEvents) {
                // for place events, store cancelled or canBuild state
                calledEvents[event] = event.isCancelled || !event.canBuild()
                event.isCancelled = true
            }
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        fun cancelBlockBreak(event: EntityDamageByEntityEvent) {
            if (event in calledEvents) {
                // for break events, store cancelled state
                calledEvents[event] = event.isCancelled
                event.isCancelled = true
            }
        }

    }

}
