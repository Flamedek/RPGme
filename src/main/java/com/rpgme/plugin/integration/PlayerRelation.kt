package com.rpgme.plugin.integration

enum class PlayerRelation {
    SELF,
    TEAM,
    NEUTRAL,
    ENEMIES
}
