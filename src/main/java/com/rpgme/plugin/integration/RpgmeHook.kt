package com.rpgme.plugin.integration

import org.bukkit.Location
import org.bukkit.entity.Player

/**
 *
 */
abstract class RpgmeHook : PluginHook {

    open fun getExpMultiplier(player: Player, location: Location): Double {
        return 1.0
    }

    open fun isRPGmeEnabled(player: Player, location: Location): Boolean {
        return true
    }
}
