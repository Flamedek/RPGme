package com.rpgme.plugin.integration

import org.bukkit.plugin.Plugin

/**
 * Base Interface to represent a hook into an other plugin
 * Note: do not implement this class directly. Instead implement one of the extended interfaces.
 */
interface PluginHook {

    val pluginName: String

    fun enable(plugin: Plugin)
}
