package com.rpgme.plugin.integration.plugin

import com.rpgme.plugin.RPGme
import com.rpgme.plugin.integration.PluginHook
import me.clip.placeholderapi.PlaceholderAPI
import me.clip.placeholderapi.PlaceholderHook
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin

class PlaceholderApiHook : PluginHook {

    override val pluginName = "PlaceHolderAPI"

    override fun enable(plugin: Plugin) {
        if (!PlaceholderAPI.registerPlaceholderHook("RPGme", RPGmePlaceHolderHook())) {
            throw IllegalStateException("Could not hook into PlaceholderAPI (already registered?)")
        }
    }

    private inner class RPGmePlaceHolderHook : PlaceholderHook() {

        override fun onPlaceholderRequest(arg0: Player?, arg1: String?): String? {
            // get our player holding all information
            val player = RPGme.getInstance().getPlayer(arg0) ?: return null

            var value = -1
            // first check if the id is a skill
            val skill = RPGme.getInstance().skillManager.getByName(arg1)
            if (skill != null) {
                value = player.getLevel(skill)
            } else {
                // else check additional tags
                when (arg1) {
                    "total" -> value = player.skillSet.totalLevel
                    "combat" -> value = player.skillSet.combatLevel
                    "average" -> value = player.skillSet.averageLevel
                }
            }

            return if (value > 0) value.toString() else null
        }
    }

}
