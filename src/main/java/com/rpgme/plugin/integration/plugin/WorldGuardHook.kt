package com.rpgme.plugin.integration.plugin

import com.rpgme.plugin.integration.RpgmeHook
import com.sk89q.worldedit.bukkit.BukkitAdapter
import com.sk89q.worldguard.WorldGuard
import com.sk89q.worldguard.bukkit.WorldGuardPlugin
import com.sk89q.worldguard.protection.flags.DoubleFlag
import com.sk89q.worldguard.protection.flags.Flag
import com.sk89q.worldguard.protection.flags.StateFlag
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry
import org.bukkit.Location
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin

class WorldGuardHook : RpgmeHook() {

    companion object {
        val EXP_FLAG = DoubleFlag("exp-gain")
        val ENABLED_FLAG = StateFlag("rpgme-enabled", true)
    }

    override val pluginName = "WorldGuard"

    private lateinit var worldGuard: WorldGuardPlugin

    override fun enable(plugin: Plugin) {
        worldGuard = plugin as WorldGuardPlugin

        val registry = WorldGuard.getInstance().flagRegistry
        registerCustomFlag(registry, EXP_FLAG)
        registerCustomFlag(registry, ENABLED_FLAG)
    }

    private fun registerCustomFlag(registry: FlagRegistry, flag: Flag<*>) {
        try {
            registry.register(flag)
        } catch (e: FlagConflictException) {
            throw IllegalStateException("Could not register flag " + flag.name, e)
        }
    }

    override fun getExpMultiplier(player: Player, location: Location): Double {
        val localPlayer = WorldGuardPlugin.inst().wrapPlayer(player)
        val wgLocation = BukkitAdapter.adapt(location)
        val container = WorldGuard.getInstance().platform.regionContainer
        val flagValues = container.createQuery().queryAllValues(
            wgLocation, localPlayer,
            EXP_FLAG
        )
        return flagValues.fold(1.0) { multiplier, flagValue ->
            multiplier * flagValue
        }
    }

    override fun isRPGmeEnabled(player: Player, location: Location): Boolean {
        val localPlayer = WorldGuardPlugin.inst().wrapPlayer(player)
        val wgLocation = BukkitAdapter.adapt(location)
        val container = WorldGuard.getInstance().platform.regionContainer
        val state = container.createQuery().queryState(
            wgLocation, localPlayer,
            ENABLED_FLAG
        )
        return state != StateFlag.State.DENY
    }

}
