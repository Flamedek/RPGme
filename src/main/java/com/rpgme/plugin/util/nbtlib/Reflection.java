package com.rpgme.plugin.util.nbtlib;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Reflection {

    private static final Reflection instance = new Reflection();

    private final Map<String, Class<?>> classes = new HashMap<>();
    private final Map<String, Method> methods = new HashMap<>();
    private final Map<String, Field> fields = new HashMap<>();

    public static Class<?> getClass(String name) throws ClassNotFoundException {
        Class<?> value = instance.classes.computeIfAbsent(name, Reflection::classOrNull);
        if (value == null) {
            throw new ClassNotFoundException(name);
        }
        return value;
    }

    private static Class<?> classOrNull(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static Method getMethod(Class<?> clazz, String name, Class<?>... parameters) throws NoSuchMethodException {
        String key = clazz.getName() + '#' + name + Arrays.toString(parameters);
        Method value = instance.methods.computeIfAbsent(key, k -> methodOrNull(clazz, name, parameters));
        if (value == null) {
            throw new NoSuchMethodException(key);
        }
        return value;
    }

    private static Method methodOrNull(Class<?> clazz, String name, Class<?>... parameters) {
        try {
            return clazz.getDeclaredMethod(name, parameters);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    public static Field getField(Class<?> clazz, String name) throws NoSuchFieldException {
        String key = clazz.getName() + '#' + name;
        Field value = instance.fields.computeIfAbsent(key, k -> fieldOrNull(clazz, name));
        if (value == null) {
            throw new NoSuchFieldException(key);
        }
        return value;
    }

    private static Field fieldOrNull(Class<?> clazz, String name) {
        try {
            return clazz.getDeclaredField(name);
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

}
