package com.rpgme.plugin.util.nbtlib;

import org.bukkit.Bukkit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CompoundTagFile {

    // the file to use
    private final File file;
    // the TagCompound containing the files content
    private CompoundTag tag;

    public CompoundTagFile(File file) {
        this.file = file;
    }

    // two optional constructors:
    public CompoundTagFile(String folder, String name) {
        this(new File(folder, name));
    }

    public CompoundTagFile(String path) {
        this(new File(path));
    }


    public void clear() {
        tag = new CompoundTag();
    }

    public boolean exists() {
        return file.exists();
    }

    public CompoundTag getTag() {
        return tag;
    }

    public void setTag(CompoundTag tag) {
        this.tag = tag;
    }

    public void read() {
        NBTInputStream fileinputstream = null;
        try {
            // if the file exists we read it
            if (file.exists()) {

                fileinputstream = new NBTInputStream(new FileInputStream(file));
                tag = (CompoundTag) fileinputstream.readTag();

            }
            // else we create an empty TagCompound
            else {
                clear();
            }

        } catch (IOException | ClassCastException e) {
            Bukkit.getLogger().severe("Error while reading file '" + file + '\'');
            e.printStackTrace();
            clear();
        } finally {

            if (fileinputstream != null) {
                try {
                    fileinputstream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public void write() {
        try {

            if (!file.exists()) {
                file.createNewFile();
            }

            NBTOutputStream fileoutputstream = new NBTOutputStream(new FileOutputStream(file));
            fileoutputstream.writeTag(tag);
            fileoutputstream.close();

        } catch (IOException e) {
            Bukkit.getLogger().severe("Error while saving file '" + file + '\'');
            e.printStackTrace();
        } catch (NoClassDefFoundError e) {
            Bukkit.getLogger().severe("Error Java NoClassDefFoundError while saving file '" + file + '\'');
        }
    }

    public void delete() {
        file.delete();
    }


    /*
     * Delegate methods for easy CompoundTag access
     */

    public Map<String, Tag> getValue() {
        return getTag().getValue();
    }

    public int size() {
        return getTag().size();
    }

    public boolean containsKey(String key) {
        return getTag().containsKey(key);
    }

    public Set<Entry<String, Tag>> entrySet() {
        return getTag().entrySet();
    }

    public boolean isEmpty() {
        return getTag().isEmpty();
    }

    public Set<String> keySet() {
        return getTag().keySet();
    }

    public CompoundTag remove(String key) {
        return getTag().remove(key);
    }

    public Collection<Tag> values() {
        return getTag().values();
    }

    public CompoundTag putTag(String key, Tag value) {
        return getTag().putTag(key, value);
    }

    public Tag getTag(String key) {
        return getTag().getTag(key);
    }

    public CompoundTag putBoolean(String key, boolean value) {
        return getTag().putBoolean(key, value);
    }

    public boolean getBoolean(String key) {
        return getTag().getBoolean(key);
    }

    public CompoundTag putString(String key, String value) {
        return getTag().putString(key, value);
    }

    public String getString(String key) {
        return getTag().getString(key);
    }

    public CompoundTag putShort(String key, short value) {
        return getTag().putShort(key, value);
    }

    public short getShort(String key) {
        return getTag().getShort(key);
    }

    public CompoundTag putLong(String key, long value) {
        return getTag().putLong(key, value);
    }

    public long getLong(String key) {
        return getTag().getLong(key);
    }

    public CompoundTag putInteger(String key, int value) {
        return getTag().putInteger(key, value);
    }

    public int getInteger(String key) {
        return getTag().getInteger(key);
    }

    public CompoundTag putIntegerArray(String key, int[] value) {
        return getTag().putIntegerArray(key, value);
    }

    public int[] getIntegerArray(String key) {
        return getTag().getIntegerArray(key);
    }

    public CompoundTag putFloat(String key, float value) {
        return getTag().putFloat(key, value);
    }

    public float getFloat(String key) {
        return getTag().getFloat(key);
    }

    public CompoundTag putDouble(String key, double value) {
        return getTag().putDouble(key, value);
    }

    public double getDouble(String key) {
        return getTag().getDouble(key);
    }

    public CompoundTag putByte(String key, byte value) {
        return getTag().putByte(key, value);
    }

    public byte getByte(String key) {
        return getTag().getByte(key);
    }

    public CompoundTag putByteArray(String key, byte[] value) {
        return getTag().putByteArray(key, value);
    }

    public byte[] getByteArray(String key) {
        return getTag().getByteArray(key);
    }

}
