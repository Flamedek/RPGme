package com.rpgme.plugin.util.nbtlib;

public enum ReflectionNames {

    FIELD_ITEMSTACK_TAG("v"),

    METHOD_CRAFTENTITY_LOAD("f"),
    METHOD_CRAFTENTITY_SAVE("save"),

    NBT_TAG_COMPOUND_METHOD_SET("a"),
    NBT_TAG_COMPOUND_FIELD_MAP("x"),

    NBT_TAG_LIST_METHOD_ADD("a"),
    NBT_TAG_LIST_FIELD_LIST("c"),
    NBT_TAG_PRIMITIVE_FIELD_DATA("c"),
    NBT_TAG_STRING_FIELD_DATA("A"),

    NBT_TAG_BASE_METHOD_GET_TYPE_ID("b"),

    ;

    public final String value;

    ReflectionNames(String value) {
        this.value = value;
    }

}
