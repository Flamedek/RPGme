package com.rpgme.plugin.util.nbtlib;

//@formatter:off

/*
 * JNBT License
 *
 * Copyright (c) 2010 Graham Edgecombe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the JNBT team nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//@formatter:on

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * The <code>TAG_Compound</code> tag.
 *
 * @author Flamedek
 */
public class CompoundTag extends Tag {

    private final Map<String, Tag> map;

    public CompoundTag() {
        super("CompoundTag");
        map = new HashMap<>();
    }

    public CompoundTag(String name, Map<String, Tag> tagMap) {
        super(name);
        map = tagMap;
    }

    // delagate Map methods
    public void clear() {
        map.clear();
    }

    public boolean containsKey(String key) {
        return map.containsKey(key);
    }

    public Set<Entry<String, Tag>> entrySet() {
        return map.entrySet();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public Set<String> keySet() {
        return map.keySet();
    }

    public CompoundTag remove(String key) {
        map.remove(key);
        return this;
    }

    public int size() {
        return map.size();
    }

    public Collection<Tag> values() {
        return map.values();
    }

    public boolean hasValueOfType(String key, byte typeID) {
        Tag tag = map.get(key);
        return tag != null && tag.getTypeId() == typeID;
    }

    // Getters and Setters

    public CompoundTag putTag(String key, Tag value) {
        map.put(key, value);
        return this;
    }

    public Tag getTag(String key) {
        return map.get(key);
    }

    public ListTag getList(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return null;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_LIST) {
            return null;
        }
        return (ListTag) tag;
    }

    public CompoundTag getCompound(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return null;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_COMPOUND) {
            return null;
        }
        return (CompoundTag) tag;
    }

    public CompoundTag putBoolean(String key, boolean value) {
        return putByte(key, value ? (byte) 1 : (byte) 0);
    }

    public boolean getBoolean(String key) {
        return getByte(key) == (byte) 1;
    }

    public CompoundTag putString(String key, String value) {
        map.put(key, new StringTag(key, value));
        return this;
    }

    public String getString(String key) {
        return getString(key, "");
    }

    public String getString(String key, String def) {
        Tag tag = map.get(key);
        if (tag == null) {
            return def;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_STRING) {
            return def;
        }
        return (String) tag.getValue();
    }

    public CompoundTag putShort(String key, short value) {
        map.put(key, new ShortTag(key, value));
        return this;
    }

    public short getShort(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return 0;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_SHORT) {
            return 0;
        }
        return (Short) tag.getValue();
    }

    public CompoundTag putLong(String key, long value) {
        map.put(key, new LongTag(key, value));
        return this;
    }

    public long getLong(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return 0;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_LONG) {
            return 0;
        }
        return (Long) tag.getValue();
    }

    public CompoundTag putInteger(String key, int value) {
        map.put(key, new IntTag(key, value));
        return this;
    }

    public int getInteger(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return 0;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_INT) {
            return 0;
        }
        return (Integer) tag.getValue();
    }

    public CompoundTag putIntegerArray(String key, int[] value) {
        map.put(key, new IntArrayTag(key, value));
        return this;
    }

    public int[] getIntegerArray(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return null;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_INT_ARRAY) {
            return null;
        }
        return (int[]) tag.getValue();
    }

    public CompoundTag putFloat(String key, float value) {
        map.put(key, new FloatTag(key, value));
        return this;
    }

    public float getFloat(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return 0f;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_FLOAT) {
            return 0f;
        }
        return (Float) tag.getValue();
    }

    public CompoundTag putDouble(String key, double value) {
        map.put(key, new DoubleTag(key, value));
        return this;
    }

    public double getDouble(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return 0.0;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_DOUBLE) {
            return 0.0;
        }
        return (Double) tag.getValue();
    }

    public CompoundTag putByte(String key, byte value) {
        map.put(key, new ByteTag(key, value));
        return this;
    }

    public byte getByte(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return 0;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_BYTE) {
            return 0;
        }
        return (Byte) tag.getValue();
    }

    public CompoundTag putByteArray(String key, byte[] value) {
        map.put(key, new ByteArrayTag(key, value));
        return this;
    }

    public byte[] getByteArray(String key) {
        Tag tag = map.get(key);
        if (tag == null) {
            return null;
        }
        if (NBTUtils.getTypeCode(tag.getClass()) != NBTConstants.TYPE_BYTE_ARRAY) {
            return null;
        }
        return (byte[]) tag.getValue();
    }


    @Override
    public Map<String, Tag> getValue() {

        return map;
    }

    @Override
    public byte getTypeId() {
        return NBTConstants.TYPE_COMPOUND;
    }

    @Override
    public String toString() {

        final String name = getName();
        String append = "";
        if ((name != null) && !name.equals("")) {
            append = "(\"" + getName() + "\")";
        }
        final StringBuilder bldr = new StringBuilder();
        bldr.append("TAG_Compound" + append + ": " + map.size()
            + " entries\r\n{\r\n");
        for (final Entry<String, Tag> entry : map.entrySet()) {
            bldr.append("   "
                + entry.getValue().toString().replaceAll("\r\n", "\r\n   ")
                + "\r\n");
        }
        bldr.append("}");
        return bldr.toString();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {

        final int prime = 31;
        int result = super.hashCode();
        result = (prime * result) + ((map == null) ? 0 : map.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof CompoundTag)) {
            return false;
        }
        final CompoundTag other = (CompoundTag) obj;
        if (map == null) {
            return other.map == null;
        } else {
            return map.equals(other.map);
        }
    }


}
