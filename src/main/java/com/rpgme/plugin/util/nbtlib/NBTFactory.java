package com.rpgme.plugin.util.nbtlib;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


@SuppressWarnings({"rawtypes", "unchecked"})
public final class NBTFactory {

    private NBTFactory() {
    }

    /**
     * The version part used in class names for NMS and CraftBukkit
     */
    public static final String VERSION = Bukkit.getServer().getClass().getPackage().getName().substring(23);

    /**
     * The <strong>package name</strong> for NMS classes
     */
    public static final String NBT_PATH = "net.minecraft.nbt";

    /**
     * The base <strong>package name</strong> for CraftBukkit classes
     */
    public static final String CRAFT_PATH = "org.bukkit.craftbukkit." + VERSION;


    /**
     * Checks if an ItemStack has an NBT tag attached
     * @param item the item to check
     * @return true if item is not null and has an NBT tag
     */
    public static boolean hasTag(ItemStack item) {
        // Material air cannot hold a tag
        if (item == null || item.getType() == Material.AIR) {
            return false;
        }

        try {
            Class<?> craftClazz = Reflection.getClass(CRAFT_PATH + ".inventory.CraftItemStack");

            Object handle = Reflection.getMethod(craftClazz, "asNMSCopy", ItemStack.class).invoke(null, item);

            if (handle == null) {
                return false;
            }

            Field tagField = Reflection.getField(handle.getClass(), ReflectionNames.FIELD_ITEMSTACK_TAG.value);
            tagField.setAccessible(true);

            Object tag = tagField.get(handle);
            return tag != null;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public static CompoundTag getFrom(ItemStack item) {
        return get(item, false);
    }

    public static CompoundTag getOrEmpty(ItemStack item) {
        return get(item, true);
    }

    private static CompoundTag get(ItemStack item, boolean orEmpty) {
        if (item == null || item.getType() == Material.AIR) {
            return orEmpty ? new CompoundTag() : null;
        }

        try {
            Class<?> craftClazz = Reflection.getClass(CRAFT_PATH + ".inventory.CraftItemStack");

            Object handle = Reflection.getMethod(craftClazz, "asNMSCopy", ItemStack.class).invoke(null, item);
            if (handle == null) {
                return null;
            }

            Field tagField = Reflection.getField(handle.getClass(), ReflectionNames.FIELD_ITEMSTACK_TAG.value);
            tagField.setAccessible(true);

            Object tag = tagField.get(handle);

            return tag == null ? (orEmpty ? new CompoundTag() : null) : toCompoundTag(tag);

        } catch (Exception e) {
            e.printStackTrace();
            return orEmpty ? new CompoundTag() : null;
        }
    }

    public static CompoundTag copyOf(Entity entity) {
        if (entity == null) {
            return null;
        }

        try {
            Object nmsTag = Reflection.getClass(NBT_PATH + ".NBTTagCompound").newInstance();

            Class<?> craftClazz = Reflection.getClass(CRAFT_PATH + ".entity.CraftEntity");
            Object handle = Reflection.getMethod(craftClazz, "getHandle").invoke(craftClazz.cast(entity));

            /*
             * If this code breaks, it's likely to be here
             * As of v1_8_R3 method 'e' puts entity information into the nms tag
             *
             * As of v1_15_R1 not obfuscated and is called 'save'
             */
            Reflection.getMethod(handle.getClass(), ReflectionNames.METHOD_CRAFTENTITY_LOAD.value, nmsTag.getClass()).invoke(handle, nmsTag);

            return toCompoundTag(nmsTag);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    public static ItemStack setCompoundTag(ItemStack item, CompoundTag tag) {
        if (item == null || item.getType() == Material.AIR) {
            throw new NullPointerException("Item may not be null or air");
        }
        if (tag == null) {
            throw new NullPointerException("Compound tag may not be null");
        }

        try {
            Class<?> craftClazz = Reflection.getClass(CRAFT_PATH + ".inventory.CraftItemStack");

            Object handle = Reflection.getMethod(craftClazz, "asNMSCopy", ItemStack.class).invoke(null, item);

            Field tagField = Reflection.getField(handle.getClass(), ReflectionNames.FIELD_ITEMSTACK_TAG.value);
            tagField.setAccessible(true);

            tagField.set(handle, toNMSTag(tag));

            return (ItemStack) Reflection.getMethod(craftClazz, "asBukkitCopy", handle.getClass()).invoke(null, handle);
        } catch (Exception e) {
            e.printStackTrace();
            return item;
        }
    }

    public static void setEntityAttributes(Entity entity, CompoundTag tag) {
        if (entity == null) {
            throw new NullPointerException("Entity may not be null");
        }
        if (tag == null) {
            throw new NullPointerException("Compound tag may not be null");
        }

        try {

            Class<?> craftClazz = Reflection.getClass(CRAFT_PATH + ".entity.CraftEntity");
            Object handle = Reflection.getMethod(craftClazz, "getHandle").invoke(craftClazz.cast(entity));

            Object nmsTag = toNMSTag(tag);

            /*
             * If this code breaks, it's likely to be here
             * As of v1_8_R3 method 'f' reads entity information from the nms tag
             * and applies it to the entity
             */
            Reflection.getMethod(handle.getClass(), ReflectionNames.METHOD_CRAFTENTITY_LOAD.value, nmsTag.getClass()).invoke(handle, nmsTag);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * NMS to mirrored (for getters)
     */

    public static CompoundTag toCompoundTag(Object nmsCompound) throws Exception {

        Field mapField = Reflection.getField(nmsCompound.getClass(), ReflectionNames.NBT_TAG_COMPOUND_FIELD_MAP.value);
        mapField.setAccessible(true);
        Object map = mapField.get(nmsCompound);

        CompoundTag tag = new CompoundTag();

        Set<Entry> entryset = ((Map) map).entrySet();

        for (Entry e : entryset) {

            String key = (String) e.getKey();
            Object tagBase = e.getValue();

            tag.putTag(key, buildTag(key, tagBase));

        }
        return tag;
    }

    private static Tag buildTag(String key, Object tagBase) throws Exception {

        byte id = (byte) Reflection.getMethod(tagBase.getClass(), ReflectionNames.NBT_TAG_BASE_METHOD_GET_TYPE_ID.value).invoke(tagBase);

        if (id == NBTConstants.TYPE_END) {

            return new EndTag();

        } else if (id == NBTConstants.TYPE_COMPOUND) {

            return toCompoundTag(tagBase);

        } else if (id == NBTConstants.TYPE_LIST) {

            ListTag tag = new ListTag(key);

            Field listField = Reflection.getField(tagBase.getClass(), ReflectionNames.NBT_TAG_LIST_FIELD_LIST.value);
            listField.setAccessible(true);

            for (Object obj : (List) listField.get(tagBase)) {
                tag.add(buildTag("", obj));
            }
            return tag;

        } else if (id == NBTConstants.TYPE_STRING) {

            Field dataField = Reflection.getField(tagBase.getClass(), ReflectionNames.NBT_TAG_STRING_FIELD_DATA.value);
            dataField.setAccessible(true);
            Object data = dataField.get(tagBase);
            return new StringTag(key, (String) data);

        } else {

            Field dataField = Reflection.getField(tagBase.getClass(), ReflectionNames.NBT_TAG_PRIMITIVE_FIELD_DATA.value);
            dataField.setAccessible(true);
            Object data = dataField.get(tagBase);

            switch (id) {
                case NBTConstants.TYPE_BYTE:
                    return new ByteTag(key, (byte) data);
                case NBTConstants.TYPE_SHORT:
                    return new ShortTag(key, (short) data);
                case NBTConstants.TYPE_INT:
                    return new IntTag(key, (int) data);
                case NBTConstants.TYPE_LONG:
                    return new LongTag(key, (long) data);
                case NBTConstants.TYPE_FLOAT:
                    return new FloatTag(key, (float) data);
                case NBTConstants.TYPE_DOUBLE:
                    return new DoubleTag(key, (double) data);
                case NBTConstants.TYPE_BYTE_ARRAY:
                    return new ByteArrayTag(key, (byte[]) data);
                case NBTConstants.TYPE_INT_ARRAY:
                    return new IntArrayTag(key, (int[]) data);
                default:
                    return null;
            }
        }
    }

    /*
     * Mirrored to NMS (for setters)
     */

    public static Object toNMSTag(CompoundTag tagCompound) throws Exception {

        Object nmsTag = Reflection.getClass(NBT_PATH + ".NBTTagCompound").newInstance();
        Class nbtbaseClass = Reflection.getClass(NBT_PATH + ".NBTBase");

        for (Entry<String, Tag> entry : tagCompound.entrySet()) {
            String name = ReflectionNames.NBT_TAG_COMPOUND_METHOD_SET.value;
            Reflection.getMethod(nmsTag.getClass(), name, String.class, nbtbaseClass).invoke(nmsTag, entry.getKey(), buildNMSTag(entry.getValue()));

        }

        return nmsTag;
    }

    private static Object buildNMSTag(Tag tag) throws Exception {

        byte id = tag.getTypeId();

        if (id == NBTConstants.TYPE_COMPOUND) {

            return toNMSTag((CompoundTag) tag);

        } else if (id == NBTConstants.TYPE_LIST) {

            Class listClazz = Reflection.getClass(NBT_PATH + ".NBTTagList");
            Class nbtbaseClass = Reflection.getClass(NBT_PATH + ".NBTBase");

            Object nmsList = listClazz.newInstance();
            for (Tag child : ((ListTag) tag).getValue()) {
                String name = ReflectionNames.NBT_TAG_LIST_METHOD_ADD.value;
                Reflection.getMethod(listClazz, name, nbtbaseClass).invoke(nmsList, buildNMSTag(child));
            }
            return nmsList;
        }

        StringBuilder sb = new StringBuilder(NBT_PATH).append(".NBTTag");

        switch (tag.getTypeId()) {
            case NBTConstants.TYPE_END:
                sb.append("End");
                break;
            case NBTConstants.TYPE_BYTE:
                sb.append("Byte");
                break;
            case NBTConstants.TYPE_SHORT:
                sb.append("Short");
                break;
            case NBTConstants.TYPE_INT:
                sb.append("Int");
                break;
            case NBTConstants.TYPE_LONG:
                sb.append("Long");
                break;
            case NBTConstants.TYPE_FLOAT:
                sb.append("Float");
                break;
            case NBTConstants.TYPE_DOUBLE:
                sb.append("Double");
                break;
            case NBTConstants.TYPE_BYTE_ARRAY:
                sb.append("ByteArray");
                break;
            case NBTConstants.TYPE_STRING:
                sb.append("String");
                break;
            case NBTConstants.TYPE_INT_ARRAY:
                sb.append("IntArray");
                break;
        }

        Class<?> valueType = NBTUtils.getValueTypeClass(tag.getTypeId());
        Class<?> clazz = Reflection.getClass(sb.toString());
        Constructor<?> constructor = clazz.getDeclaredConstructor(valueType);
        constructor.setAccessible(true);
        return constructor.newInstance(tag.getValue());
    }


}
