@file:JvmName("Materials")
@file:Suppress("NOTHING_TO_INLINE")

package com.rpgme.plugin.util

import org.bukkit.Material
import org.bukkit.Material.*

// Items

// Blocks
val WOOD_TYPES = arrayOf("oak", "spruce", "birch", "jungle", "acacia", "dark_oak")

fun Material.isLog() = anyOf(OAK_LOG, SPRUCE_LOG, BIRCH_LOG, JUNGLE_LOG, ACACIA_LOG, DARK_OAK_LOG)
fun Material.isBush() = anyOf(OAK_LEAVES, SPRUCE_LEAVES, BIRCH_LEAVES, JUNGLE_LEAVES, ACACIA_LEAVES, DARK_OAK_LEAVES)
fun Material.isButton() = anyOf(STONE_BUTTON, OAK_BUTTON, SPRUCE_BUTTON, BIRCH_BUTTON, JUNGLE_BUTTON, ACACIA_BUTTON, DARK_OAK_BUTTON)

fun isSameTreeType(one: Material, two: Material): Boolean {
    return when (one) {
        MUSHROOM_STEM -> two.anyOf(MUSHROOM_STEM, RED_MUSHROOM_BLOCK, BROWN_MUSHROOM_BLOCK)
        RED_MUSHROOM_BLOCK -> two.anyOf(MUSHROOM_STEM, RED_MUSHROOM_BLOCK)
        BROWN_MUSHROOM_BLOCK -> two.anyOf(MUSHROOM_STEM, BROWN_MUSHROOM_BLOCK)
        else -> one.name.removePrefix("STRIPPED_").substringBefore('_') ==
                two.name.removePrefix("STRIPPED_").substringBefore('_')
    }
}

// Tools
fun Material.isHoe() = matchSuffix("_HOE")

fun Material.isPickaxe() = matchSuffix("_PICKAXE")
fun Material.isAxe() = matchSuffix("_AXE")
fun Material.isShovel() = matchSuffix("_SHOVEL")
fun Material.isSword() = matchSuffix("_SWORD")
fun Material.isHelmet() = matchSuffix("_HOE")
fun Material.isChestplate() = matchSuffix("_CHESTPLATE")
fun Material.isLeggings() = matchSuffix("_LEGGINGS")
fun Material.isBoots() = matchSuffix("_BOOTS")
fun Material.isArrow() = anyOf(ARROW, SPECTRAL_ARROW, TIPPED_ARROW)

// util
private inline fun Material.matchPrefix(prefix: String) = name.startsWith(prefix)

private inline fun Material.matchSuffix(suffix: String) = name.endsWith(suffix)
private inline fun Material.match(prefix: String, suffix: String) = name.startsWith(prefix) && name.endsWith(suffix)

private inline fun Material.anyOf(type: Material) = this == type
private inline fun Material.anyOf(type1: Material, type2: Material) = this == type1 || this == type2
private inline fun Material.anyOf(type1: Material, type2: Material, type3: Material) = this == type1 || this == type2 || this == type3
private inline fun Material.anyOf(vararg types: Material) = this in types

@JvmName("anyIn")
private inline fun Material.anyOf(types: Array<Material>) = this in types

@JvmName("anyIn")
private inline fun Material.anyOf(types: Iterable<String>) = name in types