package com.rpgme.plugin.util;

import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.util.effect.EnchantmentGlow;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.Tag;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 */
public class RPGItems implements ListenerModule {

    public static final String TAG_SKILLTOMB_SKILL = "skillTomb_skill";
    public static final String TAG_SKILLTOMB_EXP = "skillTomb_exp";
    public static final String TAG_EXPTOMB_EXP = "expTomb_exp";
    public static final String TAG_EXPTOMB_MINLEVEL = "expTomb_minlevel";
    public static final String TAG_EXPTOMB_SKILLS = "expTomb_skills";

    public static final String TAG_SKILLGEM = "SkillGem";

    public static ItemStack createSkillTomb(Skill skill, int exp) {
        String name = "&7" + skill.getDisplayName() + " Tomb &f" + String.format("%,d", exp);
        String lore = "Right-Click to spend the xp stored in this item.";
        Tag nbtTag = new CompoundTag().putString(TAG_SKILLTOMB_SKILL, skill.getName())
                .putInteger(TAG_SKILLTOMB_EXP, exp);

        return EnchantmentGlow.addGlow(ItemUtils.create(nbtTag, Material.BOOK, name, lore));
    }

    public static ItemStack createExpTomb(int exp, int minlevel) {
        return createExpTomb(exp, minlevel, null);
    }

    public static ItemStack createExpTomb(int exp, int minlevel, Iterable<Skill> pickableSkills) {
        String skills = joinSkillNames(pickableSkills);
        String name = "&7Exp Tomb &f" + String.format("%,d", exp);
        String lore = "Right-Click to spend the xp stored in this item.";

        CompoundTag nbtTag = new CompoundTag()
                .putInteger(TAG_EXPTOMB_EXP, exp)
                .putInteger(TAG_EXPTOMB_MINLEVEL, minlevel);

        if (!skills.isEmpty()) {
            lore += "\n&e" + skills.replace(",", "\n&e");
            nbtTag.putString(TAG_EXPTOMB_SKILLS, skills);
        }

        return EnchantmentGlow.addGlow(ItemUtils.create(nbtTag, Material.BOOK, name, lore));
    }

//    public static ItemStack createSkillGem() {
//        return EnchantmentGlow.addGlow(ItemUtils.create(new ByteTag(TAG_SKILLGEM, (byte)1), Material.EMERALD, null,
//                "&6Skill Gem &7(Right-Click)",
//                "Levels up 1 random skill to the &lnext &llevel!"));
//    }

    private static String joinSkillNames(Iterable<Skill> it) {
        if (it == null)
            return "";

        StringBuilder builder = new StringBuilder();
        it.forEach((skill -> {
            if (builder.length() > 0)
                builder.append(",");
            builder.append(skill.getName());
        }));
        return builder.toString();
    }


}
