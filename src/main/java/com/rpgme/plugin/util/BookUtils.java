package com.rpgme.plugin.util;

import com.rpgme.plugin.RPGme;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 07/08/2016.
 */
public class BookUtils {

    /**
     * Opens a book gui for a player with the given ComponentBuilders as pages.
     * ComponentBuilder features ChatColors and hover/click events.
     * The player does not actually have to be holding or using a book item, this method just opens the GUI for them.
     */
    @SuppressWarnings("ConstantConditions")
    public static void openBook(Player player, ComponentBuilder... pages) {
        // create book item
        BookMeta meta = (BookMeta) player.getServer().getItemFactory().getItemMeta(Material.WRITTEN_BOOK);
        for (ComponentBuilder builder : pages) {
            meta.spigot().addPage(builder.create());
        }

        ItemStack bookItem = new ItemStack(Material.WRITTEN_BOOK);
        bookItem.setItemMeta(meta);

        // set the book
        int slot = player.getInventory().getHeldItemSlot();
        final ItemStack currentItem = player.getInventory().getItem(slot);
        player.getInventory().setItem(slot, bookItem);

        player.sendPluginMessage(RPGme.getInstance(), "MC|BOpen", new byte[256]);

        // reset previous item
        player.getInventory().setItem(slot, currentItem);
    }

    /**
     * Sets the given ComponentBuilders as meta for a book ItemStack.
     * ComponentBuilder features ChatColors and hover/click events.
     */
    @SuppressWarnings("ConstantConditions")
    public void setBookPages(ItemStack item, ComponentBuilder... pages) {
        try {
            BookMeta meta = (BookMeta) item.getItemMeta();
            List<BaseComponent[]> contents = new ArrayList<>(pages.length);
            for (ComponentBuilder page : pages) {
                contents.add(page.create());
            }
            meta.spigot().setPages(contents);
            item.setItemMeta(meta);

        } catch (ClassCastException e) {
            throw new IllegalArgumentException("ItemStack does not have BookMeta (incompatible type)");
        }
    }

}
