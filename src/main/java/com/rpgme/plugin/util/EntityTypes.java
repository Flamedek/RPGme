package com.rpgme.plugin.util;

import org.bukkit.entity.*;

import java.util.EnumSet;
import java.util.Set;

public class EntityTypes {

    public static final Set<EntityType> hostileEntities = EnumSet.of(EntityType.SLIME, EntityType.PHANTOM);

    public static final Set<EntityType> netherEntities = EnumSet.of(EntityType.WITHER_SKELETON, EntityType.BLAZE, EntityType.MAGMA_CUBE, EntityType.GHAST, EntityType.PIGLIN, EntityType.PIGLIN_BRUTE,EntityType.HOGLIN, EntityType.ZOGLIN,  EntityType.ZOMBIFIED_PIGLIN);

    public static final Set<EntityType> bossEntities = EnumSet.of(EntityType.ELDER_GUARDIAN, EntityType.WITHER, EntityType.ENDER_DRAGON, EntityType.WARDEN);

    public static boolean isHostile(Entity e) {
        if (e instanceof Monster) {
            return true;
        }
        if (e instanceof Wolf) {
            return ((Wolf) e).isAngry();
        }
        if (e instanceof IronGolem) {
            return !((IronGolem) e).isPlayerCreated();
        }
        return hostileEntities.contains(e.getType());
    }

    public static boolean isPassive(Entity e) {
        return !isHostile(e) && (e instanceof Creature || e instanceof Ambient);
    }

    public static boolean isNetherMob(Entity e) {
        return netherEntities.contains(e.getType());
    }

    public static boolean isBoss(Entity e) {
        return bossEntities.contains(e.getType());
    }

    public static boolean isHostile(EntityType type) {
        Class<?> clazz = type.getEntityClass();
        if (clazz == null) {
            return false;
        }
        return Monster.class.isAssignableFrom(clazz) || hostileEntities.contains(type);
    }

    public static boolean isPassive(EntityType type) {
        Class<?> clazz = type.getEntityClass();
        if (clazz == null) {
            return false;
        }
        return !isHostile(type) && (Creature.class.isAssignableFrom(clazz) || Ambient.class.isAssignableFrom(clazz));
    }

    public static boolean isNetherMob(EntityType type) {
        return netherEntities.contains(type);
    }

    public static boolean isBoss(EntityType type) {
        return bossEntities.contains(type);
    }

}
