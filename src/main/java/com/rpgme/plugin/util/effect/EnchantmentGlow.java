package com.rpgme.plugin.util.effect;

import com.rpgme.plugin.RPGme;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;

public class EnchantmentGlow extends Enchantment {

    private static Enchantment glow;

    static {
        try {
            Field field = Enchantment.class.getDeclaredField("acceptingNew");
            field.setAccessible(true);
            field.set(null, Boolean.TRUE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        NamespacedKey key = new NamespacedKey(RPGme.getInstance(), "glow");
        glow = new EnchantmentGlow(key);
        if (getByKey(key) == null) {
            registerEnchantment(glow);
        }
    }

    private EnchantmentGlow(NamespacedKey key) {
        super(key);
    }

    @Override
    public boolean isTreasure() {
        return false;
    }

    @Override
    public boolean isCursed() {
        return false;
    }

    @Override
    public String getName() {
        return "Glowy";
    }

    @Override
    public boolean canEnchantItem(ItemStack item) {
        return true;
    }

    @Override
    public boolean conflictsWith(Enchantment other) {
        return false;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return null;
    }

    @Override
    public int getMaxLevel() {
        return 1;
    }

    @Override
    public int getStartLevel() {
        return 1;
    }

    public static Enchantment getGlowEnchantment() {
        return glow;
    }

    public static ItemStack addGlow(ItemStack itemStack) {
        itemStack.addUnsafeEnchantment(getGlowEnchantment(), 1);
        return itemStack;
    }

    public static ItemStack removeGlow(ItemStack itemStack) {
        itemStack.removeEnchantment(getGlowEnchantment());
        return itemStack;
    }
}

