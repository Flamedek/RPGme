package com.rpgme.plugin.util.cooldown;

public interface Cleanable {

    void cleanUp();

}
