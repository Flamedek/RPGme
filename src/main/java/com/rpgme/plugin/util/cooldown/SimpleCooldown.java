package com.rpgme.plugin.util.cooldown;

import com.google.common.collect.Maps;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.Map;
import java.util.UUID;

/**
 * A utility timer class that can store cooldown timings for abilities or other needs.
 * This variant is to waitsa specified amount of milliseconds.
 */
public class SimpleCooldown implements Cooldown {

    protected final Map<UUID, Long> map = Maps.newHashMap();
    protected final long duration;

    public SimpleCooldown(Plugin plugin) {
        this(plugin, -1);
    }

    public SimpleCooldown(Plugin plugin, long duration) {
        CooldownCleaner.register(this, plugin);
        this.duration = duration;
    }

    @Override
    public synchronized boolean isOnCooldown(Player p) {
        Long time = map.getOrDefault(p.getUniqueId(), 0L);
        if (time < System.currentTimeMillis()) {
            map.remove(p.getUniqueId());
            return false;
        }
        return true;
    }

    @Override
    public synchronized void add(Player p) {
        if (duration < 0)
            throw new UnsupportedOperationException("No default duration has been set. Either supply a duration in the constructor or use register(Player, long).");
        map.put(p.getUniqueId(), System.currentTimeMillis() + duration);
    }

    @Override
    public synchronized void add(Player p, long duration) {
        map.put(p.getUniqueId(), System.currentTimeMillis() + duration);
    }

    @Override
    public synchronized boolean remove(Player p) {
        return map.remove(p.getUniqueId()) != null;
    }

    @Override
    public synchronized long getMillisRemaining(Player p) {
        return getMillisRemaining(map.get(p.getUniqueId()));
    }

    private synchronized long getMillisRemaining(Long since) {
        if (since == null)
            return 0;
        return since - System.currentTimeMillis();
    }

    @Override
    public synchronized void cleanUp() {
        map.entrySet().removeIf(uuidLongEntry -> getMillisRemaining(uuidLongEntry.getValue()) < 1);
    }

}
