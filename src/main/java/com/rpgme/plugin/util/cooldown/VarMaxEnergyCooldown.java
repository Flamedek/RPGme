package com.rpgme.plugin.util.cooldown;

import com.google.common.collect.Maps;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class VarMaxEnergyCooldown implements Cooldown {

    private final Map<UUID, Entry> map = Maps.newHashMap();
    private final int energyPerSec, energyCost;

    public VarMaxEnergyCooldown(Plugin plugin, int enerypersec, int energyCost) {
        CooldownCleaner.register(this, plugin);
        this.energyPerSec = enerypersec;
        this.energyCost = energyCost;
    }

    @Override
    public synchronized boolean isOnCooldown(Player p) {
        Entry e = map.get(p.getUniqueId());
        return e != null && e.getCurrentEnergy() < energyCost;
    }

    @Override
    public void add(Player p) {
        throw new UnsupportedOperationException("Use register(Player, int) instead.");
    }

    @Override
    public void add(Player p, long maxenergy) {
        Entry entry;
        synchronized (this) {
            entry = map.get(p.getUniqueId());
            if (entry == null) {
                entry = new Entry((int) maxenergy);
                map.put(p.getUniqueId(), entry);
            } else {
                entry.maxEnergy = (int) maxenergy;
            }
        }
        entry.onUse();
    }

    @Override
    public synchronized boolean remove(Player p) {
        return map.remove(p.getUniqueId()) != null;
    }

    @Override
    public synchronized long getMillisRemaining(Player p) {
        Entry e = map.get(p.getUniqueId());
        if (e == null)
            return 0L;
        double current = e.getCurrentEnergy();
        if (current >= energyCost)
            return 0L;

        return (long) ((energyCost - current) / energyPerSec * 1000L);
    }

    @Override
    public synchronized void cleanUp() {
        Iterator<Map.Entry<UUID, Entry>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry e = it.next().getValue();
            if (e.energy == e.maxEnergy)
                it.remove();

        }
    }

    public class Entry {

        private long lastUsed;
        private double energy;
        private int maxEnergy;

        public Entry(int maxEnergy) {
            this.lastUsed = System.currentTimeMillis();
            this.maxEnergy = maxEnergy;
            this.energy = maxEnergy;
        }

        double getCurrentEnergy() {
            return Math.min(maxEnergy, (energy + ((System.currentTimeMillis() - lastUsed) / 1000.0 * energyPerSec)));
        }

        void onUse() {
            energy = getCurrentEnergy() - energyCost;
            lastUsed = System.currentTimeMillis();
        }

    }

}
