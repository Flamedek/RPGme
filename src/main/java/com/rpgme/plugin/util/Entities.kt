@file:JvmName("Entities")

package com.rpgme.plugin.util

import org.bukkit.block.BlockState
import org.bukkit.block.data.Ageable
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.Damageable
import kotlin.math.min

fun ItemStack.getDamage() = (itemMeta as? Damageable)?.damage ?: 0

fun ItemStack.setDamage(damage: Int) {
    updateItemMeta<Damageable> { damageable ->
        damageable.damage = damage
    }
}

fun BlockState.setAge(age: Int) {
    updateBlockData<Ageable> {
        it.age = min(it.maximumAge, age)
    }
}

inline fun <reified T> ItemStack.updateItemMeta(block: (T) -> Unit) {
    val meta = itemMeta
    if (meta is T) {
        block(meta)
        itemMeta = meta
    }
}

inline fun <reified T> BlockState.updateBlockData(block: (T) -> Unit) {
    val data = blockData
    if (data is T) {
        block(data)
        blockData = data
    }
}


