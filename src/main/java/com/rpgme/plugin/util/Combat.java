package com.rpgme.plugin.util;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.util.math.ScaledValue;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Tameable;

public class Combat {

    private static final ScaledValue
            healthExpScale = new ScaledValue(25, 1, 250, 5),
            distanceScale = new ScaledValue(0, 2, 45 * 45, 5);

    private static final double pvpMultiplier, spawnerMultiplier;

    static {
        pvpMultiplier = RPGme.getInstance().getConfig().getDouble("PvP exp", 0.25);
        spawnerMultiplier = RPGme.getInstance().getConfig().getDouble("Mobspawner exp", 0.2);
    }

    public static int getDamageAttribute(Material mat) {
        switch (mat) {

            case WOODEN_SWORD:
                return 4;
            case STONE_SWORD:
                return 5;
            case IRON_SWORD:
                return 6;
            case GOLDEN_SWORD:
                return 4;
            case DIAMOND_SWORD:
                return 7;
            default:
                return 0;
        }

    }

    public static float getArcheryDamageExp(LivingEntity e, double damage, double distanceSquared) {
        return (float) (getAttackDamageExp(e, damage) * distanceScale.scale(distanceSquared));
    }

    @SuppressWarnings("ConstantConditions")
    public static float getAttackDamageExp(LivingEntity e, double damage) {
        double maxHealth = e.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        float totalExp = getKillExp(e);
        return (float) (damage / maxHealth * totalExp);
    }

    public static float getKillExp(LivingEntity e) {
        double base = ExpTables.getExpRewardForKilling(e);
        return (float) (base * getExpFactor(e));
    }

    @SuppressWarnings("ConstantConditions")
    public static double getExpFactor(LivingEntity e) {
        if (e instanceof Tameable && ((Tameable) e).getOwner() != null) {
            return 0;
        }

        double maxHealth = e.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        double factor = healthExpScale.scale(maxHealth);

        // multiply for pvp
        if (e.getType() == EntityType.PLAYER) {
            factor *= pvpMultiplier;
        }
        // multiply for mobspawner
        else if (e.hasMetadata("MonsterSpawner")) {
            factor *= spawnerMultiplier;
        }
        return factor;
    }

}
