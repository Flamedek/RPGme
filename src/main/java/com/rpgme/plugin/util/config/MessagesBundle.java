package com.rpgme.plugin.util.config;

import com.rpgme.plugin.RPGme;
import org.bukkit.ChatColor;

import java.text.MessageFormat;
import java.util.IllegalFormatException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 */
public class MessagesBundle implements Cloneable {

    public static final String MESSAGE_UNKNOWN = "<unknown>";

    private LinkedHashMap<String, String> map;

    public MessagesBundle() {
        this(64);
    }

    public MessagesBundle(int capacaty) {
        map = new LinkedHashMap<>(capacaty, 0.6f);
    }

    public MessagesBundle(LinkedHashMap<String, String> map) {
        this.map = map;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            entry.setValue(ChatColor.translateAlternateColorCodes('&', entry.getValue()));
        }
    }

    public LinkedHashMap<String, String> getValues() {
        return map;
    }

    public String getMessage(String key) {
        return getMessageOr(key, MESSAGE_UNKNOWN);
    }

    public String getMessageOr(String key, String def) {
        String message = map.get(key);
        if (message == null && Objects.equals(def, MESSAGE_UNKNOWN)) {
            RPGme.getInstance().getLogger().warning("Unable to find message for key '" + key + "'");
        }
        return message != null ? message : def;
    }

    public String getMessage(String key, Object... args) {
        String message = getMessage(key);
        try {
            return MessageFormat.format(message, args);
        } catch (IllegalFormatException e) {
            return message;
        }
    }

    public void put(String key, String value) {
        map.put(key, ChatColor.translateAlternateColorCodes('&', value));
    }

    public BundleSection getSection(String name) {
        if (!name.isEmpty()) name += "_";
        return new BundleSection(this, name);
    }

    @Override
    public MessagesBundle clone() {
        return new MessagesBundle(new LinkedHashMap<>(map));
    }
}
