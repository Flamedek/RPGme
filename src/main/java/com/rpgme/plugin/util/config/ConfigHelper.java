package com.rpgme.plugin.util.config;

import org.bukkit.plugin.Plugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class exists mainly for legacy reasons from the v1 -> v2 change.
 * It is a utility to load messages embedded in the plugin jar to put them in the BundleBuilder at runtime.
 * Created by Robin on 02/07/2016.
 */
public class ConfigHelper {

    private static Properties messages = new Properties();

    private static void loadMessages(InputStream stream) {
        try {
            messages.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException ignore) {
            }
        }
    }

    public static void prepare(Plugin plugin) {
        loadMessages(plugin.getResource("messages.properties"));
    }

    public static String getBundledMessage(Class<?> owner, String suffix) {
        String name = owner.getSimpleName().toLowerCase();
        return messages.getProperty(name + suffix);
    }

    public static String getBundledMessage(String key) {
        return messages.getProperty(key);
    }

}
