package com.rpgme.plugin.util.config;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class YamlFile {

    private final File file;
    public FileConfiguration data;

    public YamlFile(File file) {
        this.file = file;
    }

    public YamlFile(Plugin plugin, String name) {
        this.file = new File(plugin.getDataFolder(), name);
    }

    public YamlFile(String path) {
        this.file = new File(path);
    }

    public File getFile() {
        return file;
    }

    public FileConfiguration getData() {
        if (data == null) clear();
        return data;
    }


    public void clear() {
        data = new YamlConfiguration();
    }

    public void read() {
        data = file.exists() ? YamlConfiguration.loadConfiguration(file) : new YamlConfiguration();
    }

    public void write() {
        try {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if (!file.exists()) {
                file.createNewFile();
            }
            data.save(file);
        } catch (IOException e) {
            System.out.println("IOException! Could not save [" + file.getName() + "] to disk. All data in this Configuration is lost.");
            e.printStackTrace();
        }


    }

}
