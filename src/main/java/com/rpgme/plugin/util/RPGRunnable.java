package com.rpgme.plugin.util;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 08/10/2016.
 */
public abstract class RPGRunnable extends BukkitRunnable {

    private static List<WeakReference<RPGRunnable>> runnables = new ArrayList<>();

    /**
     * The method that has to be called when the Plugin disables.
     * This ensures that the running instances of this class are notified and can clean up accordingly.
     */
    public static void finishAll() {
        for (WeakReference<RPGRunnable> reference : runnables) {
            RPGRunnable runnable = reference.get();
            if (runnable != null) {
                runnable.finish();
                Bukkit.getScheduler().cancelTask(runnable.getTaskId());
            }
        }
        runnables.clear();
    }

    @Override
    public synchronized void cancel() throws IllegalStateException {
        super.cancel();
        finish();

        runnables.removeIf(rpgRunnableWeakReference -> rpgRunnableWeakReference.get() == this);
    }

    /**
     * Callback that is called in two cases:
     * 1.  when cancel() is invoked
     * 2. when the server shuts down while the task is scheduled.
     * This gives you a chance to restore changes that may have happened.
     * Works for running task timers (not delayed tasks).
     */
    public void finish() {

    }

    @Override
    public synchronized BukkitTask runTaskTimer(Plugin plugin, long delay, long period) throws IllegalArgumentException, IllegalStateException {
        runnables.add(new WeakReference<>(this));
        return super.runTaskTimer(plugin, delay, period);
    }

    @Override
    public synchronized BukkitTask runTaskTimerAsynchronously(Plugin plugin, long delay, long period) throws IllegalArgumentException, IllegalStateException {
        runnables.add(new WeakReference<>(this));
        return super.runTaskTimerAsynchronously(plugin, delay, period);
    }

}
