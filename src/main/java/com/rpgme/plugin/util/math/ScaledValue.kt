package com.rpgme.plugin.util.math

import com.rpgme.plugin.util.CoreUtils
import com.rpgme.plugin.util.StringUtils

enum class EdgeType {
    BOUND, CLIP, SCALE
}

class ScaledValue @JvmOverloads constructor(
        @JvmField val minLevel: Int,
        @JvmField val minvalue: Double,
        @JvmField val maxLevel: Int,
        @JvmField val maxvalue: Double,
        private val bottom: EdgeType = EdgeType.BOUND,
        private val top: EdgeType = EdgeType.BOUND
) {

    fun readableScale(level: Double): String {
        return StringUtils.readableDecimal(scale(level))
    }

    /**
     * Calculate the value scaled for the given [level].
     * Values are clamped between [minLevel] and [maxLevel].
     */
    fun scale(level: Int): Double = scale(level.toDouble())

    /**
     * Calculate the value scaled for the given [level].
     * Values are clamped between [minLevel] and [maxLevel].
     */
    fun scale(level: Double): Double {
        if (bottom == EdgeType.CLIP && level < minLevel) {
            return 0.0
        }
        if (top == EdgeType.CLIP && level > maxLevel) {
            return 0.0
        }

        val x = (maxvalue - minvalue) / (maxLevel - minLevel)
        var value = (level - minLevel) * x + minvalue

        if (bottom == EdgeType.BOUND) {
            value = Math.max(minvalue, value)
        }
        if (top == EdgeType.BOUND) {
            value = Math.min(maxvalue, value)
        }
        return value
    }

    /**
     * If value represents a percentage chance (0-100), roll a boolean random value
     */
    fun isRandomChance(level: Int): Boolean = isRandomChance(level.toDouble())

    /**
     * If value represents a percentage chance (0-100), roll a boolean random value
     */
    fun isRandomChance(level: Double): Boolean {
        val value = scale(level)
        return value >= 100 || (value > 0.0 && CoreUtils.random.nextDouble() < value / 100)
    }

}