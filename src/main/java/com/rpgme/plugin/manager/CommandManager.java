package com.rpgme.plugin.manager;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.command.CommandHelp;
import com.rpgme.plugin.command.CoreCommand;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class CommandManager implements Module {

    private final RPGme plugin;
    private final CommandMap commandMap;

    private final List<CoreCommand> commands = new ArrayList<>();

    public CommandManager(RPGme plugin) {
        this.plugin = plugin;
        commandMap = initCommandMap();
    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {
        commands.forEach(Module::onDisable);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        messages.setPath("")
                .addValue("err_playernotfound", "&cSorry, player ''{0}'' is not online")
                .addValue("err_nopermission", "&cYou do not have permission to do that.")
                .addValue("err_notunlocked", "&cYou do not meet the requirements to do that: {0}");
        commands.forEach((cmd) -> cmd.createConfig(config, messages));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        commands.forEach((cmd) -> cmd.onLoad(config, messages));
    }

    public void register(CoreCommand command) {
        if (command == null) {
            return;
        }

        unregister(command);
        try {
            command.onEnable();
            commands.add(command);
            commandMap.register(plugin.getName().toLowerCase(), command);
        } catch (Exception e) {
            plugin.getLogger().log(Level.SEVERE, "Error while enabling command " + command.getName(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public boolean unregister(CoreCommand command) {
        if (commands.remove(command)) {

            try {
                // hacky way to also unregister previous aliases. Otherwise they won't display correctly after reload (though still work).
                Field field = SimpleCommandMap.class.getDeclaredField("knownCommands");
                field.setAccessible(true);
                Map<String, Command> knownCommands = (Map<String, Command>) field.get(commandMap);
                command.getAliases().forEach(knownCommands::remove);
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }

            command.onDisable();
            command.unregister(commandMap);
            return true;
        }
        return false;
    }

    private String colorLight() {
        return plugin.getSkillManager().colorLight();
    }

    private String colorDark() {
        return plugin.getSkillManager().colorDark();
    }

    private String colorAccent() {
        return plugin.getSkillManager().colorAccent();
    }

    public void printCommandHelp(CoreCommand command, CommandSender sender) {
        StringBuilder builder = new StringBuilder();
        String SHORT_LINE = "\n                &8----",
            LONG_LINE = "&7&m" + org.apache.commons.lang.StringUtils.repeat("-", 60),
            PADDING = org.apache.commons.lang.StringUtils.repeat(" ", 16);

        String title = colorAccent() + "Command &l/" + command.getName();
        builder.append(getTitleLine(title, 55)).append('\n');
        if (!command.getAliases().isEmpty()) {
            builder.append("&aAlias");
            if (command.getAliases().size() > 1)
                builder.append("es      &7");
            else {
                builder.append("         &7");
            }

            for (String alias : command.getAliases()) {
                builder.append(" /").append(alias);
            }
            builder.append(SHORT_LINE);
        }
        builder.append("\n");

        String description = command.getDescription();
        if (!StringUtils.isEmpty(description)) {
            builder.append("\n&aDescription  &f").append(StringUtils.breakAndInsert(description, org.apache.commons.lang.StringUtils.repeat(" ", 16), false, " ", 72))
                    .append(SHORT_LINE);
        }

        CommandHelp help = command.getCommandHelp();
        if (!help.isEmpty(sender)) {

            String firstPadding = "&aUsage        ";
            boolean first = true;

            for (int i = 0; i < help.size(); i++) {

                String usage = help.getUsages().get(i);
                String desc = help.getDescriptions().get(i);
                String permission = help.getPermissions().get(i);

                if (usage.isEmpty() || (!permission.isEmpty() && !sender.hasPermission(permission))) {
                    continue;
                }
                builder.append('\n');

                if (first) {
                    builder.append(firstPadding);
                    first = false;
                } else {
                    builder.append(PADDING);
                }

                builder.append(colorDark()).append(usage).append('\n').append(PADDING);
                if (!desc.isEmpty()) {
                    builder.append("&f    ").append(desc);
                }
            }
        }

        builder.append('\n').append(LONG_LINE);
        sender.sendMessage(StringUtils.colorize(builder.toString()));
    }

    public void printHelp(CommandSender sender) {
        String title = colorAccent() + "&l" + plugin.getName() + colorAccent() + " commands";
        ComponentBuilder builder = new ComponentBuilder(StringUtils.colorize(getTitleLine(title, 55)));

        for (CoreCommand command : commands) {

            if (!command.hasPermission(sender, false)) {
                continue;
            }

            String commandLine = '\n' + colorDark() + "    /" + command.getName() + " " + ChatColor.GRAY;
            for (String alias : command.getAliases()) {
                commandLine += " /" + alias;
            }
            String commandHover = StringUtils.breakAndInsert(command.getDescription(), "&f", true, " ", 45);

            builder.append(commandLine).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder(StringUtils.colorize(commandHover)).create()))
                    .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + command.getName() + " help"));

            CommandHelp usages = command.getCommandHelp();
            for (int i = 0; i < usages.size(); i++) {

                if (!usages.getPermissions().get(i).isEmpty() && !sender.hasPermission(usages.getPermissions().get(i))) {
                    continue;
                }

                String usageLine = "\n        " + colorLight() + usages.getUsages().get(i);
                String usageHover = StringUtils.breakAndInsert(usages.getDescriptions().get(i), "&f", true, " ", 45);

                builder.append(usageLine).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                        new ComponentBuilder(StringUtils.colorize(usageHover)).create()));
            }
        }

        String footer = "\n&f&o    * Hover over commands above to see it's description\n" +
                "&f&o    * Add 'help' or '?' in any command for detailed information.\n&7&m" +
            org.apache.commons.lang.StringUtils.repeat("-", 60);
        builder.append(StringUtils.colorize(footer), ComponentBuilder.FormatRetention.NONE);
        // send
        sendMessage(sender, builder.create());
    }

    public static void sendMessage(CommandSender sender, BaseComponent[] richText) {
        if (sender instanceof Player) {
            ((Player) sender).spigot().sendMessage(richText);
        } else {
            StringBuilder builder = new StringBuilder();
            for (BaseComponent component : richText) {
                builder.append(component.toLegacyText());
            }
            sender.sendMessage(builder.toString());
        }
    }

    private String getTitleLine(String title, int length) {
        String strippedTitle = ChatColor.stripColor(StringUtils.colorize(title));
        String linepart = "&7&m" + org.apache.commons.lang.StringUtils.repeat("-", Math.max(2, (length - strippedTitle.length() - 2)) / 2);
        return linepart + "&r " + title + " " + linepart;
    }

    private CommandMap initCommandMap() {
        try {
            final Field f = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            f.setAccessible(true);
            return (CommandMap) f.get(Bukkit.getServer())
                    ;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public CommandMap getCommandMap() {
        return commandMap;
    }

}
