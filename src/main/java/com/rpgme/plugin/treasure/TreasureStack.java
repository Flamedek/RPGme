package com.rpgme.plugin.treasure;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.Entities;
import com.rpgme.plugin.util.RPGItems;
import com.rpgme.plugin.util.StringUtils;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class TreasureStack implements ConfigurationSerializable {

    static final TreasureStack EMPTY_STACK = new TreasureStack(null, -1);

    final ItemStack item;
    final int rarity;

    public TreasureStack(ItemStack item, int rarity) {
        this.item = item;
        this.rarity = Math.min(100, Math.max(1, rarity));
    }

    public int getWeight(int forLevel) {
        forLevel = Math.min(125, forLevel);
        int baseChance = 100 - Math.min(80, rarity);
        double rarityFactor;

        if (forLevel <= rarity) {
            int dif = rarity - forLevel;
            rarityFactor = dif > 80 ? 0.01 : Math.max(0.2, 1 - (dif / 40.0));
        } else {
            int dif = forLevel - rarity;
            rarityFactor = Math.max(0.1, 1 - (dif / 70.0));
        }

        return (int) Math.round(baseChance * rarityFactor);
    }

    public ItemStack getItemStack() {
        return item;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<>(6);
        ItemStack item = getItemStack();
        map.put("type", item.getType().name());
        map.put("rarity", rarity);

        if (item.getAmount() != 1)
            map.put("amount", item.getAmount());

        return map;
    }

    @SuppressWarnings("unchecked")
    public static TreasureStack deserialize(Map<String, Object> map) {
        Integer rarity = (Integer) map.get("rarity");
        if (rarity == null)
            rarity = 10;

        final String typeinput = (String) map.get("type");
        if (typeinput == null) {
            RPGme.getInstance().getLogger().severe("Invalid treasure configuration " + map + ". Missing 'type' argument.");
            return EMPTY_STACK;
        }

        TreasureStack stack = deserializeType(typeinput, rarity);
        if (stack == null) {
            return EMPTY_STACK;
        }

        ItemStack type = stack.getItemStack();

        Integer amount = (Integer) map.get("amount");
        type.setAmount(amount == null ? 1 : amount);

        Integer data = (Integer) map.get("data");
        Entities.setDamage(type, data == null ? 0 : data);

        ItemMeta meta = type.getItemMeta();
        String name = (String) map.get("name");
        if (name != null)
            meta.setDisplayName(StringUtils.colorize("&f" + name));

        Object lore = map.get("lore");
        if (lore instanceof String) {
            meta.setLore(Arrays.asList(StringUtils.colorize((String) lore).split("\n")));
        } else if (lore instanceof List) {
            meta.setLore(StringUtils.colorize((List<String>) lore));
        }

        type.setItemMeta(meta);
        return stack;
    }

    private static TreasureStack deserializeType(String typeinput, int rarity) {
        if (typeinput.startsWith("exptomb")) {
            ItemStack item = createExpTomb(typeinput);
            return item != null ? new TreasureStack(item, rarity) : null;
        }

        if (typeinput.equalsIgnoreCase("musicdisc")) {
            return new TreasureStack(null, rarity) {

                final Material[] musicDiscs = {Material.MUSIC_DISC_11, Material.MUSIC_DISC_13, Material.MUSIC_DISC_BLOCKS, Material.MUSIC_DISC_CAT,
                        Material.MUSIC_DISC_CHIRP, Material.MUSIC_DISC_FAR, Material.MUSIC_DISC_MALL, Material.MUSIC_DISC_MELLOHI, Material.MUSIC_DISC_STAL, Material.MUSIC_DISC_STRAD,
                        Material.MUSIC_DISC_WAIT, Material.MUSIC_DISC_WARD};

                @Override
                public ItemStack getItemStack() {
                    int randomIndex = CoreUtils.random.nextInt(musicDiscs.length);
                    return new ItemStack(musicDiscs[randomIndex]);
                }
            };
        }

        Material mat = Material.matchMaterial(typeinput);
        if (mat == null) {
            logError(typeinput, "Unknown type.");
            return null;
        }
        return new TreasureStack(new ItemStack(mat), rarity);
    }

    private static ItemStack createExpTomb(String input) {
        Map<String, String> args = new HashMap<>(4);

        String params = input.substring(input.indexOf('?') + 1);
        String[] split = params.split("&");
        for (String s : split) {
            int index = s.indexOf('=');
            if (index > 0) {
                args.put(s.substring(0, index), s.substring(index + 1));
            }
        }

        int exp;
        try {
            exp = Integer.valueOf(args.get("exp"));
        } catch (NumberFormatException e) {
            logError(input, "No value for exp. Treasure has been skipped.");
            return null;
        }

        int minlvl;
        try {
            minlvl = Integer.valueOf(args.get("minlevel"));
        } catch (NumberFormatException e) {
            minlvl = 1;
        }

        List<Skill> restrictedSkills = new ArrayList<>();
        SkillManager skillManager = RPGme.getInstance().getSkillManager();
        String skillsInput = args.get("skills");

        if (skillsInput != null) {
            for (String s : skillsInput.split(",")) {

                if (s.equalsIgnoreCase("random")) {
                    List<Skill> skills = skillManager.getEnabledSkills();
                    restrictedSkills.add(skills.get(CoreUtils.random.nextInt(skills.size())));
                    continue;
                }

                Skill skill = skillManager.getByName(s);
                if (skill != null) {
                    restrictedSkills.add(skill);
                } else {
                    logError(input, "Skill not found: '" + s + "'. Skill ignored");
                }
            }
        }

        if (restrictedSkills.isEmpty()) {
            restrictedSkills = null;
        }

        return RPGItems.createExpTomb(exp, minlvl, restrictedSkills);
    }

    private static void logError(String input, String error) {
        RPGme.getInstance().getLogger().warning("Invalid treasure type '" + input + "'. " + error);
    }

}