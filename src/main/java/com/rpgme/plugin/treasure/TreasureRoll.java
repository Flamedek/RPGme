package com.rpgme.plugin.treasure;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.RPGRunnable;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Robin on 26/02/2017.
 */
public class TreasureRoll extends RPGRunnable {

    private static final int MAX_ITERATIONS = 65;

    private final Player p;
    protected final Inventory inv;
    private final ItemStack[] loot;

    private int i = 0;
    private int step = 0;

    public TreasureRoll(Player p, Inventory inv, int forlevel) {
        this.p = p;
        this.inv = inv;
        this.loot = new ItemStack[12];
        TreasureBag bag = RPGme.getInstance().getModule(RPGme.MODULE_TREASURES);

        for (int i = 0; i < 12; ++i) {
            loot[i] = bag.rollTreasureItem(forlevel);
        }
    }

    private boolean shouldWait(int i) {
        if (i > 60) {
            return i % 7 != 0;
        } else if (i > 55) {
            return i % 6 != 0;
        } else if (i > 50) {
            return i % 5 != 0;
        } else if (i > 40) {
            return i % 4 != 0;
        } else if (i > 30) {
            return i % 3 != 0;
        } else if (i > 20) {
            return i % 2 != 0;
        }
        return false;
    }

    @Override
    public void run() {
        i++;
        if (i > MAX_ITERATIONS) {
            if (i == MAX_ITERATIONS + 12) {
                cancel();
                p.closeInventory();
            }

            GameSound.play(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, p, 1.25f, 1.2f, 0.4d);
        } else {
            if (shouldWait(i)) {
                return;
            }

            rollDown();
            GameSound.play(Sound.BLOCK_WOOD_BREAK, p, 0.3d);
        }
    }

    private void rollDown() {
        step++;
        ItemStack next = loot[step % loot.length];
        inv.setItem(22, inv.getItem(13));
        inv.setItem(13, inv.getItem(4));
        inv.setItem(4, next);
    }

    @Override
    public void finish() {
        ItemStack result = calculateFinalResult();
        ItemUtils.give(p, result);
        GameSound.play(Sound.ENTITY_ITEM_PICKUP, p, 1.75f, 1.5f);
        TreasureChest.currentRolls.remove(this);
    }

    private ItemStack calculateFinalResult() {
        int i = 0;
        int step = 0;

        while (i < MAX_ITERATIONS) {
            if (!shouldWait(++i)) {
                step++;
            }
        }
        return loot[(step - 1) % loot.length];
    }
}