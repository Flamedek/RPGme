package com.rpgme.plugin.treasure;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.YamlFile;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.*;
import java.util.logging.Level;

public class TreasureBag implements Module {

    /**
     * Convenience method to get the instance registered with the plugin
     */
    public static TreasureBag getInstance() {
        return RPGme.getInstance().getModule(RPGme.MODULE_TREASURES);
    }

    protected final RPGme plugin;
    protected final List<TreasureStack> treasures = new ArrayList<>();
    protected final TreasureChest chestManager;

    public TreasureBag(RPGme plugin) {
        this.plugin = plugin;
        chestManager = new TreasureChest(plugin);
        chestManager.registerListeners();

        ConfigurationSerialization.registerClass(TreasureStack.class, "Treasure");
    }

    protected void initDefaultLoot() {
        // vanilla loot
        addTreasurePossibility(Material.SADDLE, 1);
        addTreasurePossibility(Material.NAME_TAG, 50);

        addTreasurePossibility(new ItemStack(Material.WITHER_SKELETON_SKULL, 1), 75);

        addTreasurePossibility(new ItemStack(Material.ENDER_PEARL, 6), 20);
        addTreasurePossibility(new ItemStack(Material.ENDER_PEARL, 16), 60);
        addTreasurePossibility(new ItemStack(Material.EXPERIENCE_BOTTLE, 16), 25);
        addTreasurePossibility(new ItemStack(Material.EXPERIENCE_BOTTLE, 32), 50);

        addTreasurePossibility(Material.IRON_HORSE_ARMOR, 1);
        addTreasurePossibility(Material.GOLDEN_HORSE_ARMOR, 40);
        addTreasurePossibility(Material.DIAMOND_HORSE_ARMOR, 60);

        addTreasurePossibility("musicdisc", 1);

        addTreasurePossibility(new ItemStack(Material.ARROW, 32), 1);
        addTreasurePossibility(new ItemStack(Material.ARROW, 64), 30);

        addTreasurePossibility(Material.CHAINMAIL_BOOTS, 40);
        addTreasurePossibility(Material.CHAINMAIL_CHESTPLATE, 60);
        addTreasurePossibility(Material.CHAINMAIL_HELMET, 50);
        addTreasurePossibility(Material.CHAINMAIL_LEGGINGS, 50);

        addTreasurePossibility(Material.DIAMOND_BOOTS, 70);
        addTreasurePossibility(Material.DIAMOND_CHESTPLATE, 70);
        addTreasurePossibility(Material.DIAMOND_HELMET, 70);
        addTreasurePossibility(Material.DIAMOND_LEGGINGS, 70);
        addTreasurePossibility(Material.DIAMOND_SWORD, 30);

        addTreasurePossibility(new ItemStack(Material.GHAST_TEAR, 2), 10);
        addTreasurePossibility(new ItemStack(Material.GHAST_TEAR, 4), 30);

        // exp tombs
        addTreasurePossibility("exptomb500", 1);
        addTreasurePossibility("exptomb1000,10", 30);
        addTreasurePossibility("exptomb2500,25", 40);
        addTreasurePossibility("exptomb5000,50", 90);

        addTreasurePossibility("skillgem", 95);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        File treasureFile = new File(plugin.getDataFolder(), "treasures.yml");

        if (!treasureFile.exists()) {
            plugin.saveResource("treasures.yml", false);
            plugin.getLogger().info("Placing default " + treasureFile.getName());
        }
    }

    @SuppressWarnings({"unchecked", "SuspiciousMethodCalls"})
    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        File treasureFile = new File(plugin.getDataFolder(), "treasures.yml");
        YamlFile file = new YamlFile(treasureFile);

        treasures.clear();
        try {
            file.read();
            List<TreasureStack> e = (List<TreasureStack>) file.data.getList("Treasures");
            e.removeAll(Collections.singleton(TreasureStack.EMPTY_STACK));
            treasures.addAll(e);
        } catch (NullPointerException | ClassCastException e) {
            plugin.getLogger().log(Level.SEVERE, "Exception reading treasures.yml. Using default settings instead.", e);
            initDefaultLoot();
        }
    }

    public void addTreasurePossibility(ItemStack item, int rarity) {
        treasures.add(new TreasureStack(item, rarity));
    }

    public void addTreasurePossibility(Material item, int rarity) {
        addTreasurePossibility(new ItemStack(item), rarity);
    }

    public void addTreasurePossibility(String item, int rarity) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("rarity", rarity);
        map.put("type", item);
        treasures.add(TreasureStack.deserialize(map));
    }

    public List<TreasureStack> getTreasuresPossibilities() {
        return treasures;
    }

    public ItemStack rollTreasureChest(int level) {
        return chestManager.getNewTreasureChest(level);
    }

    public ItemStack rollTreasureItem(int level) {
        List<Integer> factors = new ArrayList<>();
        int total = 0;

        for (TreasureStack stack : treasures) {
            int f = stack.getWeight(level);

            factors.add(f);
            total += f;

        }

        int index = CoreUtils.random.nextInt(total);
        int sum = 0;
        int i = 0;

        while (sum < index) {
            sum += factors.get(i++);
        }

        return treasures.get(Math.max(0, i - 1)).getItemStack();
    }

    public void spawnTreasure(Block b, int level) {
        ItemStack item = rollTreasureChest(level);
        spawnTreasure(b, item);
    }

    public void spawnTreasure(Location loc, int level) {
        ItemStack item = rollTreasureChest(level);
        spawnTreasure(loc, item);
    }

    public void spawnTreasure(Block b, ItemStack item) {
        spawnTreasure(b.getLocation().add(0.5, 0.5, 0.5), item);
    }

    public void spawnTreasure(Location loc, ItemStack item) {
        loc.getWorld().spawnParticle(Particle.HEART, loc, 2, 0.2, 0.2, 0.2, 1.0);
        loc.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, loc, 5, 0.25f, 0.25f, 0.25f, 0.2f);
        GameSound.play(Sound.BLOCK_NOTE_BLOCK_HARP, loc, 2f, 1.4f);

        loc.getWorld().dropItemNaturally(loc, item);
    }

}
