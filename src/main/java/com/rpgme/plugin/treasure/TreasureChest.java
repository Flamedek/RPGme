package com.rpgme.plugin.treasure;

import com.google.common.collect.Lists;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Listener;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.RomanNumber;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.IntTag;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;
import java.util.List;

public class TreasureChest extends Listener<RPGme> {

    protected static List<TreasureRoll> currentRolls = Lists.newArrayList();

    public TreasureChest(RPGme plugin) {
        super(plugin);
    }

    public ItemStack getNewTreasureChest(int forlevel) {
        int level = Math.max(1, forlevel);
        String name = "&6Treasure Chest &7(Right-Click)";
        String lore = "&aTier: &e" + toRomanTier(level) + "\n&7Open up and roll your random treasure";
        return ItemUtils.create(new IntTag("TreasureChest", level), Material.CHEST, name, lore);
    }

    public static String toRomanTier(int level) {
        int tier = (int) Math.max(1.0, (level / 10.0));
        return RomanNumber.toRoman(tier);
    }

    public static int levelForTier(int tier) {
        return tier * 10;
    }

    public Inventory buildEmptyInventory(int level) {
        Inventory inv = plugin.getServer().createInventory(null, 27, ChatColor.GOLD + "Treasure Chest " + ChatColor.DARK_GREEN + toRomanTier(level));
        ItemStack yellow = new ItemStack(Material.YELLOW_STAINED_GLASS_PANE, 1);
        ItemStack orange = new ItemStack(Material.ORANGE_STAINED_GLASS_PANE, 1);
        ItemStack red = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
        ItemStack brown = new ItemStack(Material.BROWN_STAINED_GLASS_PANE, 1);

        for (int i = 0; i < 27; ++i) {
            if (i % 9 >= 2 && i % 9 <= 6) {
                if (i % 9 >= 3 && i % 9 <= 5) {
                    if (i / 9 == 1) {
                        inv.setItem(i, red);
                    } else {
                        inv.setItem(i, brown);
                    }
                } else {
                    inv.setItem(i, orange);
                }
            } else {
                inv.setItem(i, yellow);
            }
        }

        return inv;
    }

    @EventHandler
    public void onOpen(PlayerInteractEvent e) {
        if (e.hasItem() && e.getItem().getType() == Material.CHEST) {
            CompoundTag tag = NBTFactory.getFrom(e.getItem());
            if (tag == null) {
                return;
            }

            int chestLevel = tag.getInteger("TreasureChest");
            if (chestLevel == 0) {
                return;
            }

            e.setCancelled(true);
            ItemUtils.take(e.getPlayer().getInventory(), e.getItem(), 1);
            Inventory inv = buildEmptyInventory(chestLevel);
            e.getPlayer().openInventory(inv);
            TreasureRoll roll = new TreasureRoll(e.getPlayer(), inv, chestLevel);
            roll.runTaskTimer(plugin, 5L, 2L);
            currentRolls.add(roll);
        }

    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        for (TreasureRoll roll : currentRolls) {
            if (roll.inv.equals(e.getClickedInventory())) {
                e.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        Iterator<TreasureRoll> it = currentRolls.iterator();
        while (it.hasNext()) {
            TreasureRoll roll = it.next();
            if (e.getInventory().equals(roll.inv)) {
                it.remove();
                roll.cancel();
                return;
            }
        }
    }
}