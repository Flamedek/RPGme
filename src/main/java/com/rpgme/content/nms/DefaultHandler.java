package com.rpgme.content.nms;

import com.rpgme.content.nms.INMS.INMSUtil;
import com.rpgme.content.nms.INMS.IPacketSender;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class DefaultHandler implements INMSUtil, IPacketSender {

    @Override
    public void doArmSwing(Player p) {

    }

    @Override
    public void sendTitleLength(Player player, int fadein, int length, int fadeout) {

    }

    @Override
    public void sendTitle(Player player, String title) {

    }

    @Override
    public void sendSubtitle(Player player, String title) {

    }

    @Override
    public void setWorldborderEffect(Player p, int value) {

    }

    @Override
    public void setAbsorptionHearts(LivingEntity p, float extra) {

    }

    @Override
    public float getAbsorptionHearts(LivingEntity p) {
        return 0f;
    }

    @Override
    public void setInvisible(Entity entity, boolean value) {

    }

    @Override
    public void setMovementSpeed(Entity entity, double value) {

    }

    @Override
    public boolean isFromMobspawner(Entity entity) {
        return false;
    }

    @Override
    public boolean canPickup(Arrow arrow) {
        return false;
    }

    @Override
    public void setCanPickup(Arrow arrow, boolean value) {

    }


}
