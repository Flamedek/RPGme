package com.rpgme.content.module.trails;

import com.google.common.collect.Maps;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.event.RPGPlayerJoinEvent;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Map;

public class SkillTrails implements ListenerModule {

    private static final String SETTING_KEY = "SkillTrail";
    public static final int NONE = -1;

    final RPGme plugin = RPGme.getInstance();
    private final Map<Player, ParticleTrail> map = Maps.newHashMap();
    private MovementListener listener = new MovementListener();

    int masteryLevel;


    @Override
    public void onEnable() {
        new TrailMenuCommand(this).register();
    }

    @Override
    public void onDisable() {

        for (Map.Entry<Player, ParticleTrail> entry : map.entrySet()) {
            RPGPlayer rp = plugin.getPlayer(entry.getKey());
            rp.setSetting(SETTING_KEY, entry.getValue().getName());
        }
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        config.addValue("Level at which a skill particle trail is unlocked", "unlocked", 100);
        messages.addValue("notification", ConfigHelper.getBundledMessage(getClass(), ""));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        masteryLevel = config.getInt("unlocked");

        for (Skill skill : plugin.getSkillManager().getEnabledSkills()) {
            skill.addNotification(new Notification(masteryLevel, Notification.ICON_UNLOCK, "Skill Trail",
                    messages.getMessage("notification", skill.getDisplayName())));
        }
    }

    @EventHandler
    public void onJoin(RPGPlayerJoinEvent e) {
        RPGPlayer rp = e.getRPGPlayer();
        String setting = rp.getSetting(SETTING_KEY);
        if (setting != null) {
            setTrail(e.getPlayer(), ParticleTrail.valueOf(setting));
            rp.setSetting(SETTING_KEY, null);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (map.containsKey(p)) {
            RPGPlayer rp = plugin.getPlayer(p);
            rp.setSetting("MasteryTrail", map.get(p).getName());
            setTrail(p, null);
        }

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onKick(PlayerKickEvent e) {
        onQuit(new PlayerQuitEvent(e.getPlayer(), ""));
    }

    public void setTrail(Player p, ParticleTrail trail) {
        if (trail == null) {
            map.remove(p);
            if (map.isEmpty()) {
                listener.unregisterListeners();
            }
        } else {
            if (map.isEmpty()) {
                listener.registerListeners();
            }

            map.put(p, trail);
        }
    }

    public ParticleTrail getTrail(Player p) {
        return map.get(p);
    }

    public void onMove(PlayerMoveEvent e) {
        ParticleTrail trail = map.get(e.getPlayer());
        if (trail != null && hasMoved(e)) {
            trail.display(e.getPlayer());
        }
    }

    private boolean hasMoved(PlayerMoveEvent e) {
        return e.getFrom().getX() != e.getTo().getX() && e.getFrom().getZ() != e.getTo().getZ();
    }

    /**
     * Separate listener class that can be unregistered when no trails are active
     */
    public class MovementListener implements Listener {

        public void registerListeners() {
            plugin.getServer().getPluginManager().registerEvents(this, plugin);
        }

        public void unregisterListeners() {
            PlayerMoveEvent.getHandlerList().unregister(this);
        }

        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onMove(PlayerMoveEvent e) {
            SkillTrails.this.onMove(e);
        }
    }
}
