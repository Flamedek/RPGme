package com.rpgme.content.module.expboost;

import com.rpgme.plugin.command.CommandHelp;
import com.rpgme.plugin.command.CoreCommand;
import com.rpgme.plugin.command.TabCompleteHelper;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.TimeUtils;
import com.rpgme.plugin.util.config.MessagesBundle;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Robin on 30/07/2017.
 */
public class BoosterCommand extends CoreCommand {

    private static final String PERMISSION = "rpgme.command.boosters";
    private final BoosterModule boosters;

    public BoosterCommand(BoosterModule module) {
        super(module.getPlugin(), "rpgbooster");
        this.boosters = module;
        setConsoleAllowed(true);

        setAliases("expboost", "rpgboost");
        setDescription("This command can set an exp booster for all skills applied to a player or the whole server.");

        setCommandHelp(new CommandHelp.Builder()
                .addUsage("/expboost", "Personal overview of active exp boosts")
                .addUsage("/expboost add <player> <percentage> <minutes> [skills] [..description]", "Apply an exp boost to a specific player for some minutes. Optionally supply the skills to apply to (or 'all' for all skills) and a description to display to players.", PERMISSION)
                .addUsage("/expboost addglobal <percentage> <minutes> [skills] [..description]", "Apply a exp boost to all player for some time. Optionally supply the skills to apply to (or 'all' for all skills) and a description to display to players.", PERMISSION)
                .build());
    }

    @Override
    public List<String> getTabComplete(CommandSender sender, String label, List<String> args) {
        if (args.isEmpty()) {
            return Collections.emptyList();
        }
        if (args.size() == 1) {
            return TabCompleteHelper.completePattern(sender, args, "add,addglobal");
        }

        if (args.get(0).equals("set")) {
            return TabCompleteHelper.completePattern(sender, args, "add <player> [boost_percentage] [minutes] <skills> ...description");
        }
        if (args.get(0).equals("setglobal")) {
            return TabCompleteHelper.completePattern(sender, args, "addglobal [boost_percentage] [minutes] <skills> ...description");
        }
        return Collections.emptyList();
    }

    @Override
    public void execute(CommandSender sender, String alias, List<String> flags) {
        if (flags.isEmpty() || !hasPermission(sender, PERMISSION, false)) {
            sender.sendMessage(buildBoosterOverview(sender));
            return;
        }

        int percent, duration;

        List<String> skills = Collections.emptyList();
        String description = "";

        if (flags.remove("addglobal")) {
            if (flags.size() < 2) {
                error(sender, "Not enough arguments. /expboost addglobal <percentage> <minutes> [skills] [..description]");
                return;
            }

            try {
                percent = Integer.parseInt(flags.get(0));
                duration = Integer.parseInt(flags.get(1));
            } catch (NumberFormatException var10) {
                error(sender, "Wrong arguments, numbers expected. /expboost addglobal <percentage> <minutes> [skills] [..description]");
                return;
            }

            if (flags.size() > 2) {
                skills = Arrays.stream(flags.get(2).split(","))
                        .filter((item) -> plugin.getSkillManager().getByName(item) != null)
                        .collect(Collectors.toList());
            }
            if (flags.size() > 3) {
                description = String.join(" ", flags.subList(3, flags.size()));
            }

            boosters.addBoost(percent, duration, null, skills, description);
            sender.sendMessage(ChatColor.GREEN + "Added global exp multiplier set of " + toMultiplier(percent) + " for " + toDisplayTime(duration));

            for (Player player : plugin.getServer().getOnlinePlayers()) {
                player.sendMessage(buildBoosterOverview(player));
            }

        } else if (flags.remove("add")) {
            if (flags.size() < 3) {
                error(sender, "Not enough arguments. /expboost add <player> <percentage> <minutes> [skills] [..description]");
                return;
            }

            try {
                percent = Integer.parseInt(flags.get(1));
                duration = Integer.parseInt(flags.get(2));
            } catch (NumberFormatException var9) {
                error(sender, "Wrong arguments, numbers expected. /booster add <player> <percentage> <minutes> [skills] [..description]");
                return;
            }

            if (flags.size() > 3) {
                skills = Arrays.stream(flags.get(3).split(","))
                        .filter((item) -> plugin.getSkillManager().getByName(item) != null)
                        .collect(Collectors.toList());
            }
            if (flags.size() > 4) {
                description = String.join(" ", flags.subList(4, flags.size()));
            }

            String name = flags.get(0);
            Player player = plugin.getServer().getPlayer(name);
            if (player == null) {
                error(sender, "Player not found (offline?) name=" + name);
            } else {
                boosters.addBoost(percent, duration, player, skills, description);
                if (sender != player) {
                    String restriction = String.join(",", skills);
                    sender.sendMessage(ChatColor.GREEN + "Booster activated for player " + name + " of " + percent + " " + restriction + " xp for " + duration + "m");
                }
                player.sendMessage(buildBoosterOverview(player));
            }
        } else {
            printCommandHelp(sender);
        }
    }

    private String toMultiplier(int percent) {
        return String.format("%+d%%", percent);
    }

    private String toDisplayTime(int minutes) {
        return TimeUtils.readableAsMinuteFormat(minutes * 60);
    }

    private String remainingDuration(long endTime) {
        long remaining = endTime - System.currentTimeMillis();
        return TimeUtils.readableAsMinuteFormat((int) Math.round(remaining / 1000.0));
    }

    public String buildBoosterOverview(CommandSender sender) {
        StringBuilder sb = new StringBuilder();
        MessagesBundle messages = plugin.getMessages();

        Player player = sender instanceof Player ? (Player) sender : null;
        List<BoosterModule.Boost> boosts = boosters.getBoosts(player);
        boosts.sort(Comparator.nullsFirst(null));

        sb.append("&e ╔            &7-- &eRPGme exp Boosters &7--\n");
        sb.append("&e ║\n");

        if (boosts.isEmpty()) {
            sb.append("&e ║  ").append(messages.getMessage("ExpBoost_nothing")).append("\n");
            sb.append("&e ║\n");
        } else {
            for (BoosterModule.Boost boost : boosts) {
                String key = boost.getPlayer() == null ? "ExpBoost_global" : "ExpBoost_personal";
                String message = messages.getMessage(key, boost.getComment());
                sb.append("&e ║  ").append(message).append(toMultiplier(boost.getBoost())).append(" &7(").append(remainingDuration(boost.getEndTime())).append(")\n");

                if (!boost.getRestriction().isEmpty()) {
                    sb.append("&e ║      &7").append(boost.getRestriction()).append("\n");
                }
                sb.append("&e ║\n");
            }
        }

        sb.append("&e ╚  ");
        return StringUtils.colorize(sb.toString());
    }
}