package com.rpgme.content.module.expboost

import com.rpgme.plugin.RPGme
import com.rpgme.plugin.api.ListenerModule
import com.rpgme.plugin.event.SkillExpGainEvent
import com.rpgme.plugin.util.config.BundleBuilder
import com.rpgme.plugin.util.config.BundleSection
import com.rpgme.plugin.util.config.ConfigBuilder
import com.rpgme.plugin.util.config.YamlFile
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerKickEvent
import org.bukkit.event.player.PlayerQuitEvent
import java.io.File
import java.io.IOException
import java.util.*

class BoosterModule(val plugin: RPGme) : ListenerModule {

    data class Boost(
        val id: UUID,
        val player: UUID?,
        val boost: Int,
        val restriction: String,
        val comment: String,
        var endTime: Long,
        var paused: Boolean
    ) {
        fun pause() {
            if (!paused) {
                paused = true
                // endtime becomes remaining time
                endTime -= System.currentTimeMillis()
            }
        }

        fun resume() {
            if (paused) {
                paused = false
                endTime += System.currentTimeMillis()
            }
        }
    }

    private val command = BoosterCommand(this)
    private val boosts = arrayListOf<Boost>()

    private var pauseOnLogout = false

    override fun getName() = "ExpBoost"

    override fun createConfig(config: ConfigBuilder, messages: BundleBuilder) {
        config.addValue("Should active exp boosts be paused when a player is offline", "pause on logout", false)

        messages.addValue("global", "&fServer &6{0}&f:  &3")
            .addValue("personal", "&fPersonal &6{0}&f:  &3")
            .addValue("nothing", "There are currently no active boosters.")
    }

    override fun onLoad(config: ConfigurationSection, messages: BundleSection) {
        pauseOnLogout = config.getBoolean("pause on logout", false)
    }

    override fun onEnable() {
        command.register()
        restore()
    }

    override fun onDisable() {
        if (pauseOnLogout) {
            // pause global boosts when down
            for (boost in boosts) {
                if (boost.player == null) {
                    boost.pause()
                }
            }
        }
        save()
    }

    //
    // Api

    fun getBoosts(player: Player?): List<Boost> {
        val now = System.currentTimeMillis()
        return boosts.filter { boost ->
            !boost.paused && boost.endTime > now && (boost.player == null || boost.player == player?.uniqueId)
        }
    }

    @JvmOverloads
    fun addBoost(
        percent: Int,
        durationMinutes: Int,
        player: Player? = null,
        skills: List<String>? = null,
        description: String = ""
    ) {
        boosts += Boost(
            UUID.randomUUID(),
            player?.uniqueId,
            percent,
            skills?.joinToString(",").orEmpty(),
            description,
            getEndtime(durationMinutes),
            false
        )
    }

    fun readableBoost(percent: Int): String {
        return String.format(Locale.ENGLISH, "x%.2f", toMultiplier(percent))
    }

    fun toMultiplier(percent: Int): Double {
        return (100 + percent) / 100.0
    }

    private fun getEndtime(mins: Int): Long {
        return System.currentTimeMillis() + (mins * 60 * 1000).toLong()
    }

    private fun removeExpiredBoosts() {

    }

    //
    // Events

    @EventHandler
    fun onExpGain(event: SkillExpGainEvent) {
        val playerId = event.player.uniqueId
        val now = System.currentTimeMillis()

        var effect = 0

        val iterator = boosts.iterator()
        while (iterator.hasNext()) {
            val boost = iterator.next()
            if (boost.paused) {
                continue
            }
            if (boost.endTime < now) {
                iterator.remove()
                continue
            }
            if ((boost.player == null || boost.player == playerId) &&
                (boost.restriction.isEmpty() || event.skill.name in boost.restriction)
            ) {
                effect += boost.boost
            }
        }

        if (effect > 0) {
            val multiplier = toMultiplier(effect)
            event.exp = (event.exp * multiplier).toFloat()
        }
    }

    // pause/resume
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    fun onLogin(event: PlayerJoinEvent) {
        val playerId = event.player.uniqueId
        for (boost in boosts) {
            if (boost.player == null || boost.player == playerId) {
                boost.resume()
                event.player.sendMessage(command.buildBoosterOverview(event.player))
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    fun onKick(event: PlayerKickEvent) {
        if (pauseOnLogout) {
            val playerId = event.player.uniqueId
            for (boost in boosts) {
                if (boost.player == playerId) {
                    boost.pause()
                }
            }
        }
    }

    @EventHandler
    fun onLeave(event: PlayerQuitEvent) {
        if (pauseOnLogout) {
            val playerId = event.player.uniqueId
            for (boost in boosts) {
                if (boost.player == playerId) {
                    boost.pause()
                }
            }
        }
    }

    //
    // State

    private fun save() {
        val file = YamlFile(File(plugin.userDataFolder, "boosters.temp"))
        file.clear()

        val config = file.data
        val ids = ArrayList<String>(boosts.size)

        for (boost in boosts) {
            ids += boost.id.toString()
            config.set("${boost.id}.boost", boost.boost)
            config.set("${boost.id}.player", boost.player?.toString().orEmpty())
            config.set("${boost.id}.endTime", boost.endTime)
            config.set("${boost.id}.restriction", boost.restriction)
            config.set("${boost.id}.comment", boost.comment)
            config.set("${boost.id}.paused", boost.paused)
        }
        config.set("ids", ids)

        try {
            file.write()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun restore() {
        val file = YamlFile(File(plugin.userDataFolder, "boosters.temp"))
        boosts.clear()

        if (file.file.isFile) {
            file.read()
            val config = file.data
            val ids = config.getList("ids").orEmpty() as List<String>

            for (id in ids) {
                try {
                    val playerId = config.getString("$id.player")
                    val boost = Boost(
                        id = UUID.fromString(id),
                        player = if (playerId.isNullOrEmpty()) null else UUID.fromString(playerId),
                        boost = config.getInt("$id.boost"),
                        endTime = config.getLong("$id.endTime"),
                        restriction = config.getString("$id.restriction").orEmpty(),
                        comment = config.getString("$id.comment").orEmpty(),
                        paused = config.getBoolean("paused")
                    )

                    boosts.add(boost)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

}

