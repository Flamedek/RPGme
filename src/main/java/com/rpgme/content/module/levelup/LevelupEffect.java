package com.rpgme.content.module.levelup;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Random;

/**
 * Abstract class representing an effect to play when a player levels up
 * Created by Robin on 05/02/2017.
 */
public abstract class LevelupEffect extends BukkitRunnable {

    protected final LivingEntity target;
    protected final World world;

    protected final int duration;
    private int i = 0;

    public LevelupEffect(LivingEntity target) {
        this.target = target;
        this.world = target.getWorld();
        this.duration = this.getDuration();
    }

    public abstract void start();

    @Override
    public void run() {
        if (++i == duration) {
            cancel();
            EffectsManager.currentTasks.remove(target);
        }

        doStep(i);
    }

    public abstract int getDuration();

    public abstract void doStep(int var1);


    //// Start of implementing classes ////

    public static class Default extends LevelupEffect {

        final List<Location> list;
        final double baseHeight;

        public Default(LivingEntity target) {
            super(target);
            list = CoreUtils.circle(target.getEyeLocation(), 0.85, getDuration() / 2);
            baseHeight = target.getEyeLocation().getY() + 0.5;
        }

        @Override
        public void start() {
            EffectsManager.currentTasks.add(target);
            runTaskTimerAsynchronously(RPGme.getInstance(), 0L, 2L);
        }

        @Override
        public int getDuration() {
            return 20;
        }

        @Override
        public void doStep(int step) {
            Location loc = list.get(step % list.size());
            loc.setY(baseHeight + (0.05 * step));

            world.spawnParticle(Particle.FLAME, loc, 2, 0, 0, 0, 0.02);
            world.spawnParticle(Particle.VILLAGER_HAPPY, loc, 1, 0, 0, 0, 0.1);
        }
    }

    public static class Beacon extends LevelupEffect {
        private final Location loc;

        public Beacon(LivingEntity target) {
            super(target);
            this.loc = target.getLocation();
        }

        @Override
        public void start() {
            runTaskTimerAsynchronously(RPGme.getInstance(), 0L, 3L);
            Location loc = this.loc.clone().add(0, 1, 0);
            world.spawnParticle(Particle.SPELL_WITCH, loc, 12, 0.3, 1.0, 0.3, 1.0);
            world.spawnParticle(Particle.FIREWORKS_SPARK, loc, 5, 0.5, 1.0, 0.5, 0.5);
        }

        @Override
        public void doStep(int step) {
            double angle = 0.8975979010256552D * (double) step;

            for (int i = step; i > Math.max(0, step - 25); --i) {
                Location point = CoreUtils.getPointOnCircle(this.loc, angle, 0.55D, (double) i * 0.75D);
                world.spawnParticle(Particle.SPELL_WITCH, point, 4, 0.0, 0.15, 0.0, 0.0);
            }

            GameSound.play(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, this.loc.clone().add(0.0D, (double) step, 0.0D), 0.5F, 0.8F, 0.1D);
        }

        @Override
        public int getDuration() {
            return 40;
        }
    }

    public static class Eruption extends LevelupEffect {
        final Vector[] shots = new Vector[4];

        public Eruption(LivingEntity target) {
            super(target);
            Random r = CoreUtils.random;

            for (int i = 0; i < 4; ++i) {
                this.shots[i] = new Vector(r.nextDouble() * 2.0D - 1.0D, r.nextDouble() * 0.5D + 0.4D, r.nextDouble() * 2.0D - 1.0D);
            }

        }

        @Override
        public void start() {
            runTaskTimerAsynchronously(RPGme.getInstance(), 0L, 2L);
            GameSound.play(Sound.ENTITY_WITHER_SHOOT, this.target.getLocation(), 0.1F, 0.3F);
            GameSound.play(Sound.ENTITY_GENERIC_EXPLODE, this.target.getLocation(), 0.5F, 1.5F);
        }

        @Override
        public int getDuration() {
            return 30;
        }

        @Override
        public void doStep(int step) {
            double distance = (double) step * 0.3D;
            Vector[] var7 = this.shots;
            int var6 = this.shots.length;

            for (int var5 = 0; var5 < var6; ++var5) {
                Vector vec = var7[var5];
                Location loc = this.target.getLocation().add(vec.clone().multiply(distance));
                world.spawnParticle(Particle.DRIP_LAVA, loc, 1, 0.0, 0.0, 0.0, 0.0);
                world.spawnParticle(Particle.FLAME, loc, 1, 0.0, 0.0, 0.0, 0.0);
                if (step == this.getDuration()) {
                    GameSound.play(Sound.ENTITY_GENERIC_EXPLODE, loc, 0.55F, 1.3F);
                    GameSound.play(Sound.ENTITY_PLAYER_LEVELUP, loc, 0.5F, 1.3F);
                    world.spawnParticle(Particle.FIREWORKS_SPARK, loc, 15, 0.0, 0.0, 0.0, 0.15);
                }
            }

        }
    }

    public static class FlameHelix extends LevelupEffect {
        final Location location;

        public FlameHelix(LivingEntity target) {
            super(target);
            this.location = target.getLocation();
        }

        @Override
        public void start() {
            runTaskTimerAsynchronously(RPGme.getInstance(), 0L, 1L);
        }

        @Override
        public void doStep(int step) {
            double angle = 0.3141592653589793D * (double) step;
            double height = (double) step * 0.1D % 2.5D;
            double radius = 0.45D;
            Location one = CoreUtils.getPointOnCircle(this.location, true, angle, radius, height);
            Location two = CoreUtils.getPointOnCircle(this.location, true, angle - 3.141592653589793D, radius, height);
            world.spawnParticle(Particle.FLAME, one, 1, 0, 0, 0, 0);
            world.spawnParticle(Particle.FLAME, two, 1, 0, 0, 0, 0);
            if (step % 4 == 0) {
                GameSound.play(Sound.ENTITY_PLAYER_LEVELUP, one, 0.2F, (float) (height / 4.0D) + 0.5F);
            }

        }

        @Override
        public int getDuration() {
            return 72;
        }
    }

    public static class Pulsar extends LevelupEffect {
        public Pulsar(LivingEntity target) {
            super(target);
        }

        @Override
        public void start() {
            runTaskTimerAsynchronously(RPGme.getInstance(), 0L, 2L);
        }

        @Override
        public int getDuration() {
            return 40;
        }

        @Override
        public void doStep(int step) {
            Player p = (Player) this.target;
            double radius = (0.5D + (double) step * 0.15D) % 3.0D;
            byte speed = 4;
            if (step % speed == 1 && this.target.isValid()) {
                GameSound.play(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, (Player) this.target, 1.8F - (float) (radius / 2.5D), 1.0F, 0.15D);
            }

            for (int i = 0; (double) i < radius * 10.0D; ++i) {
                double angle = Math.PI / (radius * 10.0D) * (double) i;
                Location point = CoreUtils.getPointOnCircle(p.getLocation(), false, angle, radius, 1.0D);
                world.spawnParticle(Particle.CRIT, point, 2, 0.1, 0.1, 0.1, 0);
                if (step < 15) {
                    world.spawnParticle(Particle.CRIT_MAGIC, point, 1, 0.1, 0.1, 0.1, 0);
                }
            }

        }
    }
}
