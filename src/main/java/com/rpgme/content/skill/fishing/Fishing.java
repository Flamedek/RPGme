package com.rpgme.content.skill.fishing;

import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.treasure.TreasureBag;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.math.ScaledValue;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;

import java.util.EnumSet;
import java.util.List;

public class Fishing extends BaseSkill {

    private static final EnumSet<Material> fishingTreasure = EnumSet.of(Material.BOW, Material.ENCHANTED_BOOK, Material.FISHING_ROD, Material.NAME_TAG, Material.SADDLE);
    private static final int EXP_TREASURE = 80;
    private static final int EXP_FISH = 25;
    private static final int EXP_JUNK = 10;

    private ScaledValue treasureChance = new ScaledValue(0, 5, 100, 20);
    private ScaledValue expMultiplier = new ScaledValue(0, 100, 100, 300);

    private Material[] fishTypes = {Material.COD, Material.SALMON, Material.PUFFERFISH, Material.TROPICAL_FISH};

    public Fishing() {
        super("Fishing", SkillType.FISHING);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        super.createConfig(config, bundle);
    }

    @Override
    public Material getItemRepresentation() {
        return Material.FISHING_ROD;
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        //list.add("Bite Chance:x" + StringUtils.readableDecimal(((double)forlevel/ biteChance +1)));
        list.add("Exp Multiplier:x" + expMultiplier.readableScale(forlevel));
        list.add("Treasure Chance:" + treasureChance.readableScale(forlevel) + "%");
    }

    public int getFishingExpReward(ItemStack item) {
        Material type = item.getType();
        switch (type) {
            case COD:
                return EXP_FISH;
            case SALMON:
                return EXP_FISH * 2;
            case TROPICAL_FISH:
                return EXP_FISH * 3;
            case PUFFERFISH:
                return EXP_FISH * 4;
        }

        if (type == Material.CHEST || isVanillaTreasure(item)) {
            return EXP_TREASURE;
        } else {
            return EXP_JUNK;
        }
    }

    private boolean isVanillaTreasure(ItemStack item) {
        return fishingTreasure.contains(item.getType());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFish(PlayerFishEvent e) {

        if (!isEnabled(e.getPlayer())) {
            return;
        }

        State state = e.getState();
        int level = getLevel(e.getPlayer());

        if (state == State.CAUGHT_FISH) {

            Item item = (Item) e.getCaught();
            if (treasureChance.isRandomChance(level)) {

                TreasureBag treasureBag = plugin.getModule(RPGme.MODULE_TREASURES);
                item.setItemStack(treasureBag.rollTreasureChest(level));
                GameSound.play(Sound.ENTITY_PLAYER_LEVELUP, e.getPlayer(), 0.8f, 1.4f);
            } else {

                ItemStack caught = item.getItemStack();

                // set fish type based on level
                if (ArrayUtils.contains(fishTypes, caught.getType())) {
                    int rolledIndex = (int) Math.min(((CoreUtils.random.nextDouble() + 0.05) * ((double) level / 100) * 3), 3.0);
                    caught.setType(fishTypes[rolledIndex]);
                } else if (!isVanillaTreasure(caught)) {
                    if (CoreUtils.random.nextDouble() < level / 60.0) { // at level 60, never receive junk again
                        TreasureBag treasureBag = plugin.getModule(RPGme.MODULE_TREASURES);
                        item.setItemStack(treasureBag.rollTreasureChest(level)); // instead receive one of our treasures
                    }
                }
            }

            int exp = getFishingExpReward(item.getItemStack());

            addExp(e.getPlayer(), exp);
            e.setExpToDrop((int) Math.round(e.getExpToDrop() * expMultiplier.scale(level)));
        }
    }

    // TODO add event for killing fish entities

}
