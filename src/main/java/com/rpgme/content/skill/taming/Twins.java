package com.rpgme.content.skill.taming;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.math.ScaledValue;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.List;

/**
 * Created by Robin on 07/08/2016.
 */
public class Twins extends Ability<Taming> {

    private int unlocked;
    private ScaledValue twinChance;

    public Twins(Taming skill) {
        super(skill, "Twins", Taming.ABILITY_TWINS);
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        list.add("Twins chance:" + StringUtils.readableDecimal(twinChance.scale(forlevel)) + "%");
        list.add("Triplets chance:" + StringUtils.readableDecimal(twinChance.scale(forlevel) / 10) + "%");
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 10);
        config.addValue("chance", 0.45);

        messages.addValue("notification", "Every animal you breed now has a chance to produce twins, or even triplets! The chance this will occur will increase as you level up.");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        double chance = Math.min(1.0, Math.abs(config.getDouble("chance")));
        unlocked = config.getInt("unlocked");
        twinChance = new ScaledValue(unlocked, chance * 10, 100, chance * 100);

        addNotification(unlocked, Notification.ICON_UNLOCK, getDisplayName(), messages.getMessage("notification"));
    }

    @SuppressWarnings("incomplete-switch")
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBreed(CreatureSpawnEvent e) {
        //twins
        if (e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.BREEDING) {

            Player player = CoreUtils.getNearestPlayer(e.getLocation(), 6);
            if (!isEnabled(player))
                return;

            int level = getLevel(player);
            if (level >= unlocked && twinChance.isRandomChance(level)) {

                int amount = CoreUtils.random.nextInt(10) == 0 ? 2 : 1;
                for (int i = 0; i < amount; i++) {

                    Location location = CoreUtils.addNaturalOffset(e.getLocation().toVector(), 2).toLocation(e.getLocation().getWorld());

                    Entity ent = e.getEntity().getWorld().spawnEntity(location, e.getEntityType());
                    ((Ageable) ent).setBaby();

                    location.getWorld().spawnParticle(Particle.HEART, ent.getLocation().add(0, 2, 0), 10, 0.3, 0.2, 0.3, 1.0);
                    GameSound.play(Sound.ENTITY_FIREWORK_ROCKET_TWINKLE, location, 0.2);
                }
                int xp = ExpTables.getBreedExp(e.getEntityType()) * amount;
                addExp(player, xp);
            }
        }
    }
}
