package com.rpgme.content.skill.forging;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.Entities;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.MessageUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class QuickFix extends Ability<Forging> {

    private int unlockLevel, keepEnchantLevel;

    public QuickFix(Forging skill) {
        super(skill, "QuickFix", Forging.ABILITY_QUICK_FIX);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlockLevel", 20)
                .addValue("When upgraded, items will not lose their enchantments when using quick fix", "upgrade", 40);

        /* ConfigHelper.injectNotification\((.+?(?=,)), (.+?(?=,)), (.+?(?=\);)) */
        /* $1.addValue("notification$3", ConfigHelper.get(getClass(), $3) */

        messages.addValue("notification1", ConfigHelper.getBundledMessage(getClass(), "1"));
        messages.addValue("notification2", ConfigHelper.getBundledMessage(getClass(), "2"));
        messages.addValue("no_materials", ConfigHelper.getBundledMessage("ability_quickfix_nomaterials"));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        unlockLevel = config.getInt("unlockLevel");
        keepEnchantLevel = config.getInt("upgraded");

        /* ConfigHelper.getNotification\((.+?(?=,)), (.+?(?=,)), (.+?(?=\))) */
        /* $1.getMessage("notification$3" */
        addNotification(unlockLevel, Notification.upgradableIcon(1, 2), "Quick Fix", messages.getMessage("notification1"));//messages.getMessage("notification1"));
        addNotification(keepEnchantLevel, Notification.upgradableIcon(2, 2), "Quick Fix", messages.getMessage("notification2", 12, 23));
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {

    }


    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onRepair(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        // check item and condition
        if (!e.hasItem() || !e.hasBlock() || e.getClickedBlock().getType() != Material.ANVIL || !p.isSneaking() || !isEnabled(p) || Entities.getDamage(e.getItem()) == 0) {
            return;
        }

        int level = getLevel(p);
        if (level < unlockLevel) {
            return;
        }

        ItemStack result = e.getItem();
        Material item = result.getType();
        int seperator = item.name().indexOf('_');

        if (seperator < 0) {
            return;
        }

        Material cost = Forging.getRepairMaterial(item.name().substring(0, seperator).toLowerCase());

        if (cost == null) {
            return;
        }

        int ingots = Forging.getIngotsUsed(item.name().substring(seperator + 1).toLowerCase()) + 1;

        if (ingots <= 1) {
            return;
        }

        e.setCancelled(true);

        // take material
        if (!p.getInventory().removeItem(new ItemStack(cost)).isEmpty()) {
            MessageUtil.sendToActionBar(p, messages.getMessage("no_materials"));
            return;
        }

        // repair
        if (level < keepEnchantLevel) {
            for (Enchantment ench : result.getEnchantments().keySet()) {
                result.removeEnchantment(ench);
            }
        }

        int repair = (int) Math.round((double) item.getMaxDurability() / ingots);
        repair = Math.min(repair, Entities.getDamage(result));

        addExp(p, skill.getRepairExp(item, repair));

        Entities.setDamage(result, (Entities.getDamage(result) - repair));
        p.getInventory().setItemInMainHand(result);
        GameSound.play(Sound.BLOCK_ANVIL_USE, e.getClickedBlock().getLocation());
    }


}
