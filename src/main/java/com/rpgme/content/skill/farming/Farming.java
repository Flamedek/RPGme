package com.rpgme.content.skill.farming;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.event.RPGPlayerJoinEvent;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.treasure.TreasureBag;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.math.ScaledValue;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

public class Farming extends BaseSkill {

    private static final Set<BlockFace> faces = EnumSet.of(BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST);

    public static final int ABILITY_GAPPLE_HARVEST = Id.newId();
    public static final int ABILITY_REPLANT = Id.newId();

    private final ScaledValue treasureChance = new ScaledValue(10, 0.2, 100, 1.5);
    private final Map<String, Float> offlineExp = new HashMap<>();

    public final FarmingBlockListener blockData;

    public Farming() {
        super("Farming", SkillType.FARMING);
        blockData = new FarmingBlockListener(this);
        blockData.registerListeners();
    }

    @Override
    public Material getItemRepresentation() {
        return Material.MELON_SEEDS;
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if (forlevel >= treasureChance.minLevel) {
            list.add("Treasure Chance:" + treasureChance.readableScale(forlevel) + "%");
        }
    }

    private File getOfflineExpFile() {
        return new File(plugin.getUserDataFolder(), "offline_exp.dat");
    }

    @Override
    public void onEnable() {
        super.onEnable();

        registerAbility(ABILITY_GAPPLE_HARVEST, new GappleHarvest(this));
        registerAbility(ABILITY_REPLANT, new Replant(this));

        // in enable: read the offline exp file
        try (BufferedReader reader = new BufferedReader(new FileReader(getOfflineExpFile()))) {
            String line;

            while ((line = reader.readLine()) != null) {
                String[] split = line.split(" ");
                try {
                    offlineExp.put(split[0], Float.parseFloat(split[1]));
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException ignore) {
                }
            }
        } catch (IOException e) {
            plugin.getLogger().severe("Failed to read offline exp file: " + e.toString());
        }
    }

    @Override
    public void onDisable() {
        // in disable: write the offline exp file
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(getOfflineExpFile()))) {

            for (Entry<String, Float> entry : offlineExp.entrySet()) {
                writer.write(entry.getKey());
                writer.write(" ");
                writer.write(entry.getValue().toString());
                writer.write("\n");
                writer.flush();
            }
        } catch (IOException e) {
            plugin.getLogger().severe("Failed to save offline_exp.dat to disk. Containing data has been lost.");
            plugin.getLogger().severe(e.toString());
        }
    }

    @EventHandler
    public void onLogin(RPGPlayerJoinEvent event) {
        RPGPlayer player = event.getRPGPlayer();
        Float exp = offlineExp.remove(player.getUniqueId().toString());
        if (exp != null) {
            player.addExp(getId(), exp);
        }
    }

    public boolean isCrop(Material material) {
        return ExpTables.getFarmingHarvestExp(material) > 0;
    }

    /**
     * Add data for placed crop blocks
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onCropPlant(BlockPlaceEvent e) {
        if (!isEnabled(e.getPlayer())) {
            return;
        }

        Material crop = e.getBlock().getType();
        if (isCrop(crop)) {
            if (crop == Material.CHORUS_FLOWER && e.getBlockAgainst().getType() != Material.END_STONE) {
                return;
            }

            if (crop == Material.SUGAR_CANE && e.getBlockAgainst().getType() == Material.SUGAR_CANE) {
                blockData.setPlayerByPlaced(e.getBlock(), true);
            } else {
                blockData.setString(e.getBlock(), e.getPlayer().getUniqueId());
            }
        }
    }

    /**
     * Handles any (possably farming related) block when broken
     */
    public boolean handleBlockBreak(Block block, @Nullable Player player) {
        float exp = ExpTables.getFarmingHarvestExp(block.getType());
        if (exp < 1) {
            return false;
        }

        String ownerID = collectOwnerTag(block);

        if (block.getType() == Material.SUGAR_CANE) {
            boolean playerMade = false;
            int amount = 0;
            do {
                playerMade = playerMade || blockData.collectPlacedByPlayer(block);
                amount++;
            } while ((block = block.getRelative(BlockFace.UP)).getType() == Material.SUGAR_CANE);
            block = block.getRelative(BlockFace.DOWN, amount);

            if (playerMade) {
                return true;
            }
            exp *= amount;
        }
        if (isFinished(block)) {

            Player owner = ownerID != null ? plugin.getServer().getPlayer(UUID.fromString(ownerID)) : null;
            Player breaker = isEnabled(player) ? player : null;

            float ownerExp = breaker == null ? exp : exp / 2f;
            float breakerExp = breaker != null ? exp / 2f : 0;

            if (owner != null) {
                addExp(owner, ownerExp);
            } else if (ownerID != null) {

                Float stored = offlineExp.get(ownerID);
                float togive = (stored != null ? stored : 0f) + ownerExp;
                offlineExp.put(ownerID, togive);
            }

            if (breakerExp > 0) {
                addExp(player, breakerExp);
                int level = getLevel(player);
                if (treasureChance.isRandomChance(level)) {
                    TreasureBag.getInstance().spawnTreasure(block, level);
                }
            }
        }
        return true;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTreeGrow(StructureGrowEvent e) {
        Player p = e.getPlayer();
        if (p == null || !isEnabled(p)) {
            return;
        }

        float xp = ExpTables.getFarmingTreeExp(e.getSpecies());

        if (usedBonemeal(p)) {
            xp /= 2;
        }
        addExp(p, xp);
    }

    public boolean isFinished(Block block) {
        BlockData data = block.getBlockData();
        if (data instanceof Ageable) {
            Ageable ageable = (Ageable) block.getBlockData();
            return ageable.getAge() == ageable.getMaximumAge();
        }
        switch (block.getType()) {
            case CHORUS_PLANT:
            case CHORUS_FLOWER:
            case MELON:
            case PUMPKIN:
                return true;
            case SUGAR_CANE: {
                return block.getRelative(BlockFace.DOWN).getType() == Material.SUGAR_CANE ||
                        block.getRelative(BlockFace.UP).getType() == Material.SUGAR_CANE;
            }
            default:
                return false;
        }
    }

    private boolean usedBonemeal(Player p) {
        ItemStack item = p.getInventory().getItemInMainHand();
        return item != null && item.getType() == Material.BONE_MEAL;
    }

    public String collectOwnerTag(Block block) {
        switch (block.getType()) {
            case WHEAT:
            case PUMPKIN_STEM:
            case MELON_STEM:
            case POTATO:
            case CARROT:
            case NETHER_WART:
            case BEETROOTS:
            case CHORUS_PLANT:
                return blockData.collectString(block);

            case PUMPKIN:
            case MELON:
            case COCOA: {
                for (BlockFace face : faces) {
                    String tag = blockData.collectString(block.getRelative(face));
                    if (tag != null) {
                        return tag;
                    }
                }
                return null;
            }

            case SUGAR_CANE: {
                String tag = blockData.collectString(block);
                while (tag == null) {
                    block = block.getRelative(BlockFace.DOWN);
                    if (block.getType() != Material.SUGAR_CANE) {
                        break;
                    }
                    tag = blockData.getString(block);
                }
                return tag;
            }

            default:
                return null;
        }
    }

    public static boolean isGroundBlock(Material t) {
        return t == Material.GRASS || t == Material.DIRT || t == Material.FARMLAND || t == Material.SOUL_SAND || t == Material.SAND || t == Material.END_STONE;
    }

    public static boolean isGroundBlock(Block block) {
        return Farming.isGroundBlock(block.getType());
    }

}
