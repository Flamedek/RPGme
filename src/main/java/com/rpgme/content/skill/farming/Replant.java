package com.rpgme.content.skill.farming;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.Entities;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.UUID;

public class Replant extends Ability<Farming> {

    final String[] unlockKeys = {"crops", "crops2", "canes", "nether"};
    final int[] unlockValues = new int[unlockKeys.length];

    public Replant(Farming skill) {
        super(skill, "Replant", Farming.ABILITY_REPLANT);

    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);

        int defaultUnlock = 15;
        for (String key : unlockKeys) {
            config.addValue(key, defaultUnlock);
            defaultUnlock += 15;
        }
        messages.addValue("notification1", ConfigHelper.getBundledMessage(getClass(), "1"));
        messages.addValue("notification2", ConfigHelper.getBundledMessage(getClass(), "2"));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        for (int i = 0; i < unlockKeys.length; i++) {
            int unlock = config.getInt("Replant." + unlockKeys[i]);
            unlockValues[i] = Math.max(1, unlock);
        }

        String[] types = {"Wheat & Beetroot", "Sugar canes", "Potatoes & Carrots", "Nether warts & Chorus Plants"};

        for (int i = 0; i < unlockKeys.length; i++) {
            String title = types[i];

            String message = (i == 0 ? messages.getMessage("notification1") + '\n' : "") +
                    messages.getMessage("notification2", title);

            addNotification(unlockValues[i], i == 0 ? Notification.ICON_UNLOCK : Notification.ICON_UPGRADE,
                    getName() + StringUtils.CamelCase(unlockKeys[i]).replace("2", " II"), message);
        }
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onCropBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();
        if (!isEnabled(player)) {
            return;
        }

        Material cropType = block.getType();
        final Material seedType = getHeldSeeds(player.getInventory());
        if (seedType == null || !skill.isCrop(cropType) || (getSeedFor(cropType) == seedType && !skill.isFinished(block))) {
            return;
        }

        if (!isSuitableGround(block.getRelative(BlockFace.DOWN).getType(), seedType)) {
            return;
        }

        int level = getLevel(player);
        if (!isUnlocked(block.getType(), level)) {
            return;
        }

        takeHeldSeed(player.getInventory(), seedType);
        final UUID uid = player.getUniqueId();
        final BlockState newState = block.getState();
        newState.setType(getBlockFor(seedType));
        Entities.setAge(newState, CoreUtils.random.nextInt(3));

        // do effect
        new BukkitRunnable() {
            @Override
            public void run() {

                if (isSuitableGround(newState.getBlock().getRelative(BlockFace.DOWN).getType(), seedType)) {
                    newState.update(true, true);
                    skill.blockData.setString(newState.getBlock(), uid);
                    GameSound.play(Sound.BLOCK_GRASS_PLACE, newState.getLocation(), 2f, 1.2f);
                }

            }
        }.runTaskLater(plugin, 2L);

    }


    private int getUnlock(String name) {
        for (int i = 0; i < unlockKeys.length; i++) {
            if (unlockKeys[i].equalsIgnoreCase(name)) {
                return unlockValues[i];
            }
        }
        return 1;
    }

    public boolean isUnlocked(Material type, int level) {
        switch (type) {
            case WHEAT:
            case BEETROOTS:
                return level >= getUnlock("crops");
            case SUGAR_CANE:
                return level >= getUnlock("canes");
            case POTATO:
            case COCOA:
            case CARROT:
                return level >= getUnlock("crops2");
            case NETHER_WART:
            case CHORUS_PLANT:
            case CHORUS_FLOWER:
                return level >= getUnlock("nether");
            default:
                return false;
        }
    }

    public boolean isSeed(Material material) {
        return getBlockFor(material) != null;
    }

    // seed to block
    public Material getBlockFor(Material seed) {
        if (seed == null) return null;
        switch (seed) {
            case BEETROOT_SEEDS:
                return Material.BEETROOTS;
            case WHEAT_SEEDS:
                return Material.WHEAT;
            case SUGAR_CANE:
            case POTATO:
            case CARROT:
            case NETHER_WART:
            case COCOA:
            case CHORUS_FLOWER:
                return seed;
            default:
                return null;
        }
    }

    // block to seed
    public Material getSeedFor(Material block) {
        switch (block) {
            case WHEAT:
                return Material.WHEAT;
            case SUGAR_CANE:
            case POTATO:
            case CARROT:
            case NETHER_WART:
            case COCOA:
                return block;
            case CHORUS_PLANT:
            case CHORUS_FLOWER:
                return Material.CHORUS_FLOWER;
            default:
                return null;
        }
    }

    public Material getHeldSeeds(PlayerInventory inv) {
        ItemStack item = inv.getItemInOffHand();
        Material mat = item.getType();
        if (isSeed(mat)) {
            item.setAmount(item.getAmount() - 1);
            inv.setItemInOffHand(item.getAmount() > 0 ? item : null);
            return mat;
        }
        item = inv.getItemInMainHand();
        mat = item.getType();
        if (isSeed(mat)) {
            item.setAmount(item.getAmount() - 1);
            inv.setItemInMainHand(item.getAmount() > 0 ? item : null);
            return mat;
        }
        return null;
    }

    public void takeHeldSeed(PlayerInventory inv, Material seedType) {
        ItemStack item;
        if ((item = inv.getItemInOffHand()).getType() == seedType) {
            item.setAmount(item.getAmount() - 1);
            inv.setItemInOffHand(item.getAmount() > 0 ? item : null);
        } else if ((item = inv.getItemInMainHand()).getType() == seedType) {
            item.setAmount(item.getAmount() - 1);
            inv.setItemInMainHand(item.getAmount() > 0 ? item : null);
        }
    }

    public boolean isSuitableGround(Material ground, Material crop) {
        switch (crop) {
            case POTATO:
            case CARROT:
            case BEETROOT_SEEDS:
            case WHEAT:
                return ground == Material.FARMLAND;
            case SUGAR_CANE:
                return ground == Material.GRASS || ground == Material.SAND;
            case NETHER_WART:
                return ground == Material.SOUL_SAND;
            case CHORUS_FLOWER:
                return ground == Material.END_STONE;

            default:
                return false;
        }
    }

}
