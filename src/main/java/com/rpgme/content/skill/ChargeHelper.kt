package com.rpgme.content.skill

import com.rpgme.plugin.RPGme
import com.rpgme.plugin.api.Listener
import com.rpgme.plugin.skill.Ability
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.ChatMessageType
import net.md_5.bungee.api.chat.ComponentBuilder
import org.bukkit.entity.Player
import org.bukkit.event.Event
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.ceil


data class ChargeState(val startTime: Long, var lastTime: Long, var chargeCount: Int)

interface ChargeHelperListener {
    fun onChargeProgress(player: Player, state: ChargeState)

    fun isChargeEvent(event: PlayerInteractEvent): Boolean

    fun onChargeComplete(player: Player)

}

class ChargeHelper @JvmOverloads constructor(
    val ability: Ability<*>,
    val listener: ChargeHelperListener,
    val chargeDuration: Long = 1200L
) : Listener<RPGme>(ability.plugin) {

    // time allowed between charge events.
    // events occur every 200ms
    private val maxChargeDelay = 250L
    private val interactionGrace = 500L

    private val chargeStates = WeakHashMap<Player, ChargeState>()
    private val grace = HashMap<UUID, Long>()

    var progressChar = "="
    var chargeColor = ChatColor.AQUA
    var completeColor = ChatColor.GREEN

    @EventHandler(priority = EventPriority.HIGH)
    fun onChargeEvent(event: PlayerInteractEvent) {
        if (event.action != Action.RIGHT_CLICK_AIR && event.action != Action.RIGHT_CLICK_BLOCK) {
            return
        }

        val player = event.player
        val now = System.nanoTime()

        if (event.clickedBlock?.blockData?.material?.isInteractable == true &&
            event.useInteractedBlock() != Event.Result.DENY
        ) {

            // disable interaction shortly after activation
            grace[player.uniqueId]?.let { graceEnd ->
                if (graceEnd > now) {
                    event.setUseInteractedBlock(Event.Result.DENY)
                } else {
                    grace.remove(player.uniqueId)
                }
            }
            return
        }

        if (listener.isChargeEvent(event)) {

            val state = chargeStates[player]

            if (state != null) {
                if (now - state.lastTime > toNanos(maxChargeDelay)) {

                    // not a consecutive charge
                    chargeStates.remove(player)
                } else {

                    // still charging!
                    state.lastTime = now
                    state.chargeCount++

                    listener.onChargeProgress(player, state)

                    // cancel interactions while charging
                    if (now - state.startTime > toNanos(maxChargeDelay)) {
                        event.setUseInteractedBlock(Event.Result.DENY)
                    }
                    // check if complete
                    if (now - state.startTime > toNanos(chargeDuration)) {
                        listener.onChargeComplete(player)
                        chargeStates.remove(player)
                        grace[player.uniqueId] = now + toNanos(interactionGrace)
                    }
                    return
                }
            }

            // start charging
            chargeStates[player] = ChargeState(now, now, 0).also {
                listener.onChargeProgress(player, it)
            }
        }
    }

    fun cancelCharge(player: Player) {
        chargeStates.remove(player)
    }

    fun displayChargeProgress(player: Player, state: ChargeState) {
        val delta = TimeUnit.NANOSECONDS.toMillis(state.lastTime - state.startTime)
        if (delta < maxChargeDelay) {
            return
        }

        val blocks = 16
        val progress = ((delta - maxChargeDelay).toDouble() / (chargeDuration - maxChargeDelay)).coerceIn(0.0, 1.0)
        val activeCount = ceil(progress * blocks).toInt()
        val inactiveCount = blocks - activeCount

        val builder = ComponentBuilder(ability.skill.manager.colorLight() + ability.displayName)
            .append("  [").color(ChatColor.DARK_GRAY)
            .append(progressChar.repeat(activeCount)).color(if (inactiveCount == 0) completeColor else chargeColor)
            .append(progressChar.repeat(inactiveCount)).color(ChatColor.GRAY)
            .append("]").color(ChatColor.DARK_GRAY)
            .create()

        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, *builder)
    }

    private fun toNanos(millis: Long) = TimeUnit.MILLISECONDS.toNanos(millis)

}