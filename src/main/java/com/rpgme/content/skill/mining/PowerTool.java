package com.rpgme.content.skill.mining;

import com.rpgme.content.skill.ChargeHelper;
import com.rpgme.content.skill.ChargeHelperListener;
import com.rpgme.content.skill.ChargeState;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.*;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.SimpleCooldown;
import com.rpgme.plugin.util.math.ScaledValue;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class PowerTool<S extends Skill> extends Ability<S> implements ChargeHelperListener {

    public interface BlockBreaker {

        void onBlockBreak(BlockBreakEvent event);

    }

    public static final int ID = Id.newId();

    private int unlockLevel, upgradeLevel, infinityLevel;

    private final Cooldown cooldown = new SimpleCooldown(plugin);
    private final ScaledValue cooldownValue, durationValue;
    private final ChargeHelper chargeHelper;

    private Collection<Material> materials;
    private String toolSuffix;
    private boolean inNestedEvent = false;

    // map holding the end-time for active users;
    private final Map<UUID, Long> endtimeMap = new WeakHashMap<>();
    // map holding the face of the block they are breaking
    private final Map<UUID, BlockFace> blockfaceMap = new WeakHashMap<>();


    public PowerTool(S skill, String toolSuffix, EnumSet<Material> materials, int[] cooldownDuration, int[] abilityDuration) {
        super(skill, "Power Tool", ID);
        this.toolSuffix = toolSuffix;
        this.materials = materials;

        cooldownValue = new ScaledValue(unlockLevel, cooldownDuration[0], 100, cooldownDuration[1]);
        durationValue = new ScaledValue(unlockLevel, abilityDuration[0], 100, abilityDuration[1]);
        chargeHelper = new ChargeHelper(this, this);
        chargeHelper.registerListeners();
    }

    @Override
    public void onEnable() {
        super.onEnable();
        chargeHelper.registerListeners();
    }

    @Override
    public void onDisable() {
        chargeHelper.unregisterListeners();
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if (forlevel >= unlockLevel) {
            list.add("Power Tool duration:" + (forlevel < infinityLevel ? durationValue.readableScale(forlevel) + 's' : "infinite"));
            list.add("Power Tool cooldown:" + (forlevel < infinityLevel ? cooldownValue.readableScale(forlevel) + 's' : "none"));
        }
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 5)
                .addValue("unlock full 3x3 area", "upgrade1", 50)
                .addValue("unlock infinite usage", "upgrade2", 100);
        messages.addValue("notification1", ConfigHelper.getBundledMessage(getClass(), "1"));
        messages.addValue("notification2", ConfigHelper.getBundledMessage(getClass(), "2"));
        messages.addValue("notification3", ConfigHelper.getBundledMessage(getClass(), "3"));
        messages.addValue("activated", ConfigHelper.getBundledMessage("ability_powertool_activated"));
        messages.addValue("deactivated", ConfigHelper.getBundledMessage("ability_powertool_deactivated"));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        unlockLevel = getConfig().getInt("unlocked", 1);
        upgradeLevel = getConfig().getInt("upgrade1", 50);
        infinityLevel = getConfig().getInt("upgrade2", 100);

        // register notifications
        addNotification(unlockLevel, Notification.upgradableIcon(1, 3), getDisplayName(), messages.getMessage("notification1", toolSuffix));
        addNotification(upgradeLevel, Notification.upgradableIcon(2, 3), getDisplayName(), messages.getMessage("notification2"));
        addNotification(infinityLevel, Notification.upgradableIcon(3, 3), getDisplayName(), messages.getMessage("notification3"));
    }

    //
    // Charging

    @Override
    public boolean isChargeEvent(@NotNull PlayerInteractEvent event) {
        Player player = event.getPlayer();
        return isAcceptedItem(event.getItem()) && getLevel(player) >= unlockLevel && isEnabled(player);
    }

    @Override
    public void onChargeProgress(@NotNull Player player, @NotNull ChargeState state) {
        float pitch = 0.5f + state.getChargeCount() * 0.25f;
        GameSound.play(Sound.BLOCK_STONE_BREAK, player, 1f, pitch);
        chargeHelper.displayChargeProgress(player, state);
    }

    @Override
    public void onChargeComplete(@NotNull Player player) {
        if (isActive(player)) {
            setActive(player, false);
            return;
        }

        if (cooldown.isOnCooldown(player)) {
            sendOnCooldownMessage(player, cooldown, true);
            return;
        }

        GameSound.play(Sound.ENTITY_ARROW_HIT_PLAYER, player, 0.3f, 1.5f);
        MessageUtil.sendToActionBar(player, ChatColor.GREEN.toString() + getDisplayName());
        setActive(player, true);
    }


    //
    // Events

    private boolean isAcceptedItem(ItemStack item) {
        return item != null && item.getType().name().endsWith(toolSuffix);
    }

    private boolean isAcceptedBlock(Player p, Block block) {
        return materials.contains(block.getType());
    }

    private boolean isAcceptedClick(Action action, Block block) {
        return CoreUtils.isRightClick(action) && (block == null || !CoreUtils.hasClickAction(block.getType()));
    }

    public boolean isActive(Player p) {
        Long endtime = endtimeMap.get(p.getUniqueId());
        return endtime != null && (endtime == 0 || endtime > System.currentTimeMillis());
    }


    public void setActive(Player p, boolean active) {
        int level = getLevel(p);
        if (active) {

            if (level < unlockLevel) {
                return;
            }

            String message = messages.getMessage("activated");
            if (level < infinityLevel) {

                message += " &b: " + durationValue.readableScale(level) + 's';
                endtimeMap.put(p.getUniqueId(), System.currentTimeMillis() + (long) (durationValue.scale(level) * 1000L));
            } else {
                endtimeMap.put(p.getUniqueId(), 0L);
            }
            MessageUtil.sendToActionBar(p, StringUtils.colorize(message));

        } else {

            endtimeMap.remove(p.getUniqueId());
            blockfaceMap.remove(p.getUniqueId());
            chargeHelper.cancelCharge(p);

            String message = messages.getMessage("deactivated");

            if (level < infinityLevel) {
                long cooldownDuration = (long) (cooldownValue.scale(level) * 1000L);
                cooldown.add(p, cooldownDuration);
                message += (" &7cooldown: &b" + cooldownValue.readableScale(level) + 's');
            }

            MessageUtil.sendToActionBar(p, StringUtils.colorize(message));
        }
    }

    @EventHandler
    public void onBlockClick(PlayerInteractEvent event) {
        if (event.getAction() == Action.LEFT_CLICK_BLOCK && isActive(event.getPlayer()) && isAcceptedItem(event.getItem())) {
            blockfaceMap.put(event.getPlayer().getUniqueId(), event.getBlockFace());
        }
    }

    //
    // Effect

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPowertoolUse(BlockBreakEvent event) {
        if (inNestedEvent) {
            return;
        }

        Player p = event.getPlayer();

        Long endtime = endtimeMap.get(p.getUniqueId());
        if (endtime == null) {
            return;
        }

        if (endtime > 0 && endtime < System.currentTimeMillis()) {
            setActive(p, false);
            return;
        }

        ItemStack tool = p.getInventory().getItemInMainHand();
        if (!isAcceptedBlock(p, event.getBlock()) || !isAcceptedItem(tool)) {
            return;
        }

        if (!isEnabled(p)) {
            return;
        }

        BlockFace face = blockfaceMap.remove(p.getUniqueId());
        if (face == null) face = CoreUtils.getNearestFace(event.getBlock(), p.getEyeLocation());

        boolean reduced = getLevel(p) < upgradeLevel;
        inNestedEvent = true;
        for (Block b : getSurroundingBlocks(event.getBlock(), face, reduced)) {

            if (isAcceptedBlock(p, b)) {
                BlockBreakEvent breakEvent = new BlockBreakEvent(b, p);
                Bukkit.getPluginManager().callEvent(breakEvent);

                if (!breakEvent.isCancelled()) {
                    ((BlockBreaker) skill).onBlockBreak(breakEvent);
                    b.breakNaturally(tool);
                }
            }
        }
        inNestedEvent = false;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onItemInHandChanged(PlayerItemHeldEvent e) {
        Player p = e.getPlayer();
        if (isActive(p) && isAcceptedItem(p.getInventory().getItem(e.getNewSlot()))) {
            // show activation message
            String message = messages.getMessage("activated");
            Long endTime = endtimeMap.get(p.getUniqueId());
            if (endTime != null && endTime > 0) {
                long remaining = endTime - System.currentTimeMillis();
                message += " &b: " + StringUtils.readableDecimal(remaining / 1000d) + 's';
            }
            MessageUtil.sendToActionBar(p, StringUtils.colorize(message));
        }
    }

    public static Collection<Block> getSurroundingBlocks(Block clicked, BlockFace face, boolean reduced) {
        int[][] pattern;

        switch (face) {
            case EAST:
            case WEST:
                pattern = PowerTool.cylVertical2;
                break;
            case NORTH:
            case SOUTH:
                pattern = PowerTool.cylVertical;
                break;
            case UP:
            case DOWN:
            default:
                pattern = PowerTool.cylFlat;
                break;
        }

        Set<Block> set = new HashSet<>();
        set.add(clicked);

        if (reduced) {
            for (int i : PowerTool.reduced) {
                int[] arr = pattern[i];
                set.add(clicked.getRelative(arr[0], arr[1], arr[2]));
            }
        } else {
            for (int[] arr : pattern) {
                set.add(clicked.getRelative(arr[0], arr[1], arr[2]));
            }
        }
        return set;
    }

    public static final int[] reduced = new int[]{1, 3, 4, 6};

    public static final int[][] cylVertical2 = {
        new int[]{0, 1, -1}, new int[]{0, 1, 0}, new int[]{0, 1, 1},
        new int[]{0, 0, -1}, new int[]{0, 0, 1},
        new int[]{0, -1, -1}, new int[]{0, -1, 0}, new int[]{0, -1, 1}};

    public static final int[][] cylVertical = {
        new int[]{-1, 1, 0}, new int[]{0, 1, 0}, new int[]{1, 1, 0},
        new int[]{-1, 0, 0}, new int[]{1, 0, 0},
        new int[]{-1, -1, 0}, new int[]{0, -1, 0}, new int[]{1, -1, 0}};

    public static final int[][] cylFlat = new int[][]{
        new int[]{-1, 0, 1}, new int[]{0, 0, 1}, new int[]{1, 0, 1},
        new int[]{-1, 0, 0}, new int[]{1, 0, 0},
        new int[]{-1, 0, -1}, new int[]{0, 0, -1}, new int[]{1, 0, -1}};


}
