package com.rpgme.content.skill.mining;

import com.google.common.collect.Lists;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.command.CoreCommand;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class AutosmeltCommand extends CoreCommand {

    protected static final String SETTING_KEY = "noautosmelt";

    public AutosmeltCommand(RPGme plugin) {
        super(plugin, "autosmelt", "rpgme.command.autosmelt");
        setConsoleAllowed(false);
        setDescription("Toggle the autosmelt ability for the Mining skill.");
        setUsage("/autosmelt on/off");
    }

    @Override
    public List<String> getTabComplete(CommandSender sender, String label,
                                       List<String> args) {
        if (args.size() == 1) {
            return Lists.newArrayList("on", "off");
        }
        return Collections.emptyList();
    }

    @Override
    public void execute(CommandSender sender, String alias, List<String> flags) {

        Player p = (Player) sender;
        RPGPlayer rp = plugin.getPlayer(p);

        if (rp == null) {
            return;
        }

        boolean autosmelt = rp.getSetting(SETTING_KEY) == null;

        if (flags.remove("on")) {
            autosmelt = true;
        } else if (flags.remove("off")) {
            autosmelt = false;
        } else {
            autosmelt = !autosmelt;
        }

        if (autosmelt) {
            p.sendMessage(StringUtils.colorize("&eMining autosmelt: &aON"));
            rp.setSetting(SETTING_KEY, null);

        } else {
            p.sendMessage(StringUtils.colorize("&eMining autosmelt: &cOFF"));
            rp.setSetting(SETTING_KEY, "true");

        }
    }

//	@Override
//	public void addCommandHelp(CommandSender sender, List<String> list) {
//		if(hasPermission(sender)) {
//			list.add("/autosmelt:"+getDescription());
//		}
//	}

}
