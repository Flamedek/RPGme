package com.rpgme.content.skill.woodcutting;

import org.bukkit.block.Block;

import java.util.Objects;
import java.util.Set;

public class TimberBlockResult {

    public static final int OK = 0;
    public static final int ERR_PLAYER_PLACED = 1;
    public static final int ERR_TO_LARGE = 3;
    public static final int ERR_NO_TREE = 4;

    private int statusCode;
    private Set<Block> blocks;

    public TimberBlockResult(int statusCode, Set<Block> blocks) {
        this.statusCode = statusCode;
        this.blocks = blocks;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Set<Block> getBlocks() {
        return blocks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimberBlockResult that = (TimberBlockResult) o;
        return statusCode == that.statusCode &&
                Objects.equals(blocks, that.blocks);
    }

    @Override
    public int hashCode() {

        return Objects.hash(statusCode, blocks);
    }
}
