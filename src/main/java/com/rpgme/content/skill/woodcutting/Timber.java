package com.rpgme.content.skill.woodcutting;

import com.rpgme.content.skill.ChargeHelper;
import com.rpgme.content.skill.ChargeHelperListener;
import com.rpgme.content.skill.ChargeState;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.blockmeta.PlayerPlacedListener;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.*;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.VarEnergyPerSecCooldown;
import com.rpgme.plugin.util.math.ScaledValue;
import kotlin.Pair;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Timber extends Ability<Woodcutting> implements ChargeHelperListener {

    public static final String META_NAME = "RPGme_timber";

    private static final Vector[] checkArea = {
            new Vector(0, 1, 0),
            new Vector(0, 0, 1),
            new Vector(1, 0, 0),
            new Vector(0, 0, -1),
            new Vector(-1, 0, 0),
            new Vector(0, -1, 0),
    };

    private static final int ENERY_COST = 40;
    private static final int MIN_TREE_HEIGHT = 3;

    private final Cooldown cooldown = new VarEnergyPerSecCooldown(plugin, ENERY_COST, 100);
    private ScaledValue cooldownSpeedValue;
    private ScaledValue maxSizeValue;
    private ChargeHelper chargeHelper;

    private int unlock;

    private final Set<UUID> activePlayers = new HashSet<>();

    public Timber(Woodcutting skill) {
        super(skill, "Timber", Woodcutting.ABILITY_TIMBER);
    }

    @Override
    public void onEnable() {
        super.onEnable();
        chargeHelper = new ChargeHelper(this, this);
        chargeHelper.registerListeners();
    }

    @Override
    public void onDisable() {
        chargeHelper.unregisterListeners();
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);

        config.addValue("unlocked", 15);
        config.addValue("Maximum amount of blocks per tree at unlock level", "max-size-start", 150);
        config.addValue("Maximum amount of blocks per tree at max level", "max-size-end", 600);

        messages.addValue("tree_to_big", "Tree too big");
        messages.addValue("not_a_tree", "Not a Tree");
        messages.addValue("placed_by_player", "Tree is placed by player");
        messages.addValue("no_durability", "Axe is too damaged");
        messages.addValue("on_active", "Timber activated! The next tree you chop will fall drop entirely.");
        messages.addValue("notification", ConfigHelper.getBundledMessage(getClass(), ""));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlock = getConfig().getInt("unlocked");
        cooldownSpeedValue = new ScaledValue(unlock, 1, 100, 6);
        maxSizeValue = new ScaledValue(unlock, config.getInt("max-tree-start", 150), 100, config.getInt("max-size-end", 600));

        addNotification(unlock, Notification.ICON_UNLOCK, getDisplayName(), messages.getMessage("notification"));
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if (forlevel >= unlock) {
            list.add("Timber max cooldown:" + StringUtils.readableDecimal(ENERY_COST / cooldownSpeedValue.scale(forlevel)) + "s");
            list.add("Timber max tree size:" + (int) maxSizeValue.scale(forlevel));
        }
    }

    //
    // Charging

    @Override
    public boolean isChargeEvent(@NotNull PlayerInteractEvent event) {
        Player player = event.getPlayer();
        return Materials.isAxe(event.getMaterial()) && !activePlayers.contains(event.getPlayer().getUniqueId()) &&
                getLevel(player) >= unlock && isEnabled(player);
    }

    @Override
    public void onChargeProgress(@NotNull Player player, @NotNull ChargeState state) {
        float pitch = 0.5f + state.getChargeCount() * 0.25f;
        GameSound.play(Sound.BLOCK_WOOD_BREAK, player, 1f, pitch);
        chargeHelper.displayChargeProgress(player, state);
    }

    @Override
    public void onChargeComplete(@NotNull Player player) {
        if (cooldown.isOnCooldown(player)) {
            sendOnCooldownMessage(player, cooldown, true);
            return;
        }

        GameSound.play(Sound.ENTITY_ARROW_HIT_PLAYER, player, 0.3f, 1.5f);
        player.sendMessage(ChatColor.GRAY.toString() + getMessages().getMessage("on_active"));
        MessageUtil.sendToActionBar(player, ChatColor.GREEN.toString() + getDisplayName());
        activePlayers.add(player.getUniqueId());
    }

    private void cancelAbility(@NotNull Player player) {
        if (activePlayers.remove(player.getUniqueId())) {
            chargeHelper.cancelCharge(player);
            GameSound.play(Sound.ENTITY_ENDER_DRAGON_FLAP, player, 0.5f, 0.8f);
            MessageUtil.sendToActionBar(player, ChatColor.RED.toString() + ChatColor.STRIKETHROUGH.toString() + getDisplayName());
        }
    }


    //
    // Events

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onAbility(BlockBreakEvent e) {
        Player p = e.getPlayer();
        ItemStack item = p.getInventory().getItemInMainHand();

        if (activePlayers.contains(p.getUniqueId())) {
            if (e.getBlock().hasMetadata(META_NAME) ||
                    !Woodcutting.isWood(e.getBlock().getType()) ||
                    !Materials.isAxe(item.getType()) ||
                    getLevel(p) < unlock ||
                    !isEnabled(p)) {
                cancelAbility(e.getPlayer());
            } else {
                dropTree(e.getBlock().getState(), p);
                activePlayers.remove(p.getUniqueId());
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockDropped(EntityChangeBlockEvent e) {
        if (e.getEntity().hasMetadata(META_NAME)) {
            e.setCancelled(true);
            ItemStack item = new ItemStack(e.getTo());
            e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), item);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onItemInHandChanged(PlayerItemHeldEvent e) {
        cancelAbility(e.getPlayer());
    }

    //
    // Timber

    private void dropTree(BlockState start, Player player) {
        Bukkit.getScheduler().runTaskAsynchronously(RPGme.getInstance(), () -> {

            final TimberBlockResult result = getTreeResult(start, player);

            Bukkit.getScheduler().runTask(RPGme.getInstance(), () -> {

                Set<Block> blocks = result.getBlocks();
                String error = "";
                switch (result.getStatusCode()) {
                    case TimberBlockResult.OK: {
                        if (!useDurability(player, blocks)) {
                            error = messages.getMessage("no_durability");
                        } else {
                            dropBlocks(start, player, blocks);
                            long energypersec = (long) cooldownSpeedValue.scale(getLevel(player)) * 1000;
                            cooldown.add(player, energypersec);
                        }
                        break;
                    }
                    case TimberBlockResult.ERR_NO_TREE: {
                        error = messages.getMessage("not_a_tree");
                        break;
                    }
                    case TimberBlockResult.ERR_TO_LARGE: {
                        int found = result.getBlocks().size();
                        String count;
                        if (found >= maxSizeValue.maxvalue) {
                            count = maxSizeValue.maxvalue + "+";
                        } else {
                            count = found + " / " + (int) maxSizeValue.scale(getLevel(player));
                        }
                        error = messages.getMessage("tree_to_big") + count;
                        break;
                    }
                    case TimberBlockResult.ERR_PLAYER_PLACED: {
                        error = messages.getMessage("placed_by_player");
                        break;
                    }
                }

                if (!error.isEmpty()) {
                    MessageUtil.sendError(player, error);
                }
            });
        });
    }

    private TimberBlockResult getTreeResult(BlockState start, Player player) {
        int level = getLevel(player);
        int blockLimit = (int) maxSizeValue.scale(level);
        int checkLimit = (int) maxSizeValue.maxvalue;

        Set<Block> toDrop = new HashSet<>();
        Set<Block> toCheck = new HashSet<>();
        Set<Block> toAdd = new HashSet<>();
        Set<Block> invalids = new HashSet<>();

        toCheck.add(start.getBlock());

        int playerPlacedCount = 0;
        int woodHeight = 0;

        // iterate over blocks
        do {
            for (Block b : toCheck) {
                if (toDrop.contains(b))
                    continue;

                for (Vector dir : checkArea) {

                    Block other = b.getLocation().add(dir).getBlock();

                    if (!Materials.isSameTreeType(start.getType(), other.getType())) {
                        continue;
                    }

                    if (Materials.isBush(other.getType())) {
                        toAdd.add(other);
                    } else if (Woodcutting.isWood(other.getType())) {
                        if (PlayerPlacedListener.getInstance().isPlayerPlaced(other)) {
                            playerPlacedCount++;
                        }
                        toAdd.add(other);
                        woodHeight = Math.max(woodHeight, other.getY() - start.getY() + 1);
                    } else if (shouldDestroy(other.getType())) {
                        other.breakNaturally();
                    } else if (other.getType().isSolid()) {
                        invalids.add(other);
                    }
                }

                toDrop.add(b);
            }

            toCheck.addAll(toAdd);
            toAdd.clear();

        } while (toDrop.size() <= checkLimit && !toDrop.containsAll(toCheck));

        // find outcome
        int status;
        if (toDrop.size() > blockLimit) {
            status = TimberBlockResult.ERR_TO_LARGE;
        } else if (woodHeight < MIN_TREE_HEIGHT || invalids.size() > toDrop.size() / 2) {
            status = TimberBlockResult.ERR_NO_TREE;
        } else if ((float) playerPlacedCount / toDrop.size() > 0.1) {
            status = TimberBlockResult.ERR_PLAYER_PLACED;
        } else {
            status = TimberBlockResult.OK;
        }

        toDrop.remove(start.getBlock());
        return new TimberBlockResult(status, toDrop);
    }

    private void dropBlocks(BlockState start, Player player, Set<Block> toDrop) {
        Vector dir = Vector.getRandom().normalize().setY(0.3);
        int woodblocks = 1;

        List<Pair<Location, BlockState>> blockLocations = new ArrayList<>();

        // Break blocks
        for (Block b : toDrop) {
            b.setMetadata(META_NAME, CoreUtils.FIXED_META_VALUE);
            BlockBreakEvent blockEvent = new BlockBreakEvent(b, player);
            Bukkit.getServer().getPluginManager().callEvent(blockEvent);

            if (!blockEvent.isCancelled()) {
                BlockState state = b.getState();

                if (Materials.isBush(state.getType())) {
                    if (b.getRelative(BlockFace.UP).getType() == Material.SNOW) {
                        b.getRelative(BlockFace.UP).setType(Material.AIR);
                    }

                    if (CoreUtils.random.nextDouble() < 0.12) {
                        b.setType(Material.AIR);
                        blockLocations.add(new Pair<>(b.getLocation(), state));
                    } else {
                        LeavesDecayEvent leavesDecayEvent = new LeavesDecayEvent(b);
                        Bukkit.getServer().getPluginManager().callEvent(leavesDecayEvent);
                        b.breakNaturally();
                    }

                } else {
                    woodblocks++;
                    GameSound.play(Sound.BLOCK_WOOD_BREAK, b.getLocation());

                    b.setType(Material.AIR);
                    blockLocations.add(new Pair<>(b.getLocation(), state));
                }
            }
            b.removeMetadata(META_NAME, RPGme.getInstance());
        }

        // Spawn FallingBlocks in their place
        for (Pair<Location, BlockState> entry : blockLocations) {
            BlockState state = entry.getSecond();
            int height = state.getY() - start.getY() + 1;
            double speed = Math.min(height * 0.04 + 0.2, 0.6);
            FallingBlock block = state.getWorld().spawnFallingBlock(entry.getFirst(), state.getBlockData());
            block.setMetadata(META_NAME, CoreUtils.FIXED_META_VALUE);
            block.setVelocity(dir.clone().multiply(speed));
            block.setDropItem(true);
            block.setHurtEntities(true);
        }

        float xp = woodblocks * 5f;
        addExp(player, xp);
    }

    private boolean useDurability(Player player, Set<Block> toDrop) {
        ItemStack axe = player.getInventory().getItemInMainHand();

        if (!Materials.isAxe(axe.getType())) {
            return false;
        }

        double stability = axe.containsEnchantment(Enchantment.DURABILITY) ? axe.getEnchantmentLevel(Enchantment.DURABILITY) + 1 : 1;
        String type = axe.getType().name();
        if (type.startsWith("WOOD")) {
            stability += 0.01;
        } else if (type.startsWith("STONE")) {
            stability += 0.08;
        } else if (type.startsWith("IRON")) {
            stability += 0.2;
        } else if (type.startsWith("GOLD")) {
            stability += 0.17;
        } else if (type.startsWith("DIAMOND")) {
            stability += 1;
        }

        int durability = (int) (Entities.getDamage(axe) + (toDrop.size() / stability));

        if (durability > axe.getType().getMaxDurability()) {
            return false;
        } else {
            Entities.setDamage(axe, durability);
            player.getInventory().setItemInMainHand(axe);
        }
        return true;
    }

    private boolean shouldDestroy(Material mat) {
        return mat == Material.VINE || mat == Material.COCOA;
    }


}
