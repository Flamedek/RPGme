package com.rpgme.content.skill.defence;

import com.rpgme.content.nms.NMS;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.ExpFix;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.SimpleCooldown;
import com.rpgme.plugin.util.effect.PotionEffectUtil;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class SecondWind extends Ability<Defence> {

    private int unlock, upgraded;
    private boolean keepInventory;
    private double expCost;
    private Cooldown cooldown;

    private final Set<Task> currentTasks = Collections.synchronizedSet(new HashSet<>());
    private final HashMap<UUID, Location> deathLocations = new HashMap<>();

    public SecondWind(Defence skill) {
        super(skill, "Second Wind", Defence.ABILITY_SECOND_WIND);

    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {


    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 50)
                .addValue("cooldown in seconds", "cooldown", 180)
                .addValue("Get short invulnerability after spawning", "upgraded", 65)
                .addValue("keep inventory", true)
                .addValue("amount of the players exp that should be dropped [0-1]", "exp drop", 0.5)
        ;
        messages.addValue("notification1", ConfigHelper.getBundledMessage(getClass(), "1"));
        messages.addValue("notification2", ConfigHelper.getBundledMessage(getClass(), "2"));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlock = getConfig().getInt("unlocked", 50);
        upgraded = getConfig().getInt("upgraded", 65);
        keepInventory = getConfig().getBoolean("keep inventory", true);
        expCost = getConfig().getDouble("exp drop", 0.5);

        int cdDuration = getConfig().getInt("Second Wind.cooldown", 180);
        cooldown = new SimpleCooldown(plugin, cdDuration * 1000);

        addNotification(unlock, Notification.ICON_UNLOCK, getDisplayName(), messages.getMessage("notification1", cdDuration + "s"));
        addNotification(upgraded, Notification.ICON_UPGRADE, getDisplayName(), messages.getMessage("notification2"));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDeath(PlayerDeathEvent e) {

        Player p = e.getEntity();

        if (isEnabled(p) && getLevel(p) >= unlock) {

            if (cooldown.isOnCooldown(p)) {
                if (NMS.isHooked()) {
                    NMS.packets.sendTitleLength(p, 5, 20, 30);
                    NMS.packets.sendTitle(p, ChatColor.DARK_RED + "You died");
                }
                return;
            }

            cooldown.add(p);

            e.setKeepInventory(keepInventory);

            int exp = ExpFix.getTotalExperience(p);
            e.setDeathMessage(null);
            e.setDroppedExp((int) (exp * expCost));
            e.setNewTotalExp((int) (exp * (1 - expCost)));

            Task deathTask = new Task(p);
            currentTasks.add(deathTask);
            deathLocations.put(p.getUniqueId(), p.getLocation());

            deathTask.runTaskTimerAsynchronously(plugin, 15L, 6L);

            GameSound.play(Sound.ENTITY_WITHER_SPAWN, p.getLocation(), 0.6f, 1.1f);

            NMS.packets.sendTitleLength(p, 10, 100, 30);
            NMS.packets.sendTitle(p, ChatColor.DARK_GREEN + "Second Wind");
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onRespawn(PlayerRespawnEvent e) {

        Player p = e.getPlayer();

        Task task = get(p);

        if (task != null) {
            task.cancel();
            currentTasks.remove(task);

            Location loc = deathLocations.remove(e.getPlayer().getUniqueId());
            if (loc == null)
                return;

            e.setRespawnLocation(loc);

            GameSound.play(Sound.ENTITY_WITHER_SPAWN, loc, 0.65f, 1.1f);
            loc.getWorld().spawnParticle(Particle.HEART, loc.add(0, 1, 0), 10, 0.4, 1.0, 0.4, 0.0);

            if (getLevel(p) >= upgraded) {
                PotionEffectUtil.addPotionEffect(p, PotionEffectType.SPEED, 2, 4, false, false);
                p.setNoDamageTicks(80);
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerQuit(PlayerQuitEvent e) {
        deathLocations.remove(e.getPlayer().getUniqueId());
    }


    private Task get(Player p) {
        for (Task task : currentTasks) {
            if (task.player.equals(p)) {
                return task;
            }
        }
        return null;
    }

    private class Task extends BukkitRunnable {

        private final Player player;
        int i = 0;

        public Task(Player p) {
            player = p;
        }

        @Override
        public void run() {

            if (!player.isOnline()) {
                cancel();
                currentTasks.remove(this);
            }

            Location location = player.getLocation();

            double y = ((++i % 20) * 0.15);
            double radius = Math.max((y - 1) + 0.7, 0.7);
            double offset = Math.max(y - 2.5, 0.1);

            List<Location> circle = CoreUtils.circle(location.add(0, y, 0), radius, 12);

            for (Location loc : circle) {

                loc.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, loc, 3, offset, offset, offset, 0.05f);
                if (i % 4 == 0) {
                    loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc, 4, offset + 0.2, offset + 0.2, offset + 0.2, 0.1);
                }

            }

        }


    }


}