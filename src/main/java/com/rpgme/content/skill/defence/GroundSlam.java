package com.rpgme.content.skill.defence;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.ArmorUtils;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;


public class GroundSlam extends Ability<Defence> {

    // one tick in nano seconds
    private static final long NANO_TICK = 50 * 10_000_000;

    private final Map<Player, ChargeStatus> chargeMap = new WeakHashMap<>();
    private int unlocked;

    private static class ChargeStatus {
        int charge = 0;
        long chargeTime = 0;
    }

    public GroundSlam(Defence defence) {
        super(defence, "Ground Slam", Defence.ABILITY_GROUND_SLAM);

    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {

    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 50);
        messages.addValue("notification", ConfigHelper.getBundledMessage(getClass(), ""));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlocked = getConfig().getInt("unlocked");
        addNotification(unlocked, Notification.ICON_UNLOCK, getDisplayName(),
            messages.getMessage("notification"));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFall(PlayerMoveEvent e) {
        Player p = e.getPlayer();

        if (p.isSneaking() && !p.isFlying() && !p.getLocation().getBlock().isLiquid() && isEnabled(p) && !p.isOnGround() && getLevel(p) >= unlocked) {

            ChargeStatus status = chargeMap.get(p);
            if (status == null) {
                status = new ChargeStatus();
                chargeMap.put(p, status);
            }

            long now = System.nanoTime();
            if (now - status.chargeTime > NANO_TICK) {
                status.charge = 0;
            }

            status.charge++;
            status.chargeTime = now;
            doParticleEffect(p.getLocation(), status.charge - 5);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onLand(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.FALL && e.getEntityType() == EntityType.PLAYER) {

            ChargeStatus status = chargeMap.remove(e.getEntity());
            if (status != null && System.nanoTime() - status.chargeTime < NANO_TICK) {

                Player p = (Player) e.getEntity();

                int charge = status.charge;
                int level = getLevel(p);

                // 5 - 10 damage based on level
                double damage = (5 + (6.0 / 70 * (level - 25)))
                    * getArmorMultiplier(p) // x armor bonus
                    * Math.min((charge) / 10.0, 2); // per 10 charges

                double radius = Math.min(charge / 3, 4.0);

                doSlamEffect(p, damage, radius);

                // prevent death if died within 3 hearts
                if (e.getDamage() >= p.getHealth() && p.getHealth() - e.getDamage() > -6) {
                    e.setDamage(p.getHealth() - 2);
                }

                // sound
                float volume = Math.max(0.5f, Math.min(2f, charge / 40f));
                GameSound.play(Sound.ENTITY_GENERIC_EXPLODE, p.getLocation(), volume, 1.2f);
            }
        }
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onHit(EntityChangeBlockEvent e) {
        if (e.getEntity().hasMetadata("groundSlam")) {
            e.setCancelled(true);
            FallingBlock block = (FallingBlock) e.getEntity();
            block.setDropItem(false);
            block.remove();
        }

    }

    private void doSlamEffect(Player player, double damage, double range) {
        int amount = (int) Math.round(damage / 1.6);
        spawnBlocks(player.getLocation().getBlock().getRelative(BlockFace.DOWN), amount);

        for (Entity e : player.getNearbyEntities(range, range / 2, range)) {
            if (e instanceof LivingEntity) {

                Vector out = e.getLocation().subtract(player.getLocation()).toVector().setY(0.5);
                e.setVelocity(out);

                ((Damageable) e).damage(damage);
            }
        }
    }

    private void spawnBlocks(Block center, int amount) {

        int[][] cyl = com.rpgme.content.skill.mining.PowerTool.cylFlat;

        for (int i = 0; i < amount; i++) {

            int[] mod = cyl[i % cyl.length];
            Block b = center.getRelative(mod[0], mod[1], mod[2]);

            if (!b.getType().isSolid()) {
                continue;
            }

            Vector dir = new Vector(mod[0] * 0.2, 0.4, mod[1] * 0.2);

            FallingBlock block = b.getWorld().spawnFallingBlock(b.getLocation().add(0, 1.1, 0), b.getBlockData());
            block.setMetadata("groundSlam", CoreUtils.FIXED_META_VALUE);
            block.setVelocity(dir);
            block.setDropItem(false);
        }

    }

    private double getArmorMultiplier(Player p) {
        ItemStack[] armor = p.getEquipment().getArmorContents();
        double value = 1.0;
        for (ItemStack item : armor) {
            if (ArmorUtils.isHeavy(item)) {
                value += 0.25;
            } else if (ArmorUtils.isLight(item)) {
                value += 0.1;
            }
        }
        return value;
    }

    private void doParticleEffect(Location loc, int amount) {
        if (amount > 0) {
            loc.getWorld().spawnParticle(Particle.BLOCK_CRACK, loc, amount,
                    0.2, 0, 0.2, 1.0, Material.BEDROCK.createBlockData());
        }
    }


}
