package com.rpgme.content.skill.enchanting;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.math.ScaledValue;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class CustomEnchantmentsHook extends Ability<Enchanting> {

    private static final String CONFIG_PATH = "Global.Enchantments.CEnchantingProbability";

    private Plugin enchantmentsPlugin;
    private int originalProbability;

    private int unlocked;
    private ScaledValue chanceIncrease;

    public CustomEnchantmentsHook(Enchanting skill) throws NoClassDefFoundError {
        super(skill, "CustomEnchantments Hook", Enchanting.ABILITY_CUSTOM_ENCHANTS_HOOK);
    }

    @Override
    public void onEnable() {
        super.onEnable();
        enchantmentsPlugin = Bukkit.getPluginManager().getPlugin("CustomEnchantments");
        if (enchantmentsPlugin == null) {
            RPGme.getInstance().removeModule(getId());
        }
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);

        config.addValue("unlocked", 15)
                .addValue("max increase", 2.5);

        messages.addValue("notification", ConfigHelper.getBundledMessage("customenchantments_hook"));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        originalProbability = enchantmentsPlugin.getConfig().getInt(CONFIG_PATH);

        unlocked = config.getInt("unlocked", 15);
        double maxMultiplier = config.getDouble("max increase", 2.5);
        chanceIncrease = new ScaledValue(unlocked, 1.15, 100, maxMultiplier);

        addNotification(unlocked, Notification.ICON_UNLOCK, "Custom Enchantments", messages.getMessage("notification"));
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if (forlevel >= unlocked) {
            list.add("Custom Enchant chance:x" + chanceIncrease.readableScale(forlevel));
        }
    }

    private void setProbability(int percent) {
        enchantmentsPlugin.getConfig().set(CONFIG_PATH, String.valueOf(percent));
    }

    private void resetProbability() {
        setProbability(originalProbability);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEnchant(EnchantItemEvent event) {

        Player player = event.getEnchanter();
        if (!isEnabled(player)) {
            resetProbability();
            return;
        }

        int level = getLevel(player);

        if (level < unlocked) {
            resetProbability();
        } else {

            double multiplier = chanceIncrease.scale(level);
            int chance = (int) Math.round(originalProbability * multiplier);
            setProbability(chance);
        }
    }
}
