package com.rpgme.content.skill;

import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.util.EntityTypes;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.Arrays;

public class ExpTables implements Module {

    public static final float STAMINA_PER_SEC = 2.8f;
    public static final float STAMINA_JUMP_EXP = 0.8f;

    // cached for heavy use
    private static double expScale = 1.0;

    // settable from config
    public static void setRequirementMultiplier(double expScale) {
        ExpTables.expScale = expScale;
    }

    private static int[] levelLookupTable;

    static {
        fillLevelLookupTable();
    }

    private static void fillLevelLookupTable() {
        int toLevel = 100;
        levelLookupTable = new int[toLevel];
        for (int i = 0; i < toLevel; i++) {
            levelLookupTable[i] = xpForLevel(i + 1);
        }
    }

    /**
     * Gets the exp required to reach level <code>level</code>
     *
     * @param level
     * @return the exp at which you should be this level
     */
    public static int xpForLevel(int level) {
        /*
            v1: ((Math.pow(lvl-1, 2.6) + ((lvl-1)*(35+lvl))) * expScale)
            v2: ((Math.pow(lvl, 2.6) + (lvl * (lvl+1) * 2)) * 5 * expScale)
            v3: ((Math.pow(level, 2.25) + (level * level * 1.6)) * 4.6 * expScale) + 1
            v3 > 99: 170000 + Math.pow(level-80, 3.6))
         */
        return (int) (((Math.pow(level - 1, 2.72) * 2) + ((level - 1) * 20)) * expScale); // v4
    }

    public static int getLevelAt(int exp) {
        int index = Arrays.binarySearch(levelLookupTable, exp);
        if (index < 0) {
            if (index < -levelLookupTable.length) {
                // out of lookup list. calculate manually.
                int level = levelLookupTable.length;
                int required = levelLookupTable[level - 1];
                while (exp >= required) {
                    required = (xpForLevel(++level));
                }
                return level - 1;
            }
            // not an exact match but we are somewhere between levels (most common)
            return Math.abs(index) - 1;
        }
        // exactly at exp required for next level
        return index + 1;
    }

    public static int getLevelAt(float exp) {
        return getLevelAt((int) exp);
    }


    /* combat */

    public static double getExpRewardForKilling(Entity killed) {
        double exp;
        if (EntityTypes.isPassive(killed)) {
            exp = 3.5;
        } else if (EntityTypes.isNetherMob(killed)) {
            exp = 7.5;
        } else if (EntityTypes.isBoss(killed)) {
            exp = 350;
        } else if (EntityTypes.isHostile(killed)) {
            exp = 5.5;
        } else {
            exp = 0;
        }
        return exp;
    }

    // TODO run through all lists below to see if any flattend materials should be added

    /* Mining */

    public static float getMiningExp(Block block) {
        Material mat = block.getType();
        switch (mat) {
            case NETHERRACK:
            case END_STONE:
                return 0.1f;

            case SMOOTH_STONE:
            case COBBLESTONE:
            case STONE:
            case ANDESITE:
            case GRANITE:
            case DIORITE:
                return 1;

            case SANDSTONE:
            case RED_SANDSTONE:
                return 1.5f;

            case PRISMARINE:
            case TERRACOTTA:
            case MOSSY_COBBLESTONE:
                return 3;

            case OBSIDIAN:
            case NETHER_QUARTZ_ORE:
            case COAL_ORE:
                return 5;

            case IRON_ORE:
                return 8;

            case LAPIS_ORE:
            case REDSTONE_ORE:
                return 10;

            case EMERALD_ORE:
            case GOLD_ORE:
                return 14;

            case DIAMOND_ORE:
                return 40;
            default:
                return 0;
        }
    }

    /* Landscaping */
    public static int getLandscapingExp(Material mat) {
        switch (mat) {
            case TALL_GRASS:
            case TALL_SEAGRASS:
                return 1;
            case SNOW:
            case SAND:
            case RED_SAND:
            case DIRT:
                return 2;
            case COARSE_DIRT:
            case PODZOL:
            case GRASS_BLOCK:
            case SOUL_SAND:
                return 3;
            case GRAVEL:
            case SNOW_BLOCK:
                return 5;
            case CLAY:
                return 10;
            default:
                return 0;
        }
    }

    /* Woodcutting */
    public static int getWoodcuttingExp(Block block) {
        switch (block.getType()) {
            case OAK_LOG:
                return 5;
            case BIRCH_LOG:
                return 6;
            case SPRUCE_LOG:
                return 8;
            case JUNGLE_LOG:
                return 8;
            case DARK_OAK_LOG:
                return 8;
            case ACACIA_LOG:
                return 10;
            case MUSHROOM_STEM:
            case BROWN_MUSHROOM_BLOCK:
            case RED_MUSHROOM_BLOCK:
                return 5;
            default:
                return 0;
        }
    }

    /* Forging */
    public static float getForgingExp(String material) {
        int amount = 2;
        switch (material.toLowerCase()) {
            case "diamond":
                amount *= 2;
            case "chainmail":
            case "golden":
                amount *= 2;
            case "leather":
            case "iron":
                amount *= 2;
            case "stone":
                amount *= 2;
            case "wooden":
                break;
            default:
                return 0;
        }
        return amount;

    }

    /* Alchemy */

    public static int getAlchemyExp(Material ingredient) {
        switch (ingredient) {
            case NETHER_WART:
            case REDSTONE:
            case SUGAR:
            case SPIDER_EYE:
                return 7;
            case GLOWSTONE_DUST:
            case GUNPOWDER:
                return 9;
            case FERMENTED_SPIDER_EYE:
            case GLISTERING_MELON_SLICE:
            case GOLDEN_CARROT:
                return 18;
            case BLAZE_POWDER:
            case MAGMA_CREAM:
                return 25;
            case GHAST_TEAR:
            case RABBIT_FOOT:
                return 40;
            case PUFFERFISH:
                return 70;
            default:
                return 0;
        }

    }

    /* farming */

    // grow tree
    public static int getFarmingTreeExp(TreeType treeType) {
        switch (treeType) {
            case JUNGLE_BUSH:
                return 10;
            case TREE:
            case SWAMP:
                return 15;
            case BIRCH:
            case ACACIA:
            case REDWOOD:
            case SMALL_JUNGLE:
            case COCOA_TREE:
                return 20;
            case BROWN_MUSHROOM:
            case RED_MUSHROOM:
                return 25;
            case BIG_TREE:
                return 30;
            case TALL_BIRCH:
            case TALL_REDWOOD:
                return 40;
            case JUNGLE:
                return 60;
            case DARK_OAK:
            case MEGA_REDWOOD:
                return 80;
            default:
                return 0;
        }
    }

    // grow
    public static int getFarmingGrowExp(Material crop) {
        switch (crop) {
            case SUGAR_CANE:
            case WHEAT:
            case POTATO:
            case CARROT:
                return 4;
            case PUMPKIN:
            case MELON:
                return 6;
            case NETHER_WART:
            case COCOA:
                return 8;
            case PUMPKIN_STEM:
            case MELON_STEM:
                return 15;
            case CHORUS_PLANT:
            case CHORUS_FLOWER:
                return 12;
            default:
                return 0;
        }
    }

    // harvest
    public static float getFarmingHarvestExp(Material mat) {
        switch (mat) {
            case SUGAR_CANE:
            case CHORUS_PLANT:
            case CHORUS_FLOWER:
                return 5;
            case PUMPKIN_STEM:
            case MELON_STEM:
            case WHEAT:
                return 6;
            case BEETROOTS:
            case POTATO:
            case CARROT:
                return 8;
            case COCOA:
            case NETHER_WART:
                return 10;
            case PUMPKIN:
            case MELON:
                return 12;
            default:
                return 0;
        }
    }

    /* Taming */

    public static final double TAMING_EXP_RIDING_HORSE = 0.25;

    public static int getTameExp(EntityType type) {
        switch (type) {
            case HORSE:
                return 55;
            case WOLF:
            case OCELOT:
                return 35;
            default:
                return 0;
        }
    }

    public static int getBreedExp(EntityType type) {
        switch (type) {
            case CHICKEN:
            case COW:
            case PIG:
            case SHEEP:
            case RABBIT:
                return 10;
            case OCELOT:
            case WOLF:
                return 12;
            case HORSE:
            case IRON_GOLEM:
            case SNOWMAN:
                return 22;
            case VILLAGER:
                return 500; // for curing zombies
            default:
                return 0;
        }
    }


}
