package com.rpgme.content.skill.archery;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.VarMaxEnergyCooldown;
import com.rpgme.plugin.util.math.ScaledValue;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.AbstractArrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;
import org.bukkit.util.Vector;

import java.util.List;


public class Darts extends Ability<Archery> {

    private int UNLOCK, POISON_UNLOCK;
    private ScaledValue maxEnergy, velocity;
    private VarMaxEnergyCooldown cooldown;

    private ScaledValue poisonChance;

    public Darts(Archery skill) {
        super(skill, "Darts", Archery.ABILITY_DARTS);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 15);
        config.addValue("poisoned", 40);

        messages.addValue("notification1", ConfigHelper.getBundledMessage(getClass(), "1"));
        messages.addValue("notification2", ConfigHelper.getBundledMessage(getClass(), "2"));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        UNLOCK = config.getInt("unlocked");
        POISON_UNLOCK = config.getInt("poisoned");

        cooldown = new VarMaxEnergyCooldown(plugin, 5, 10);
        maxEnergy = new ScaledValue(UNLOCK, 20, 100, 50);
        velocity = new ScaledValue(UNLOCK, 1.5, 100, 2.2);
        poisonChance = new ScaledValue(POISON_UNLOCK, 10, 100, 50);

        addNotification(UNLOCK, Notification.upgradableIcon(1, 2), getDisplayName(), messages.getMessage("notification1"));
        addNotification(POISON_UNLOCK, Notification.upgradableIcon(2, 2), "Poisoned " + getName(), messages.getMessage("notification2"));
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if (forlevel >= UNLOCK) {
            list.add("Darts Energy:" + (int) Math.round(maxEnergy.scale(forlevel)) + "&7 costs 12");
        }
        if (forlevel >= POISON_UNLOCK)
            list.add("Poison Chance:" + poisonChance.readableScale(forlevel) + "%");
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onThrow(PlayerInteractEvent e) {

        if (!e.hasItem() || e.getAction() == Action.PHYSICAL || !isArrow(e.getItem().getType()))
            return;

        Player p = e.getPlayer();
        if (!isEnabled(p))
            return;

        int level = skill.getLevel(p);

        if (level >= UNLOCK) {

            // do cooldown
            if (cooldown.isOnCooldown(p)) {
                sendOnCooldownMessage(p, cooldown.getMillisRemaining(p));
                return;
            }
            int maxCooldownEnergy = (int) Math.round(maxEnergy.scale(level));
            cooldown.add(p, maxCooldownEnergy);

            // take arrow and spawn
            PlayerInventory inv = p.getInventory();
            if (e.getHand() == EquipmentSlot.HAND) {
                ItemStack item = inv.getItemInMainHand();
                spawnArrow(p, level, item);
                item.setAmount(item.getAmount() - 1);
                inv.setItemInMainHand(item);
            } else if (e.getHand() == EquipmentSlot.OFF_HAND) {
                ItemStack item = inv.getItemInOffHand();
                spawnArrow(p, level, item);
                item.setAmount(item.getAmount() - 1);
                inv.setItemInOffHand(item);
            }
        }

    }

    public void spawnArrow(Player p, int level, ItemStack item) {
        // do poison
        if (item.getType() == Material.ARROW && level > POISON_UNLOCK && poisonChance.isRandomChance(level)) {
            GameSound.play(Sound.BLOCK_SLIME_BLOCK_FALL, p.getLocation(), 0.8f, 0.6f); // TODO add metadata and check on hit
            item = item.clone();
            item.setType(Material.TIPPED_ARROW);
            PotionMeta meta = (PotionMeta) item.getItemMeta();
            meta.setBasePotionData(new PotionData(PotionType.POISON));
            item.setItemMeta(meta);
        }
        // spawn arrow
        Vector dir = CoreUtils.addNaturalOffset(p.getLocation().getDirection().multiply(velocity.scale(level)), 0.06);
        AbstractArrow a = skill.spawnArrow(p, item, dir);
        a.setCritical(true); // TODO make critical a level unlock
        // TODO make poison 2 a level unlock
    }

    private boolean isArrow(Material mat) {
        return mat == Material.ARROW || mat == Material.TIPPED_ARROW || mat == Material.SPECTRAL_ARROW;
    }
}
