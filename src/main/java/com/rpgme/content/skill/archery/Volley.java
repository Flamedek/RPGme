package com.rpgme.content.skill.archery;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.MessageUtil;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.VarMaxEnergyCooldown;
import com.rpgme.plugin.util.math.ScaledValue;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.AbstractArrow;
import org.bukkit.entity.AbstractArrow.PickupStatus;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

public class Volley extends Ability<Archery> {

    private int unlocked;
    private int upgradeInterval;

    private final VarMaxEnergyCooldown cooldown;
    private ScaledValue maxEnergy;

    private final Map<Player, ItemStack> map = new WeakHashMap<>();

    public Volley(Archery skill) {
        super(skill, "Volley", Archery.ABILITY_VOLLEY);
        cooldown = new VarMaxEnergyCooldown(plugin, 1, 20);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 30);
        config.addValue("amount of levels needed to get an extra arrow", "upgrade interval", 15);

        messages.addValue("notification1", ConfigHelper.getBundledMessage(getClass(), "1"));
        messages.addValue("notification2+", ConfigHelper.getBundledMessage(getClass(), "2+"));
        messages.addValue("loaded", ConfigHelper.getBundledMessage("ability_volley_loaded"));
        messages.addValue("unload", ConfigHelper.getBundledMessage("ability_volley_unload"));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlocked = config.getInt("unlocked");
        upgradeInterval = config.getInt("upgrade interval");

        maxEnergy = new ScaledValue(unlocked, 20, 100, 70);

        addNotification(unlocked, Notification.ICON_UNLOCK, getDisplayName(), messages.getMessage("notification1"));

        int maxLvl = getPlugin().getSkillManager().getTargetLevel();
        int lvl = unlocked;
        int arrows = 3;

        while (lvl < maxLvl) {
            arrows++;
            lvl += upgradeInterval;
            addNotification(lvl, Notification.ICON_UPGRADE, getDisplayName() + " (" + arrows + ")", messages.getMessage("notification2+", arrows));
        }
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if (forlevel >= unlocked) {
            list.add("Volley Energy:" + (int) Math.round(maxEnergy.scale(forlevel)) + "&7 costs 20");
            list.add("Volley Arrows:" + getArrowsToShoot(forlevel));
        }
    }

    private int getArrowsToShoot(int level) {
        int arrows = 1;
        for (int i = 0; i < 5; i++) {
            arrows++;
            if (level < unlocked + upgradeInterval * i) {
                break;
            }
            if (i == 4) {
                arrows++;
            }
        }
        return arrows;
    }

    @EventHandler
    public void onLoad(PlayerInteractEvent e) {
        if (!e.hasItem() || e.getItem().getType() != Material.BOW)
            return;

        if (e.getAction() != Action.LEFT_CLICK_AIR && e.getAction() != Action.LEFT_CLICK_BLOCK)
            return;

        Player p = e.getPlayer();
        if (!isEnabled(p))
            return;

        int level = getLevel(e.getPlayer());

        if (level >= unlocked) {

            //cooldown
            if (cooldown.isOnCooldown(p)) {
                sendOnCooldownMessage(p, cooldown.getMillisRemaining(p));
                return;
            }

            int arrowAmount = getArrowsToShoot(level);
            ItemStack arrows = findArrowsToShoot(p, arrowAmount);

            if (arrows == null) {
                GameSound.play(Sound.ENTITY_VILLAGER_NO, p);
                p.sendMessage(ChatColor.RED + "You need " + arrowAmount + " arrows to load an volley.");
                return;
            }

            if (arrows.equals(map.get(p))) {
                return;
            }

            map.put(p, arrows);

            MessageUtil.sendToActionBar(p, messages.getMessage("loaded", arrowAmount, getEffectName(arrows)));
            GameSound.play(Sound.BLOCK_NOTE_BLOCK_PLING, p, 1.5f, 0.8f, 0.2);
        }
    }

    private ItemStack findArrowsToShoot(Player player, int amount) {
        EnumSet<Material> toConsider = EnumSet.of(Material.ARROW, Material.TIPPED_ARROW, Material.SPECTRAL_ARROW);

        PlayerInventory inv = player.getInventory();
        ItemStack[] items = player.getInventory().getContents();
        ItemStack found = null;

        for (int i = 0; i < items.length; i++) {
            // start at 40 to get offhand first
            ItemStack item = items[(40 + i) % items.length];

            if (item != null && toConsider.contains(item.getType()) && (item.getAmount() >= amount || inv.containsAtLeast(item, amount))) {
                found = item.clone();
                break;
            }
        }

        if (found != null) {
            // -1 because one arrow is shot regularly
            found.setAmount(amount - 1);
        }
        return found;
    }

    private String getEffectName(ItemStack item) {
        String type = "";
        switch (item.getType()) {
            case ARROW:
                break;
            case SPECTRAL_ARROW:
                type = "Spectral";
                break;
            case TIPPED_ARROW:
                PotionMeta meta = (PotionMeta) item.getItemMeta();
                type = StringUtils.reverseEnum(meta.getBasePotionData().getType().getEffectType().getName());
                if (meta.getBasePotionData().isUpgraded())
                    type += " II";
                break;
        }
        return type;
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onShoot(EntityShootBowEvent e) {
        final ItemStack shots = map.remove(e.getEntity());
        if (shots == null)
            return;

        Bukkit.getScheduler().runTask(RPGme.getInstance(), () -> {
            Player p = (Player) e.getEntity();

            if (p.getGameMode() != GameMode.CREATIVE && !ItemUtils.take(p.getInventory(), shots, shots.getAmount())) {
                GameSound.play(Sound.ENTITY_VILLAGER_NO, p);
                return;
            }

            int level = getLevel(p);

            cooldown.add(p, (int) Math.round(maxEnergy.scale(level)));

            Vector velocity = p.getLocation().getDirection().multiply(e.getProjectile().getVelocity().length());
            boolean isCritical = ((Arrow) e.getProjectile()).isCritical();
            int fireTicks = e.getProjectile().getFireTicks();
            PickupStatus pickupStatus = p.getGameMode() == GameMode.CREATIVE ? PickupStatus.CREATIVE_ONLY : PickupStatus.ALLOWED;

            shootVolley(p, velocity, shots, isCritical, fireTicks, pickupStatus);
        });
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSlotChange(PlayerItemHeldEvent e) {
        if (map.remove(e.getPlayer()) != null) {
            MessageUtil.sendToActionBar(e.getPlayer(), messages.getMessage("unload"));
        }
    }

    public void shootVolley(final ProjectileSource source, final Vector velocity, final ItemStack arrows, final boolean crit, final int fireTicks, final PickupStatus pickupStatus) {

        final Vector halves = new Vector(0.5, 0.5, 0.5);
        final int amount = arrows.getAmount();
        final double offset = 0.22;

        new BukkitRunnable() {

            int i = 0;

            @Override
            public void run() {
                if (++i >= amount) {
                    cancel();
                }

                Vector dir = Vector.getRandom().subtract(halves).multiply(2 * offset).add(velocity);

                AbstractArrow a = skill.spawnArrow(source, arrows, dir);
                a.setCritical(crit);
                a.setFireTicks(fireTicks);
                a.setBounce(false);
                a.getWorld().playSound(a.getLocation(), Sound.ENTITY_ARROW_SHOOT, SoundCategory.PLAYERS, 1.5f, 1.1f + (i * 0.15f));
                a.setPickupStatus(pickupStatus);
            }

        }.runTaskTimer(plugin, 1, 2);
    }


}
